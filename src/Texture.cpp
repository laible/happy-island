#include "Texture.h"
#include "SDL.h"
#include "SDL_image.h"


std::map<std::string, GLuint > Texture::resource_manager_ ; 

Texture::Texture(const std::string& filename, TextureFlags flags):id(0)
{
  std::map<std::string, GLuint >::iterator it = resource_manager_.find(std::string(filename));

  if (it!=resource_manager_.end())
  {
    id = it->second;
    return;
  };



  glPushAttrib(GL_ALL_ATTRIB_BITS);
    // load from file
    SDL_Surface *image ;
    image = IMG_Load(filename.c_str());
    printf("%s\n", SDL_GetError());
    if (!image)
    {
      printf ( "Texture load error!\n");
    } else 
    {            
	    //Check that the image is square (width equal to height)
	    if (image->w != image->h) 
      {
		   // printf("Error: width doesn't match height!\n");
	    }    

	    //Check that the image's width is valid and then check that the image's width is a power of 2
	    if (image->w < 1) 
      {
		    printf("Error: width < 1!\n");
	    } else if ( !((image->w & (image->w-1))==0) ) 
      {
		    printf("Error: texture data not power of 2!\n");
	    }    

      if (image->format->BitsPerPixel < 24)
      {
        printf("Error: BitsPerPixel < 24! Pixel format of file %s not supported\n", filename.c_str());
      }

    //  printf("Surface %s: w:%d h:%d bpp:%d\n", filename.c_str(), image->w, image->h, image->format->BitsPerPixel);

     // printf("Masks: %X %X %X %X\n", image->format->Rmask, image->format->Gmask, image->format->Bmask, image->format->Amask);



      width_ = image->w;
      height_ = image->h;
            
      // generate Texture ids
      glGenTextures(1, &id);

	    //Load the texture
	    glBindTexture(GL_TEXTURE_2D, id);

      
	    //Generate the texture
      if(image->format->BitsPerPixel == 24)
      {
	      glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image->w, image->h, 0, GL_RGB, GL_UNSIGNED_BYTE, image->pixels);
      } else
      {
	      glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image->w, image->h, 0, GL_RGBA , GL_UNSIGNED_BYTE, image->pixels);
      }

      // select modulate to mix texture with color for shading
      glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );

      // when texture area is small, bilinear filter the closest mipmap
      glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST );
      // when texture area is large, bilinear filter the original
      glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

      // the texture wraps over at the edges (repeat)
      glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
      glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
      
      // build our texture mipmaps
      if (flags == MIPMAPS)
      {
        if(image->format->BitsPerPixel == 24)
        {
          gluBuild2DMipmaps( GL_TEXTURE_2D, 3, image->w, image->h, GL_RGB, GL_UNSIGNED_BYTE, image->pixels );
        } else
        {
          gluBuild2DMipmaps( GL_TEXTURE_2D, 3, image->w, image->h, GL_RGBA, GL_UNSIGNED_BYTE, image->pixels );
        }

      }
    };
    if (image) { SDL_FreeSurface(image); }
  glPopAttrib();
  resource_manager_[filename] = id;
}

