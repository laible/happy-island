#include "Landscape.h"
#include "SDL.h"

#define DEBUG_LANDSCAPE

#define RESOLUTION 10

#define SQRT_2_PI_ (2.506628274631000502)

Landscape::Landscape(char* ase_filename):
  mesh_(ase_filename),
  map_vi_to_ti(0)
{
};

// Triangle Selectors

// every triangle that has at least one vertex with distance < max_distance will be added
// O(mesh_.getNumberOfTriangles())
std::list<Mesh3d::TriangleIndex> Landscape::getNearTriangles(const Vector3d& v, const float max_distance)
{
  float squared_distance = max_distance * max_distance;
  std::list<Mesh3d::TriangleIndex> ret;   

  for (Mesh3d::TriangleIndex i = 0; i!=mesh_.getNumberOfTriangles(); i++)
  {
    // get vertex indizes of current triangle    
    Mesh3d::VertexIndex Ai = mesh_.getFaceToVertexMap()[i].indexOfA;
    Mesh3d::VertexIndex Bi = mesh_.getFaceToVertexMap()[i].indexOfB;
    Mesh3d::VertexIndex Ci = mesh_.getFaceToVertexMap()[i].indexOfC;
    // calc difference vector
    Vector3d da = mesh_.getVertices()[Ai]-v;
    Vector3d db = mesh_.getVertices()[Bi]-v;
    Vector3d dc = mesh_.getVertices()[Ci]-v;
    // compare squared length (faster)
    if (da.squaredLength() < squared_distance || db.squaredLength() < squared_distance || dc.squaredLength() < squared_distance)
      ret.push_back(i);   
  }   
  return ret;
}

void Landscape::buildSSS()
{
  structure_.clear();

  // calculate bounding box
  lowerLeftCorner = Vector3d(9e10f, 9e10f, 9e10f);
  upperRightCorner = Vector3d(-9e10f, -9e10f, -9e10f);

  for (Mesh3d::VertexIndex i=0; i!=mesh_.getNumberOfVertices(); ++i)
  {
    if (mesh_.getVertices()[i].x < lowerLeftCorner.x) lowerLeftCorner.x = mesh_.getVertices()[i].x;
    if (mesh_.getVertices()[i].y < lowerLeftCorner.y) lowerLeftCorner.y = mesh_.getVertices()[i].y;
    if (mesh_.getVertices()[i].z < lowerLeftCorner.z) lowerLeftCorner.z = mesh_.getVertices()[i].z;
    if (mesh_.getVertices()[i].x > upperRightCorner.x) upperRightCorner.x = mesh_.getVertices()[i].x;
    if (mesh_.getVertices()[i].y > upperRightCorner.y) upperRightCorner.y = mesh_.getVertices()[i].y;
    if (mesh_.getVertices()[i].z > upperRightCorner.z) upperRightCorner.z = mesh_.getVertices()[i].z;
  }

  // TODO???: expand bounding box a bit
  

  #ifdef DEBUG_LANDSCAPE
    printf("LS::building sub-division into squares\n", lowerLeftCorner.x, lowerLeftCorner.y, lowerLeftCorner.z, upperRightCorner.x, upperRightCorner.y, upperRightCorner.z);
  #endif

  // subdivide bounding box along x/y plane
  // create structure
  unsigned int resolution = RESOLUTION;
  structure_.resize(resolution);  
  for (int i=0; i!=resolution; i++)
  {
    structure_[i].resize(resolution);
  }

  Vector3d cubeURC = upperRightCorner;
  Vector3d cubeLLC = lowerLeftCorner; 

  // fill structure
  for (Mesh3d::VertexIndex i=0; i!=mesh_.getNumberOfVertices(); i++)
  {

    float dx = mesh_.getVertices()[i].x - lowerLeftCorner.x;
    float dy = mesh_.getVertices()[i].y - lowerLeftCorner.y;   

    unsigned ix = (unsigned) ((resolution-1) * dx / (upperRightCorner.x-lowerLeftCorner.x));  
    unsigned iy = (unsigned) ((resolution-1) * dy / (upperRightCorner.y-lowerLeftCorner.y));    

    structure_[ix][iy].push_front(i);

  }  

    #ifdef DEBUG_LANDSCAPE
      printf("LS::sub-division done \n");          
    #endif

}



// vertices with distance < max_distance will be added
// O(mesh_.getNumberOfVertices())
std::vector<Mesh3d::VertexIndex> Landscape::getNearVertices(const Vector3d& v, const float max_distance)
{
  std::vector<Mesh3d::VertexIndex> ret;
  ret.reserve(1024);
  unsigned int resolution = RESOLUTION;
  if (structure_.empty())
  {
    float squared_distance = max_distance * max_distance;

    for (Mesh3d::VertexIndex i = 0; i!=mesh_.getNumberOfVertices(); i++)
    {
      // calc difference vector
      Vector3d da = mesh_.getVertices()[i]-v;    
      // compare squared length (faster)
      if (da.squaredLength() < squared_distance)
        ret.push_back(i);   
    }    
  } else
  {
    float dx = v.x - lowerLeftCorner.x;
    float dy = v.y - lowerLeftCorner.y;   

    float ix = ((resolution-1) * dx / (upperRightCorner.x-lowerLeftCorner.x));
    float iy = ((resolution-1) * dy / (upperRightCorner.y-lowerLeftCorner.y));  
    
    // we want to get the 9 surrounding squares
    if (ix > resolution-2) ix = resolution-2;
    if (iy > resolution-2) iy = resolution-2;

    if (ix < 1) ix = 1;
    if (iy < 1) iy = 1;

    std::copy(structure_[(int)ix][(int)iy].begin(),structure_[(int)ix][(int)iy].end(), std::back_inserter(ret));       
    std::copy(structure_[(int)ix+1][(int)iy+1].begin(),structure_[(int)ix+1][(int)iy+1].end(), std::back_inserter(ret));       
    std::copy(structure_[(int)ix-1][(int)iy-1].begin(),structure_[(int)ix-1][(int)iy-1].end(), std::back_inserter(ret));       
    std::copy(structure_[(int)ix+1][(int)iy-1].begin(),structure_[(int)ix+1][(int)iy-1].end(), std::back_inserter(ret));       
    std::copy(structure_[(int)ix-1][(int)iy+1].begin(),structure_[(int)ix-1][(int)iy+1].end(), std::back_inserter(ret));       
    std::copy(structure_[(int)ix-1][(int)iy].begin(),structure_[(int)ix-1][(int)iy].end(), std::back_inserter(ret));       
    std::copy(structure_[(int)ix+1][(int)iy].begin(),structure_[(int)ix+1][(int)iy].end(), std::back_inserter(ret));       
    std::copy(structure_[(int)ix][(int)iy+1].begin(),structure_[(int)ix][(int)iy+1].end(), std::back_inserter(ret));       
    std::copy(structure_[(int)ix][(int)iy-1].begin(),structure_[(int)ix][(int)iy-1].end(), std::back_inserter(ret));       

  } 
  //printf("Ret.size() %d\n", ret.size());
  return ret;  
}

// given a vertex index calculate all adjacent faces
// Complexity: first time O(mesh_.getNumberOfTriangles()), else O(1)
std::list<Mesh3d::TriangleIndex> Landscape::getAdjacentTriangles(const Mesh3d::VertexIndex vi)
{
  // build search structure on first time
  if (map_vi_to_ti.empty())
  {
    map_vi_to_ti.resize(mesh_.getNumberOfVertices());
    for (Mesh3d::TriangleIndex i = 0; i!=mesh_.getNumberOfTriangles(); i++)
    {
      // get vertex indizes of current triangle    
      Mesh3d::VertexIndex Ai = mesh_.getFaceToVertexMap()[i].indexOfA;
      Mesh3d::VertexIndex Bi = mesh_.getFaceToVertexMap()[i].indexOfB;
      Mesh3d::VertexIndex Ci = mesh_.getFaceToVertexMap()[i].indexOfC;
      // add current triangle index to map 
      map_vi_to_ti[Ai].push_back(i);
      map_vi_to_ti[Bi].push_back(i);
      map_vi_to_ti[Ci].push_back(i);         
    }    
  }
  
  return map_vi_to_ti[vi];    
} 

void DUMP(const std::list<Mesh3d::VertexIndex>& l)
{
  for (std::list<Mesh3d::VertexIndex>::const_iterator it = l.begin(); it != l.end(); it++)
  {
    printf("%d\n", *it);
  }  
}

void Landscape::calcSmoothDeformation(const Vector3d& v, const float radius)
{
  long time =  SDL_GetTicks();
  printf("Landscape::deforming ...\n");
  Vector3d impact = v + Vector3d(0,0, 2); 
 printf("Time in deform landscape (in ms): %d\n", SDL_GetTicks()- time);
  if (structure_.empty()) buildSSS();  // bringt nicht viel
 printf("Time in deform landscape (in ms): %d\n", SDL_GetTicks()- time); 
  std::vector<Mesh3d::VertexIndex> modified_vis = getNearVertices(impact, 2* radius);
 
//  DUMP( modified_vis); 
 printf("Time in deform landscape (in ms): %d\n", SDL_GetTicks()- time);
  for (std::vector<Mesh3d::VertexIndex>::iterator it = modified_vis.begin(); it != modified_vis.end(); it++)
  {
    Vector3d diff = impact-mesh_.getVertices()[*it];
    Vector3d distance = (impact-mesh_.getVertices()[*it])*(1/radius);
    float dist = distance.squaredLength();
    float x = 1.0f / (SQRT_2_PI_ ) * exp( - dist );
    mesh_.getVertices()[*it].z = mesh_.getVertices()[*it].z - radius*x;
    if (fabs(dist) < 1.0)
      mesh_.getNormals()[*it] = (mesh_.getVertices()[*it]-impact).normalisedCopy();
  }
 printf("Time in deform landscape (in ms): %d\n", SDL_GetTicks()- time);
////////////////////BOTTLENECK 42ms

  // build triangles from ASE-FaceMap
  for (Mesh3d::TriangleIndex i=0; i!=mesh_.getNumberOfTriangles(); i++)
  {
    mesh_.getTriangles()[i] = Triangle3d(mesh_.getVertices()[mesh_.getFaceToVertexMap()[i].indexOfA], mesh_.getVertices()[mesh_.getFaceToVertexMap()[i].indexOfB], mesh_.getVertices()[mesh_.getFaceToVertexMap()[i].indexOfC]);
    mesh_.getNormalsVArray()[i] = Triangle3d(mesh_.getNormals()[mesh_.getFaceToVertexMap()[i].indexOfA], mesh_.getNormals()[mesh_.getFaceToVertexMap()[i].indexOfB], mesh_.getNormals()[mesh_.getFaceToVertexMap()[i].indexOfC]);
	  if (mesh_.getNumberOfTextureStages()==1)
	  {
		  mesh_.getTextureCoordsVArray()[i] = Triangle3dUVTextureCoords(mesh_.getTextureCoords()[mesh_.getFaceToVertexMap()[i].indexOfA],mesh_.getTextureCoords()[mesh_.getFaceToVertexMap()[i].indexOfB],mesh_.getTextureCoords()[mesh_.getFaceToVertexMap()[i].indexOfC]);
	  }
  }
////////////////////BOTTLENECK
 printf("Time in deform landscape (in ms): %d\n", SDL_GetTicks()- time);
  // recreate display list
 glDeleteLists(mesh_.getDL(),1);
 printf("Time in deform landscape (in ms): %d\n", SDL_GetTicks()- time);
 mesh_.createDL(); 
 printf("Time in deform landscape (in ms): %d\n", SDL_GetTicks()- time);
  printf("Landscape::deforming done...\n");
  printf("Time in deform landscape (in ms): %d\n", SDL_GetTicks()- time);
}

void Landscape::calcSphericalDeformation(const Vector3d& v, const float radius)
{
  Vector3d impact = v + Vector3d(0,0, 2); 

 
  std::vector<Mesh3d::VertexIndex> modified_vis = getNearVertices(impact, radius);
 

//  DUMP( modified_vis); 

  for (std::vector<Mesh3d::VertexIndex>::iterator it = modified_vis.begin(); it != modified_vis.end(); it++)
  {
    // project vertices on sphere
    mesh_.getNormals()[*it] = (impact-mesh_.getVertices()[*it]).normalisedCopy();
    mesh_.getVertices()[*it] = impact - mesh_.getNormals()[*it]*radius;
  }


  // build triangles from ASE-FaceMap
  for (Mesh3d::TriangleIndex i=0; i!=mesh_.getNumberOfTriangles(); i++)
  {
    mesh_.getTriangles()[i] = Triangle3d(mesh_.getVertices()[mesh_.getFaceToVertexMap()[i].indexOfA], mesh_.getVertices()[mesh_.getFaceToVertexMap()[i].indexOfB], mesh_.getVertices()[mesh_.getFaceToVertexMap()[i].indexOfC]);
    mesh_.getNormalsVArray()[i] = Triangle3d(mesh_.getNormals()[mesh_.getFaceToVertexMap()[i].indexOfA], mesh_.getNormals()[mesh_.getFaceToVertexMap()[i].indexOfB], mesh_.getNormals()[mesh_.getFaceToVertexMap()[i].indexOfC]);
	  if (mesh_.getNumberOfTextureStages()==1)
	  {
		  mesh_.getTextureCoordsVArray()[i] = Triangle3dUVTextureCoords(mesh_.getTextureCoords()[mesh_.getFaceToVertexMap()[i].indexOfA],
        mesh_.getTextureCoords()[mesh_.getFaceToVertexMap()[i].indexOfB],
        mesh_.getTextureCoords()[mesh_.getFaceToVertexMap()[i].indexOfC]);
	  }
  }


  // recreate display list
  glDeleteLists(mesh_.getDL(),1);
  mesh_.createDL();  
}

