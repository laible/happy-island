#include "Menu.h"

//#define DEBUG_MENU

Menu::Menu()
{
}

void Menu::draw()
{
  #ifdef DEBUG_MENU
    printf("drawing Menu...\n");
  #endif

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT ); 
  glPushAttrib(GL_ALL_ATTRIB_BITS);
  glDisable(GL_LIGHTING);
  mainMenu_.draw();
  glPopAttrib();
  glEnable(GL_LIGHTING);
}

void Menu::keyPressEvent(SDL_KeyboardEvent e)
{
  mainMenu_.keyPressEvent(e);
}

void Menu::update()
{
  if (mainMenu_.state_ > 3) {
    setTerminated(true);
  }
}

