#include "SpatialSearchStructure.h"

#include "SDL.h"

#define DEBUG_SSS

#define RESOLUTION 10

void SpatialSearchStructure::build(Mesh m)
{
  long time = SDL_GetTicks();

  structure_.clear();
  #ifdef DEBUG_LANDSCAPE
    printf("Building spatial search structure...\n", m);
  #endif

  // calculate bounding box
 if (va_triangles==0)
  {
  lowerLeftCorner = Vector3d(9e10f, 9e10f, 9e10f);
  upperRightCorner = Vector3d(-9e10f, -9e10f, -9e10f);
  for (Mesh3d::VertexIndex i=0; i!=m.getNumberOfVertices(); ++i)
  {
    if (m.getVertices()[i].x < lowerLeftCorner.x) lowerLeftCorner.x = m.getVertices()[i].x;
    if (m.getVertices()[i].y < lowerLeftCorner.y) lowerLeftCorner.y = m.getVertices()[i].y;
    if (m.getVertices()[i].z < lowerLeftCorner.z) lowerLeftCorner.z = m.getVertices()[i].z;
    if (m.getVertices()[i].x > upperRightCorner.x) upperRightCorner.x = m.getVertices()[i].x;
    if (m.getVertices()[i].y > upperRightCorner.y) upperRightCorner.y = m.getVertices()[i].y;
    if (m.getVertices()[i].z > upperRightCorner.z) upperRightCorner.z = m.getVertices()[i].z;
  }
}
 
  
  #ifdef DEBUG_LANDSCAPE
    printf("Landscape Bounding Box: (%f, %f, %f) (%f, %f, %f)\n", lowerLeftCorner.x, lowerLeftCorner.y, lowerLeftCorner.z, upperRightCorner.x, upperRightCorner.y, upperRightCorner.z);
  #endif
  


 if (va_triangles==0)
  {
//    printf("Building va_triangles...\n");
    va_triangles = new Triangle3d[m.getNumberOfTriangles()];

    for (Mesh3d::TriangleIndex i=0; i!=m.getNumberOfTriangles(); i++)
    {
      Mesh3d::VertexIndex A = m.getFaceToVertexMap()[i].indexOfA;
      Mesh3d::VertexIndex B = m.getFaceToVertexMap()[i].indexOfB;
      Mesh3d::VertexIndex C = m.getFaceToVertexMap()[i].indexOfC; 

      va_triangles[i] = Triangle3d( m.getVertices()[A],
                                    m.getVertices()[B],
                                    m.getVertices()[C]); 
    }
  }

  #ifdef DEBUG_SSS
    printf("SSS::building sub-division into squares\n", lowerLeftCorner.x, lowerLeftCorner.y, lowerLeftCorner.z, upperRightCorner.x, upperRightCorner.y, upperRightCorner.z);
  #endif

  // subdivide bounding box along x/y plane
  // create structure
  unsigned int resolution = RESOLUTION;
  structure_.resize(resolution);  
  for (int i=0; i!=resolution; i++)
  {
    structure_[i].resize(resolution);
  }

  Vector3d cubeURC = upperRightCorner;
  Vector3d cubeLLC = lowerLeftCorner; 
  // fill structure
  for (Mesh3d::TriangleIndex i=0; i!=m.getNumberOfTriangles(); i++)
  {
    Mesh3d::VertexIndex A = m.getFaceToVertexMap()[i].indexOfA;
    Mesh3d::VertexIndex B = m.getFaceToVertexMap()[i].indexOfB;
    Mesh3d::VertexIndex C = m.getFaceToVertexMap()[i].indexOfC; 
/*    Vector3d mid_triangle = (m.getVertices()[A] + m.getVertices()[B] + m.getVertices()[C]) * (1.0f/3.0f); 

//    printf("%f , %f , %f \n" , m.getVertices()[A].x, m.getVertices()[A].y, m.getVertices()[A].z);  
    printf("%f , %f , %f \n" , mid_triangle.x, mid_triangle.y, mid_triangle.z);  

    float dx = mid_triangle.x - lowerLeftCorner.x;
    float dy = mid_triangle.y - lowerLeftCorner.y;   

    unsigned ix = (unsigned) ((resolution-1) * dx / (upperRightCorner.x-lowerLeftCorner.x));
    unsigned iy = (unsigned) ((resolution-1) * dy / (upperRightCorner.y-lowerLeftCorner.y));    
    
    #ifdef DEBUG_LANDSCAPE
      printf("dx = %f , dy = %f \n" , dx, dy);    
      printf("adding to SSS[%d][%d] \n" , ix, iy);    
    #endif
    structure_[ix][iy].push_front(m.getTriangles()[i]);

  */
    float dx = m.getVertices()[A].x - lowerLeftCorner.x;
    float dy = m.getVertices()[A].y - lowerLeftCorner.y;   

    unsigned ix = (unsigned) ((resolution-1) * dx / (upperRightCorner.x-lowerLeftCorner.x));  
    unsigned iy = (unsigned) ((resolution-1) * dy / (upperRightCorner.y-lowerLeftCorner.y));    

    structure_[ix][iy].push_back(m.getTriangles()[i]);

    dx = m.getVertices()[B].x - lowerLeftCorner.x;
    dy = m.getVertices()[B].y - lowerLeftCorner.y;   

    ix = (unsigned) ((resolution-1) * dx / (upperRightCorner.x-lowerLeftCorner.x));  
    iy = (unsigned) ((resolution-1) * dy / (upperRightCorner.y-lowerLeftCorner.y));    

    structure_[ix][iy].push_back(m.getTriangles()[i]);

    dx = m.getVertices()[C].x - lowerLeftCorner.x;
    dy = m.getVertices()[C].y - lowerLeftCorner.y;   

    ix = (unsigned) ((resolution-1) * dx / (upperRightCorner.x-lowerLeftCorner.x));  
    iy = (unsigned) ((resolution-1) * dy / (upperRightCorner.y-lowerLeftCorner.y));    

    structure_[ix][iy].push_back(m.getTriangles()[i]);

    /*
     * let box overlap     
     */
    
    float xfrac = (((float)resolution-1) * dx / (upperRightCorner.x-lowerLeftCorner.x))-(float)ix; 
    float yfrac = (((float)resolution-1) * dy / (upperRightCorner.y-lowerLeftCorner.y))-(float)iy; 
    
    bool discardLeft = false;
    bool discardRight = false;
    bool discardTop = false;
    bool discardBottom = false;
    bool discardLeftTop = false;
    bool discardLeftBottom = false;
    bool discardRightTop = false;
    bool discardRightBottom = false;

    if (ix == 0) discardLeft = true;
    if (iy == 0) discardBottom = true;
    if (ix == RESOLUTION-1) discardRight = true;
    if (iy == RESOLUTION-1) discardTop = true;

    discardLeftTop = discardLeft | discardTop;
    discardLeftBottom = discardLeft | discardBottom;    
    discardRightTop = discardRight | discardTop;
    discardRightBottom = discardRight | discardBottom;    

    if (xfrac < 0.2 && !discardLeft) structure_[ix-1][iy].push_back(m.getTriangles()[i]);
    if (xfrac > 0.8 && !discardRight) structure_[ix+1][iy].push_back(m.getTriangles()[i]);
    if (yfrac < 0.2 && !discardBottom) structure_[ix][iy-1].push_back(m.getTriangles()[i]);
    if (yfrac > 0.8 && !discardTop) structure_[ix][iy+1].push_back(m.getTriangles()[i]);

    if (xfrac < 0.2 && yfrac < 0.2 && !discardLeftBottom) structure_[ix-1][iy-1].push_back(m.getTriangles()[i]);
    if (xfrac > 0.8 && yfrac < 0.2 && !discardRightBottom) structure_[ix+1][iy-1].push_back(m.getTriangles()[i]);
    if (xfrac < 0.2 && yfrac > 0.8 && !discardLeftTop) structure_[ix-1][iy+1].push_back(m.getTriangles()[i]);
    if (xfrac > 0.8 && yfrac > 0.8 && !discardRightTop) structure_[ix+1][iy+1].push_back(m.getTriangles()[i]);



  }  

    #ifdef DEBUG_SSS
      printf("SSS::sub-division done \n");          
    #endif
  printf("Time in build SSS (in ms): %d", SDL_GetTicks()- time);
}


std::vector<Mesh3d::VertexIndex> SpatialSearchStructure::requestNearVertices(const Vector3d& v, const float max_distance) const
{
    unsigned int resolution = RESOLUTION;
    float dx = v.x-lowerLeftCorner.x;
    float dy = v.y-lowerLeftCorner.y;
    float dz = v.z-lowerLeftCorner.z;
    unsigned ix = (unsigned) (resolution * dx / (upperRightCorner.x-lowerLeftCorner.x));
    unsigned iy = (unsigned) (resolution * dy / (upperRightCorner.y-lowerLeftCorner.y));    
    unsigned iz = (unsigned) (resolution * dz / (upperRightCorner.z-lowerLeftCorner.z));  
    if (ix >= resolution-1) --ix; if (iy >= resolution-1) --iy; if (iz >= resolution-1) --iz;
    if (ix <= 0) ++ix; if (iy <= 0) ++iy; if (iz <= 0) ++iz;
    std::vector<Mesh3d::VertexIndex> ret;
    ret.resize(structure_[ix][iy].size()); // TODO???: also add neighbor cubes
//    std::copy(structure_[ix][iy].begin(),structure_[ix][iy].end(), std::back_inserter(ret));   
    return ret;  
}

std::vector<Triangle3d> SpatialSearchStructure::requestNearTriangles(Mesh m, const Vector3d& v, const float max_distance)
{
    // build triangles / normals from ASE-FaceMap
  if (va_triangles==0)
  {
    va_triangles = new Triangle3d[m.getNumberOfTriangles()];

    for (Mesh3d::TriangleIndex i=0; i!=m.getNumberOfTriangles(); i++)
    {
      Mesh3d::VertexIndex A = m.getFaceToVertexMap()[i].indexOfA;
      Mesh3d::VertexIndex B = m.getFaceToVertexMap()[i].indexOfB;
      Mesh3d::VertexIndex C = m.getFaceToVertexMap()[i].indexOfC; 

      va_triangles[i] = Triangle3d( m.getVertices()[A],
                                    m.getVertices()[B],
                                    m.getVertices()[C]); 
    }
  }

  float squared_maxdistance = max_distance*max_distance;
  std::vector<Triangle3d> ret;

   if (structure_.empty() /*|| v.z < 0.8f*/)  // z < # hack!!! because triangle under water overlap to many boxes of sss
   {

    for (Mesh3d::TriangleIndex i=0; i!=m.getNumberOfTriangles(); i++)
    {
      if ( ((va_triangles[i].A-v).squaredLength() < squared_maxdistance) ||
          ((va_triangles[i].B-v).squaredLength() < squared_maxdistance) ||
          ((va_triangles[i].C-v).squaredLength() < squared_maxdistance) )
      {
        ret.push_back(va_triangles[i]);
      }
    }
//    printf("returning all triangles!");
  } else
  {
    float dx = v.x - lowerLeftCorner.x;
    float dy = v.y - lowerLeftCorner.y;   

    float ix = ((RESOLUTION-1) * dx / (upperRightCorner.x-lowerLeftCorner.x));
    float iy = ((RESOLUTION-1) * dy / (upperRightCorner.y-lowerLeftCorner.y));  
    
    if (ix > RESOLUTION-1) ix = RESOLUTION-1;
    if (iy > RESOLUTION-1) iy = RESOLUTION-1;

    if (ix < 0) ix = 0;
    if (iy < 0) iy = 0;


    std::copy(structure_[(int)ix][(int)iy].begin(),structure_[(int)ix][(int)iy].end(), std::back_inserter(ret));       
  }


   return ret;   
}


void SpatialSearchStructure::drawDebugTriangles(const std::vector<Triangle3d>& tri)
{
  glPushMatrix();
  glPushAttrib(GL_ALL_ATTRIB_BITS);

    glDisable(GL_CULL_FACE); // ??? wieso funktionierts nicht mit cull_face
    GLfloat mat_color[] = {1.0f, 0.0f, 0.0f, 0.9f};
    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, mat_color);  
    GLfloat mat_spec[] = {1.0f, 0.0f, 0.0f, 1.0f};
    glMaterialfv(GL_FRONT, GL_SPECULAR, mat_spec);
    GLfloat mat_shine[] = {228.0f}; 
    glMaterialfv(GL_FRONT, GL_SHININESS, mat_shine);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);

    for (int i=0; i!=tri.size(); i++)
    {
      glBegin(GL_TRIANGLES);
        glVertex3f(tri[i].A.x, tri[i].A.y, tri[i].A.z);
        glVertex3f(tri[i].B.x, tri[i].B.y, tri[i].B.z);
        glVertex3f(tri[i].C.x, tri[i].C.y, tri[i].C.z);
      glEnd();
    }
    glDisable(GL_BLEND);
  glPopAttrib();
  glPopMatrix();
  return;
}