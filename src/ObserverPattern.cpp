#include "ObserverPattern.h"

void Observable::attach( Observer* o ) //MeldeAn
{
  observers_.push_back(o);
}

void Observable::detach( Observer* o ) //MeldeAb
{
  size_t n = observers_.size();
  size_t i;

  for( i = 0; i < n; ++i )
  {
    if(observers_[i] == o)
      break;
  }

  if(i < n)
    observers_.erase( observers_.begin() + i );
}

void Observable::notifyObservers() //Benachrichtige
{
  size_t n = observers_.size();
  for( size_t i = 0; i < n; ++i )
    observers_[i]->update(this);
}
