#include "Game.h"  


Game::Game():
  state_(GS_INTRO),
  inGame_(0)
{
  
}    

void Game::draw()
{

  switch(state_)
  {
    case GS_INTRO:
      intro_.draw();
    break;
    case GS_MENU:
      menu_.draw();
    break;
    case GS_INGAME:
      if (inGame_!=0 && !inGame_->isTerminated())
        inGame_->draw();
    break;
    case GS_OUTRO:
      outro_.draw();
    break;  
    default:
      printf("undefined state in %s %s", __FILE__, __LINE__);
    break;
  }
}

void Game::keyPressEvent(SDL_KeyboardEvent e)
{
  switch(state_)
  {
    case GS_INTRO:
      if (e.type == SDL_KEYDOWN)
        intro_.setTerminated(true);
    break;
    case GS_MENU:
      if (e.type == SDL_KEYDOWN)
      {
        menu_.keyPressEvent(e);      
      }      
    break;
    case GS_INGAME:
      if (inGame_!=0 && !inGame_->isTerminated())
        inGame_->keyPressEvent(e);  
    break;
    case GS_OUTRO:
      if (e.type == SDL_KEYDOWN)
        outro_.setTerminated(true);
    break;  
    default:
      printf("undefined state in %s %s", __FILE__, __LINE__);
    break;
  }
}

void Game::mouseMotionEvent(SDL_MouseMotionEvent e)
{
  switch(state_)
  {
    case GS_INTRO:
    break;
    case GS_MENU:  
    break;
    case GS_INGAME:
      if (e.type == SDL_MOUSEMOTION)
      {
        if (inGame_!=0 && !inGame_->isTerminated())
          inGame_->mouseMotionEvent(e);
      }     
    break;  
    case GS_OUTRO:
    break; 
    default:      
    break;
  }
}

void Game::mousePressEvent(SDL_MouseButtonEvent e)
{
  switch(state_)
  {
    case GS_INTRO:
        intro_.setTerminated(true);
    case GS_MENU:  
    break;
    case GS_INGAME:
        if (inGame_!=0 && !inGame_->isTerminated())
          inGame_->mousePressEvent(e);  
    break;
    case GS_OUTRO:
        outro_.setTerminated(true);
    break;  
    default:
      printf("undefined state in %s %s", __FILE__, __LINE__);
    break;
  }  
}

void Game::keyReleaseEvent(SDL_KeyboardEvent e)
{
  switch(state_)
  {
    case GS_INTRO:

    case GS_MENU:  
    break;
    case GS_INGAME:
      if (inGame_!=0 && !inGame_->isTerminated())
        inGame_->keyReleaseEvent(e);  
    break;
    case GS_OUTRO:

    break;  
    default:
      printf("undefined state in %s %s", __FILE__, __LINE__);
    break;
  }  
}

void Game::mouseReleaseEvent(SDL_MouseButtonEvent e)
{
  switch(state_)
  {
    case GS_INTRO:
        intro_.setTerminated(true);
    case GS_MENU:  
    break;
    case GS_INGAME:
      if (inGame_!=0 && !inGame_->isTerminated())
        inGame_->mouseReleaseEvent(e);  
    break;
    case GS_OUTRO:
        outro_.setTerminated(true);
    break;  
    default:
      printf("undefined state in %s %s", __FILE__, __LINE__);
    break;
  }  
}

   
void Game::updateTime(long time, long delta_t)
{ 
  switch(state_)
  {
    case GS_INTRO:
      intro_.updateTime(time, delta_t);
      if (intro_.isTerminated()) {
        state_ = GS_MENU; 
        menu_.setTerminated(false);
      }
    break;
    case GS_MENU:
      menu_.update(); 
      if (menu_.isTerminated()) { // TODO: check options/exit
        switch (menu_.mainMenu_.state_)
        {
          case 4:
            intro_.setTerminated(false);          
            state_ = GS_INTRO;
          break;
          case 5:
            if (inGame_!=0 && !inGame_->isTerminated())
            {
              inGame_->setTerminated(false);
            } else
            {
              inGame_ = new InGame();
            }
            state_ = GS_INGAME;
          break;
          case 6:
            outro_.setTerminated(false);
            state_ = GS_OUTRO;
          break;
          case 7:
          default:
            //state_ = GS_LEAVE;
            menu_.setTerminated(false);
          break;
        }
        menu_.mainMenu_.state_ = 0;
      }
    break;
    case GS_INGAME:
      srand(time);
 //     printf("InGame = %d\n",inGame_); 
      if (inGame_!=0)
      {
//        printf("InGame.dataLoaded() = %d\n",inGame_->isDataLoaded()); 
        if (!inGame_->isDataLoaded() ||  delta_t > 1000)  // b�ser hack
        {
          inGame_->updateTime(0, 0);
        } else
        {
          inGame_->updateTime(time, delta_t);
        }
        if (inGame_->isTerminated()) 
        {
          state_ = GS_MENU;          
//          printf("Deleting InGame\n"); 
          delete(inGame_);
          menu_.setTerminated(false);
        }
      }
    break;
    case GS_OUTRO:
      outro_.updateTime(time, delta_t);
      if (outro_.isTerminated()) {
        state_ = GS_MENU;
        menu_.setTerminated(false);
      }
    break;  
    case GS_LEAVE: 
      exit(0);
      break;
    default:
      printf("undefined state in %s %s", __FILE__, __LINE__);
    break;
  }
}

