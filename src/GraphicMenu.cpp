#include "GraphicMenu.h"

void GraphicMenu::draw()
{
  glPushAttrib(GL_ALL_ATTRIB_BITS);
  glPushMatrix();
  glDisable(GL_DEPTH_TEST);
  glDisable(GL_ALPHA_TEST);
  glDisable(GL_LIGHTING);
  
  backGround_.draw();
  
	glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  // "Intro"
  if (state_ == 0) 
  {
     glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
  } else
  {
    glColor4f(1.0f, 1.0f, 1.0f, 0.5f);
  }

  itemIntro_.draw();
  glEnable(GL_BLEND);
  
  // "Play"
  if (state_ == 1) 
  {
     glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
  } else
  {
    glColor4f(1.0f, 1.0f, 1.0f, 0.5f);
  }

  itemPlay_.draw();
  glEnable(GL_BLEND);

  // "Outro"
  if (state_ == 2) 
  {
     glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
  } else
  {
    glColor4f(1.0f, 1.0f, 1.0f, 0.5f);
  }

  itemOutro_.draw();
  glEnable(GL_BLEND);  
  
  // "Leave"
  if (state_ == 3) 
  {
     glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
  } else
  {
    glColor4f(1.0f, 1.0f, 1.0f, 0.5f);
  }

  itemLeave_.draw();
  glEnable(GL_BLEND);

  glDisable(GL_BLEND);
  glPopMatrix();
  glPopAttrib();
}

void GraphicMenu::keyPressEvent(SDL_KeyboardEvent e) 
{
  switch (e.keysym.sym) 
  {
    case SDLK_UP:
      --state_;
      if (state_ < 0) state_ = 3;
      break;
    case SDLK_DOWN:
      ++state_;
      if (state_ > 3) state_ = 0;
      break;
    case SDLK_RETURN:
      state_ += 4;
    default:
      break;
  }
}