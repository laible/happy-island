#include "SoundManager.h"

SoundManager::SoundManager():
	sound_loaded_(false)
{
}

ALboolean SoundManager::loadWave_(char *szWaveFile, ALuint BufferID)
{
	ALint	error;
	int size,freq;
	ALenum	format;
	ALvoid	*data;
	ALboolean loop;

	if (!szWaveFile)
		return AL_FALSE;

	alutLoadWAVFile((signed char*)szWaveFile, &format, &data, &size, &freq, &loop);
	if ((error = alGetError()) != AL_NO_ERROR)
	{
		printf("Failed to load %s\n", szWaveFile);
		return AL_FALSE;
	}

	// Copy data into ALBuffer
	alBufferData(BufferID,format,data,size,freq);
	if ((error = alGetError()) != AL_NO_ERROR)
	{
		return AL_FALSE;
	}

	// Unload wave file
	alutUnloadWAV(format,data,size,freq);
	if ((error = alGetError()) != AL_NO_ERROR)
	{
		return AL_FALSE;
	}

	return AL_TRUE;
}

bool SoundManager::soundStopped_(ALuint n)
{
  ALint source_state;
  alGetSourcei(n, AL_SOURCE_STATE, &source_state);
  
  if (source_state != AL_PLAYING) {
    return true;      
  } 
  else {
    return false;
  }
}

void SoundManager::setListenerPosition(ALfloat x, ALfloat y, ALfloat z)
{
  alListener3f(AL_POSITION, x, y, z);
  // set static sounds/background music to current listener position
  alSource3f(source[0], AL_POSITION, x, y, z);  
  alSource3f(source[2], AL_POSITION, x, y, z);
  alSource3f(source[3], AL_POSITION, x, y, z);
  alSource3f(source[4], AL_POSITION, x, y, z);
  alSource3f(source[5], AL_POSITION, x, y, z);
  alSource3f(source[6], AL_POSITION, x, y, z);
  alSource3f(source[7], AL_POSITION, x, y, z);
  alSource3f(source[8], AL_POSITION, x, y, z);	
}

void SoundManager::loadSounds()
{
	if (!sound_loaded_)
	{
	    alGenBuffers(10, alBuffers); 	// Generate Buffers
	    loadWave_("Music/HappyIsland.wav", alBuffers[0]);  // Load Wave in Buffer
	    loadWave_("Sounds/BOMB.wav", alBuffers[1]);  // Load Wave in Buffer
	    loadWave_("Sounds/Reload.wav", alBuffers[2]);  // Load Wave in Buffer
	    loadWave_("Sounds/bubbles.wav", alBuffers[3]);  // Load Wave in Buffer
	    loadWave_("Sounds/gong.wav", alBuffers[4]);  // Load Wave in Buffer
	    loadWave_("Sounds/launch.wav", alBuffers[5]);  // Load Wave in Buffer
	    loadWave_("Sounds/ocean_waves.wav", alBuffers[6]);  
	    loadWave_("Sounds/clock.wav", alBuffers[7]); 
	    loadWave_("Sounds/jaws.wav", alBuffers[8]); 
	    loadWave_("Sounds/Seagull.wav", alBuffers[9]); 
	
	    alGenSources(10, source);  // Generate Sources
	    alSourcei(source[0], AL_BUFFER, alBuffers[0]);  // Attach buffers to sources
		alSourcei(source[0], AL_LOOPING, AL_TRUE);  // set looping	
	
	    alSourcei(source[1], AL_BUFFER, alBuffers[1]);  // Attach buffers to sources
	    alSourcei(source[2], AL_BUFFER, alBuffers[2]);  // Attach buffers to sources
	    alSourcei(source[3], AL_BUFFER, alBuffers[3]);  // Attach buffers to sources
	    alSourcei(source[4], AL_BUFFER, alBuffers[4]);  // Attach buffers to sources
	    alSourcei(source[5], AL_BUFFER, alBuffers[5]);  // Attach buffers to sources
	    alSourcei(source[6], AL_BUFFER, alBuffers[6]); 
	    alSourcei(source[7], AL_BUFFER, alBuffers[7]); 
	    alSourcei(source[8], AL_BUFFER, alBuffers[8]); 
	    alSourcei(source[9], AL_BUFFER, alBuffers[9]);
	    
	    alSourcei(source[0], AL_BUFFER, alBuffers[0]);  // Attach buffers to sources
	    alSourcei(source[0], AL_LOOPING, AL_TRUE);  // set looping	    
	    alSourcei(source[1], AL_BUFFER, alBuffers[1]);  // Attach buffers to sources
	    alSourcei(source[2], AL_BUFFER, alBuffers[2]);  // Attach buffers to sources
	    alSourcei(source[3], AL_BUFFER, alBuffers[3]);  // Attach buffers to sources
	    alSourcei(source[4], AL_BUFFER, alBuffers[4]);  // Attach buffers to sources
	    alSourcei(source[5], AL_BUFFER, alBuffers[5]);  // Attach buffers to sources
	    alSourcei(source[6], AL_BUFFER, alBuffers[6]); 
	    alSourcei(source[7], AL_BUFFER, alBuffers[7]); 
	    alSourcei(source[8], AL_BUFFER, alBuffers[8]); 
	    alSourcei(source[9], AL_BUFFER, alBuffers[9]);
	    sound_loaded_ = true;
	} else
	{
		printf("Warning: trying to load sound several times.\n"); 
	}	      
}


void SoundManager::playBombSound(ALfloat x, ALfloat y, ALfloat z)  
{ 
  alSource3f(source[1], AL_POSITION, x*0.02, y*0.02, z*0.02);   // TODO: check what 0.02 is for
  alSourcePlay(source[1]); 
}

void SoundManager::stopMusic()
{
	alSourceStop(source[0]);
}

void SoundManager::playReloadSound() 
{ 
  alSourcePlay(source[2]); 
}


/////////////////////////////////////////////////
// sounds that can't be played back several times at the same time
void SoundManager::playMusic()
{
  if (soundStopped_(source[0]))
  {	
	alSourcePlay(source[0]);
  }
}

void SoundManager::playBubbleSound() 
{ 
  if (soundStopped_(source[3]))   
  {
	  alSourcePlay(source[3]);
  } 
}

void SoundManager::playOceanSound()  
{ 
  if (soundStopped_(source[6]))
  {
  	alSourcePlay(source[6]);
  } 
}

void SoundManager::playJawsSound()  
{
  if (soundStopped_(source[8]))
  {
	  alSourcePlay(source[8]);
  } 
}

void SoundManager::playClockSound()  
{ 
  if (soundStopped_(source[7]))
  {
	  alSourcePlay(source[7]);
  } 
}

void SoundManager::playSeagullSound()  
{ 
  if (soundStopped_(source[9]))
  {
	  ALfloat x, y, z;
	  alGetListener3f(AL_POSITION, &x, &y, &z);
	  alSource3f(source[0], AL_POSITION, x, y, 200);  // TODO: check if 0 is right 
	  alSourcePlay(source[9]); 
  } 
}	

//////////////////////////////////////////////////

void SoundManager::playGongSound()   
{ 
  alSourcePlay(source[4]); 
}
void SoundManager::playLaunchSound() 
{ 
  alSourcePlay(source[5]); 
}

void SoundManager::stopBubbleSound()
{
	alSourceStop(source[3]);
}

void SoundManager::stopOceanSound()
{
	alSourceStop(source[6]);
}

void SoundManager::stopSeagullSound()
{
	alSourceStop(source[8]);
}
