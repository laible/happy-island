#include "CutScene.h"

#include "FallingObject.h"

#include <stdio.h> 
#include <stdlib.h> 

RoundTransition::RoundTransition(InGame* ig) : state_(0), in_game_(ig)
{
  static int roundTransition = 0;

  bool fo = (roundTransition!=0 && roundTransition % 2 == 0); // do we have a falling object in this round?

//  printf("fo = %d/n", fo);

    // first create views and controllers
    Text3D* t3d = new Text3D(Vector3d(0, 0, 100), "ASE/nextplayer.ASE");
    Text3DView* t3dv = new Text3DView(t3d);
    Text3DController* t3dc = new Text3DController(t3d, t3dv, in_game_);
  
    std::vector<Vector3d> v;
    v.push_back(Vector3d( -865, 281, 1000 ) );
    v.push_back(Vector3d( -787, 571, 1000 ) );
    v.push_back(Vector3d( -486, 504, 1000 ) );
    v.push_back(Vector3d( -453, 738, 1000 ) );
    v.push_back(Vector3d( -286, 515, 1000 ) );
    v.push_back(Vector3d(   47, 437, 1000 ) );
    v.push_back(Vector3d( -186, 248, 1000 ) );
    v.push_back(Vector3d(   47, 215, 1000 ) );
    v.push_back(Vector3d(  225, 126, 1000 ) );
    v.push_back(Vector3d(  503, 192, 1000 ) );
    v.push_back(Vector3d(  637, 504, 1000 ) );
    v.push_back(Vector3d(  548, 749, 1000 ) );
    v.push_back(Vector3d(  581, -74, 1000 ) );
    v.push_back(Vector3d(  269,-118, 1000 ) );
    v.push_back(Vector3d(  537,-341, 1000 ) );
    v.push_back(Vector3d(  347,-478, 1000 ) );
    v.push_back(Vector3d(  637,-530, 1000 ) );
    v.push_back(Vector3d(  581,-786, 1000 ) );
    v.push_back(Vector3d(   14,-864, 1000 ) );
    v.push_back(Vector3d( -219,-252, 1000 ) );
    v.push_back(Vector3d( -442,-430, 1000 ) );
    v.push_back(Vector3d( -709,-608, 1000 ) );
    v.push_back(Vector3d( -631,-809, 1000 ) );
   

    
    //double r = (   (double)rand() / ((double)(RAND_MAX)+(double)(1)) );      
    double r = rand();

    printf("%f\n", (int)r % v.size());
    Vector3d start_pos = v[(int)r % v.size()];    

    models_.push_back(t3d);
    views_.push_back(t3dv);
    controllers_.push_back(t3dc);


    Text3D* go = new Text3D(Vector3d(0, 0, 100), "ASE/go.ASE");
    Text3DView* gov = new Text3DView(go);
    Text3DController* goc = new Text3DController(go, gov, in_game_);


    if (fo)
    {
      FallingObject* fo = new FallingObject(start_pos, "ASE/Flugzeug.ASE");
      FallingObjectView* fov = new FallingObjectView(fo);
      FallingObjectController* foc = new FallingObjectController(fo, fov, in_game_);
      

      models_.push_back(fo);
      views_.push_back(fov);
      controllers_.push_back(foc );
      goc->attachToFallingObject(fo);
    } else
    {
      goc->attachToPlayer(in_game_->getActivePlayer());
    }


    models_.push_back(go);
    views_.push_back(gov);
    controllers_.push_back(goc);


    // set current view / controller
    current_view_ = views_[0];
    current_controller_ = controllers_[0];    

    roundTransition++;
}


void Text3DView::draw()
{
  glMatrixMode (GL_MODELVIEW);
  glPushAttrib(GL_ALL_ATTRIB_BITS); 
  glDisable(GL_DEPTH_TEST);

  glPushMatrix ();
  glLoadIdentity ();
  glMatrixMode (GL_PROJECTION);
  glPushMatrix ();
  glLoadIdentity ();   

  glColor3f(1.0,1.0,1.0);
  glDisable(GL_LIGHTING);

  glRotatef(-90, 1.0, 0.0, 0.0);
  float scale = fabs(model_->getScale());
  glScaled(scale, scale, scale); 
  if (model_!=0)
    model_->getMesh()->draw();

  glPopMatrix ();
  glMatrixMode (GL_MODELVIEW);
  glPopAttrib();
  glPopMatrix ();
}

void Text3DController::updateTime(long time, long delta_t)
{  
  if (isTerminated()) return;
  
  age_ += delta_t;

  if (model_!=0)
  {
    model_->setCenter(((MovableGameObject*)in_game_->getActivePlayer())->getCenter() + Vector3d(0,0,4));
    model_->setHorizontalRotationAngle(in_game_->getActivePlayer()->getHorizontalRotationAngle());
    model_->setVerticalRotationAngle(in_game_->getActivePlayer()->getVerticalRotationAngle());
    model_->setScale(0.003*sin((float)age_/1000.0f));
  }

  if (age_ > time_to_live_) {
    // controller and view should be destroyed by garbage collector so mark them as terminated
    terminated_ = true;
 //   if (view_!=0)
   //   view_->setGarbage(true);
  }



  // camera handling depending on attachment
  if (attached_to_fo_)
  {
    // let camera look on falling object
    Camera* camera_ = in_game_->getCamera();
    camera_->set_center(fo_->getCenter());  // lookat
    camera_->set_eye(fo_->getStartPosition() * 0.5 );
    camera_->set_up(Vector3d(0,0,1));  
  } else
  {
    Player* active_player = in_game_->getActivePlayer();
    Vector3d eye_of_player = active_player->getCenter() + active_player->getUp() * active_player->getHeightToEye();    
    Vector3d eye_of_camera = eye_of_player - active_player->getDirection() * 20;  // camera base point
    Vector3d center_of_camera = eye_of_camera + active_player->getDirection();
    Vector3d up_of_camera = active_player->getUp();
    Camera* camera_ = in_game_->getCamera();
    camera_->set_center(center_of_camera);
    camera_->set_eye(eye_of_camera);
    camera_->set_up(up_of_camera);     
  }
}


PlayerWonTransition::PlayerWonTransition(InGame* ig):
  in_game_(ig),
  model_(0),
  view_(0),
  controller_(0)
{
  model_ = new Text3D(Vector3d(0,0,0), "ASE/winner.ASE");
  view_ = new Text3DView(model_);
  controller_ = new Text3DController(model_, view_, ig);
  controller_->attachToPlayer(ig->getActivePlayer());
}

void PlayerWonTransition::updateTime(long time, long delta_t)
{
  controller_->updateTime(time, delta_t);
  if (controller_->isTerminated()) 
  {
    this->setTerminated(true);
    
  }
}


void PlayerWonTransition::draw()
{
  view_->draw();
}