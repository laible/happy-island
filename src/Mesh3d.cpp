#include "SDL.h"
#include "Mesh3d.h"


#include <fstream>
#include <iostream>
#include <string>
#include <sstream>
#include "algorithm"

#define NO_VBOS
#define DEBUG_MESH3D

std::map<std::string, boost::shared_ptr<Mesh3d> > Mesh::resource_manager_ ; 


#define INFINITY 10000


// VBO Extension Definitions, From glext.h
#define GL_ARRAY_BUFFER_ARB 0x8892
#define GL_STATIC_DRAW_ARB 0x88E4
typedef void (APIENTRY * PFNGLBINDBUFFERARBPROC) (GLenum target, GLuint buffer);
typedef void (APIENTRY * PFNGLDELETEBUFFERSARBPROC) (GLsizei n, const GLuint *buffers);
typedef void (APIENTRY * PFNGLGENBUFFERSARBPROC) (GLsizei n, GLuint *buffers);
typedef void (APIENTRY * PFNGLBUFFERDATAARBPROC) (GLenum target, int size, const GLvoid *data, GLenum usage);

// VBO Extension Function Pointers
PFNGLGENBUFFERSARBPROC glGenBuffersARB = NULL;					// VBO Name Generation Procedure
PFNGLBINDBUFFERARBPROC glBindBufferARB = NULL;					// VBO Bind Procedure
PFNGLBUFFERDATAARBPROC glBufferDataARB = NULL;					// VBO Data Loading Procedure
PFNGLDELETEBUFFERSARBPROC glDeleteBuffersARB = NULL;			// VBO Deletion Procedure

bool		g_fVBOSupported = false;							// ARB_vertex_buffer_object supported?

// Based Off Of Code Supplied At OpenGL.org
bool IsExtensionSupported( char* szTargetExtension )
{
	const unsigned char *pszExtensions = NULL;
	const unsigned char *pszStart;
	unsigned char *pszWhere, *pszTerminator;

	// Extension names should not have spaces
	pszWhere = (unsigned char *) strchr( szTargetExtension, ' ' );
	if( pszWhere || *szTargetExtension == '\0' )
		return false;

	// Get Extensions String
	pszExtensions = glGetString( GL_EXTENSIONS );

	// Search The Extensions String For An Exact Copy
	pszStart = pszExtensions;
	for(;;)
	{
		pszWhere = (unsigned char *) strstr( (const char *) pszStart, szTargetExtension );
		if( !pszWhere )
			break;
		pszTerminator = pszWhere + strlen( szTargetExtension );
		if( pszWhere == pszStart || *( pszWhere - 1 ) == ' ' )
			if( *pszTerminator == ' ' || *pszTerminator == '\0' )
				return true;
		pszStart = pszTerminator;
	}
	return false;
}

Mesh3d::Mesh3d(): 
nVertices(0),
vertices(0),
normals(0),
texture_coords(0),
nTriangles(0),
va_triangles(0),
va_normals(0),
va_texture_coords(0),
nTextureStages(0),
//texture_ids(0),
map_ti_to_vi(0),
display_list_(0),
material_(DEFAULT_MATERIAL),
plane_eq_(0),
va_neighbours(0),
visible_(0)
{
	// Check For VBOs Supported
#ifndef NO_VBOS
	g_fVBOSupported = IsExtensionSupported( "GL_ARB_vertex_buffer_object" );
	if( g_fVBOSupported )
	{
		// Get Pointers To The GL Functions
		glGenBuffersARB = (PFNGLGENBUFFERSARBPROC) wglGetProcAddress("glGenBuffersARB");
		glBindBufferARB = (PFNGLBINDBUFFERARBPROC) wglGetProcAddress("glBindBufferARB");
		glBufferDataARB = (PFNGLBUFFERDATAARBPROC) wglGetProcAddress("glBufferDataARB");
		glDeleteBuffersARB = (PFNGLDELETEBUFFERSARBPROC) wglGetProcAddress("glDeleteBuffersARB");
		// Load Vertex Data Into The Graphics Card Memory
		g_pMesh->BuildVBOs();									// Build The VBOs
	}
#else /* NO_VBOS */
	g_fVBOSupported = false;
#endif

//  printf("VBO supported: %d", g_fVBOSupported);

};


//#define DEBUG_MESH3D 

/** Load ASE file  
  * Note: don't forget to break out of while loop after block has been processed
  **/
bool Mesh3d::loadFromASEFile(const char* filename, TextureMappingMode tm)
{

  // load *.ASE-file
  std::ifstream ASE_file(filename);
  if (!ASE_file.good()) 
  {
    std::cerr << "Cannot load file!\n";
    return false;
  }
  
  int geomobjectCounter = -1;
  std::string token;   
  int openBrackets = 0;
  
  while (ASE_file >> token) 
  {
    if (token == "*MATERIAL_LIST") // Level 0;
    {
      while (ASE_file >> token)
      {
        if (token == "{") openBrackets++;
        if (token == "}") openBrackets--;
        if (openBrackets == 0) break;  // go back to level 0 parsing
                   
        if (token == "*MATERIAL_COUNT")      // Level 1;
        {
           ASE_file >> nTextureStages;
           //texture_ids = new GLuint[nTextureStages];
           if (nTextureStages == 0) 
           {
             std::cout << "Warning: No Material assigned." << std::endl;
             break; // break out of *MATERIAL_LIST
           };
           if (nTextureStages > 1)
           { 
             std::cout << "Error: More than one Material assigned." << std::endl;
             break;
           }
        }
        
        if (token == "*BITMAP")
        {
          ASE_file >> texture_filename;       /// ! filename may not contain whitespaces !
          texture_filename.erase(texture_filename.length()-1, 1); // delete last char (")
          texture_filename.erase(0, 1); // delete first char (")
        }                                        
      }
    }
    
    if (token == "*GEOMOBJECT")    // Level 0;
    {
      geomobjectCounter++;
      while (ASE_file >> token)
      {
        if (token == "*MESH")      // Level 1;
        {
          while (ASE_file >> token)
          {            
            if (token == "*MESH_NUMVERTEX")
            {              
              ASE_file >> nVertices;;
              if (vertices!=0) delete[] vertices;
              if (normals!=0) delete[] normals;
              if (texture_coords!=0) delete[] texture_coords;
              vertices = new Vector3d[nVertices];
              normals = new Vector3d[nVertices];
              texture_coords = new UVTextureCoordinate[nVertices];                            
            }
            
            if (token == "*MESH_NUMFACES")
            {              
              ASE_file >> nTriangles;
              if (va_triangles!=0) delete[] va_triangles;
              if (map_ti_to_vi!=0) delete[] map_ti_to_vi;
              va_triangles = new Triangle3d[nTriangles];
              va_normals = new Triangle3d[nTriangles];
              va_texture_coords = new Triangle3dUVTextureCoords[nTriangles];
              map_ti_to_vi = new VertexIndexTriple[nTriangles];
            }
                                             
            if (token == "*MESH_VERTEX_LIST")      // Level 2
            {
              while (ASE_file >> token)
              {
                if (token == "*MESH_VERTEX")
                {
                  unsigned vertex_index;
                  float x, y, z;
                  ASE_file >> vertex_index >> x >> y >> z;                  
                  vertices[vertex_index] = Vector3d(x,y,z);
                  if (vertex_index == nVertices-1) break;
                }                                 
              }                 
            }

            if (token == "*MESH_FACE_LIST")      // Level 2
            {
              while (ASE_file >> token)
              {
                if (token == "*MESH_FACE")
                {
                  std::string temp;                                    
                  // extract face_index by chopping off the last character
                  ASE_file >> temp;
                  temp.erase(temp.length()-1, 1);  // delete last char (':')
                  std::istringstream iss(temp);
                  unsigned int triangle_index;
                  iss >> triangle_index;
                  
                  // extract vertex indizes this face consists of
                  unsigned int Ai, Bi, Ci;                  
                  ASE_file >> temp >> Ai >> temp >> Bi >> temp >> Ci;
                  map_ti_to_vi[triangle_index].indexOfA = Ai;
                  map_ti_to_vi[triangle_index].indexOfB = Bi;
                  map_ti_to_vi[triangle_index].indexOfC = Ci;
                  if (triangle_index == nTriangles-1) break;
                }                                 
              }                 
            }

            if (token == "*MESH_NUMTVERTEX")
            {
              
              ASE_file >> nTextureVertices;
             // if (nTextureVertices!=nVertices)
             //   std::cout << "Error: number of texture coordinates doesn't match number of vertices!" << std::endl;
              texture_coords = new UVTextureCoordinate[nTextureVertices];
            }               
            
            if (token == "*MESH_TVERTLIST")      // Level 2
            {
              while (ASE_file >> token)
              {
                if (token == "*MESH_TVERT")
                {                    
                  unsigned int vertex_index;
                  float U,V,W;                
                  ASE_file >> vertex_index  >> U >> V >> W;
                  texture_coords[vertex_index] = UVTextureCoordinate(U,V);
                  if (vertex_index == nTextureVertices-1) break;
                }                                 
              }                 
            }

            if (token == "*MESH_NUMTVFACES")
            {
              
              unsigned int num_texture_faces;
              ASE_file >> num_texture_faces;
              
              map_tti_to_vi.resize(nTriangles);      // resize (and construct uninitialised)
              
            }                     
            
            if (token == "*MESH_TFACELIST")      // Level 2
            {
              while (ASE_file >> token)
              {
                if (token == "*MESH_TFACE")
                {                                      
                  unsigned int triangle_index, A,B,C;                
                  ASE_file >> triangle_index >> A >> B >> C;
                  
                  map_tti_to_vi[triangle_index].indexOfA = A;
                  map_tti_to_vi[triangle_index].indexOfB = B;
                  map_tti_to_vi[triangle_index].indexOfC = C;
                  
                  if (triangle_index == nTriangles-1) break;
                }                                 
              }                 
            }

            if (token == "*MESH_NORMALS")      // Level 2
            {
              while (ASE_file >> token)
              {
                if (token == "*MESH_FACENORMAL")
                { 
                      
                  unsigned int index;
                  float x,y,z;
                  ASE_file >> index >> x >> y >> z;
                  /*
                  face_normals[index] = Vector3d(x , y , z);
                  */
                }     

                if (token == "*MESH_VERTEXNORMAL")
                {                    
                  unsigned int vertex_index;
                  float x,y,z;
                  ASE_file >> vertex_index >> x >> y >> z;
                  normals[vertex_index] = Vector3d(x , y , z);
                }                                                                                               
              }                 
            }                                                                             
          }          
        }
      }      
    } 
  }
  
  // build triangles from ASE-FaceMap
  for (TriangleIndex i=0; i!=nTriangles; i++)
  {
    va_triangles[i] = Triangle3d(vertices[map_ti_to_vi[i].indexOfA], vertices[map_ti_to_vi[i].indexOfB], vertices[map_ti_to_vi[i].indexOfC]);
    va_normals[i] = Triangle3d(normals[map_ti_to_vi[i].indexOfA], normals[map_ti_to_vi[i].indexOfB], normals[map_ti_to_vi[i].indexOfC]);
	  if (nTextureStages==1)
	  {
		  va_texture_coords[i] = Triangle3dUVTextureCoords(texture_coords[map_tti_to_vi[i].indexOfA],texture_coords[map_tti_to_vi[i].indexOfB],texture_coords[map_tti_to_vi[i].indexOfC]);
	  }
  }
  
  #ifdef DEBUG_MESH3D  
    std::cout << "==============================" << std::endl;
    std::cout << "File: " << filename << std::endl;
    std::cout << "Vertices: " << nVertices << std::endl;
    std::cout << "Triangles: " << nTriangles << std::endl;
    std::cout << "Texture: " << texture_filename << std::endl;
    std::cout << "Texture Stages: " << nTextureStages << std::endl;
    std::cout << "==============================" << std::endl;
    dump();
  #endif

  if (nTextureStages==1)
  {
    texture_ = new Texture(texture_filename.c_str());
/*


    // load from file
    SDL_Surface *image ;
    image = SDL_LoadBMP(texture_filename.c_str());
    std::cout << SDL_GetError() << std::endl;
    if (!image)
    {
      printf ( "Texture load error!");
    } else 
    {            

	    //Check that the image's width is valid and then check that the image's width is a power of 2
	    if (image->w < 1) 
      {
		    std::cout << "Error: width < 1!" << std::endl;
	    } else if ( !((image->w & (image->w-1))==0) ) 
      {
		    std::cout << "Error: texture data not power of 2!" << std::endl;
	    }    


            
      // generate Texture ids
      glGenTextures(1, texture_ids);

	    //Load the texture
	    glBindTexture(GL_TEXTURE_2D, texture_ids[0]);

	    //Generate the texture
	    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, image->w, image->h, 0, GL_BGR, GL_UNSIGNED_BYTE, image->pixels);

      // select modulate to mix texture with color for shading
      glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );

      // when texture area is small, bilinear filter the closest mipmap
      glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST );
      // when texture area is large, bilinear filter the original
      glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

      // the texture wraps over at the edges (repeat)
      glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
      glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
      
      // build our texture mipmaps
      gluBuild2DMipmaps( GL_TEXTURE_2D, 3, image->w, image->h, GL_BGR, GL_UNSIGNED_BYTE, image->pixels );

    };

*/
//    if (image) { SDL_FreeSurface(image); }
  }
    
  createDL();
  return true;
}

void Mesh3d::prepareForShadows()
{
  // build connectivity/neighbour structure of object
	unsigned int p1i, p2i, p1j, p2j;
	unsigned int P1i, P2i, P1j, P2j;

  va_neighbours = new Neighbours[nTriangles];
  for (int i=0; i!=nTriangles; i++) 
  {
    va_neighbours[i].neigh[0] = 0;
    va_neighbours[i].neigh[1] = 0;
    va_neighbours[i].neigh[2] = 0;
  }

	for(TriangleIndex i=0; i < nTriangles-1; i++)
		for(TriangleIndex j=i+1; j < nTriangles; j++)
			for(unsigned int ki=0; ki<3; ki++)
				if(!va_neighbours[i].neigh[ki])  // ???
        {
					for(unsigned int kj=0; kj<3; kj++)
          {
						p1i=ki;
						p1j=kj;
						p2i=(ki+1)%3;
						p2j=(kj+1)%3;

            switch (p1i)
            {
              case 0: p1i = map_ti_to_vi[i].indexOfA; break;
              case 1: p1i = map_ti_to_vi[i].indexOfB; break;
              case 2: p1i = map_ti_to_vi[i].indexOfC; break;
              default:
              break;
            }

            switch (p2i)
            {
              case 0: p2i = map_ti_to_vi[i].indexOfA; break;
              case 1: p2i = map_ti_to_vi[i].indexOfB; break;
              case 2: p2i = map_ti_to_vi[i].indexOfC; break;
              default:
              break;
            }

            switch (p1j)
            {
              case 0: p1j = map_ti_to_vi[j].indexOfA; break;
              case 1: p1j = map_ti_to_vi[j].indexOfB; break;
              case 2: p1j = map_ti_to_vi[j].indexOfC; break;
              default:
              break;
            }

            switch (p2j)
            {
              case 0: p2j = map_ti_to_vi[j].indexOfA; break;
              case 1: p2j = map_ti_to_vi[j].indexOfB; break;
              case 2: p2j = map_ti_to_vi[j].indexOfC; break;
              default:
              break;
            }

						P1i = ((p1i+p2i)-abs(p1i-p2i))/2;
						P2i = ((p1i+p2i)+abs(p1i-p2i))/2;
						P1j = ((p1j+p2j)-abs(p1j-p2j))/2;
						P2j = ((p1j+p2j)+abs(p1j-p2j))/2;

						if((P1i==P1j) && (P2i==P2j))  //they are neighbours
            {
							va_neighbours[i].neigh[ki] = j+1;	  
							va_neighbours[j].neigh[kj] = i+1;	  
						}
					}
				}

  // fill plane equations
  plane_eq_ = new PlaneEquation[nTriangles];
  for (int i=0; i!=nTriangles; i++) 
  {
    plane_eq_[i].a = 0;
    plane_eq_[i].b = 0;
    plane_eq_[i].c = 0;
    plane_eq_[i].d = 0;
  }

	for (TriangleIndex ti=0; ti != nTriangles; ti++)			// Loop Through All Object faces
  {
	  Vector3d v[3];

    VertexIndexTriple vit = map_ti_to_vi[ti];  // get vertice indizes forming this triangle
		v[0].x = vertices[vit.indexOfA].x;
		v[0].y = vertices[vit.indexOfA].y;
		v[0].z = vertices[vit.indexOfA].z;

		v[1].x = vertices[vit.indexOfB].x;
		v[1].y = vertices[vit.indexOfB].y;
		v[1].z = vertices[vit.indexOfB].z;

		v[2].x = vertices[vit.indexOfC].x;
		v[2].y = vertices[vit.indexOfC].y;
		v[2].z = vertices[vit.indexOfC].z;

	  plane_eq_[ti].a = v[0].y*(v[1].z-v[2].z) + v[1].y*(v[2].z-v[0].z) + v[2].y*(v[0].z-v[1].z);
	  plane_eq_[ti].b = v[0].z*(v[1].x-v[2].x) + v[1].z*(v[2].x-v[0].x) + v[2].z*(v[0].x-v[1].x);
	  plane_eq_[ti].c = v[0].x*(v[1].y-v[2].y) + v[1].x*(v[2].y-v[0].y) + v[2].x*(v[0].y-v[1].y);
	  plane_eq_[ti].d =-( v[0].x*(v[1].y*v[2].z - v[2].y*v[1].z) +
					    v[1].x*(v[2].y*v[0].z - v[0].y*v[2].z) +
					    v[2].x*(v[0].y*v[1].z - v[1].y*v[0].z) );
  }

  visible_ = new bool[nTriangles];
  for (int i=0; i!=nTriangles; i++) 
  {
    visible_[i] = true; 
  } 

}

void Mesh3d::castShadows()
{
  if (plane_eq_ == 0 || visible_==0 || plane_eq_== 0 || va_neighbours == 0) return;

  // printf("casting shadows\n");
	unsigned int	j, k, jj;
	unsigned int	p1, p2;
	Vector3d			v1, v2;
	float			side;

//  Vector3d lp = l->getCenter();

  Vector3d lp = Vector3d(0,0,1000);

	//set visual parameter

  for (TriangleIndex i=0; i != nTriangles; i++)
  {
		// check to see if light is in front or behind the plane (face plane)
		side =	plane_eq_[i].a*lp[0]+	plane_eq_[i].b*lp[1] + plane_eq_[i].c*lp[2] + plane_eq_[i].d * 1;
		if (side > 0) visible_[i] = true;
				else visible_[i] = false;
	}

	glShadeModel(GL_SMOOTH);							// Enable Smooth Shading
	glClearColor(0.0f, 0.0f, 0.0f, 0.5f);				// Black Background
	glClearDepth(1.0f);									// Depth Buffer Setup
	glClearStencil(0);									// Stencil Buffer Setup
	glEnable(GL_DEPTH_TEST);							// Enables Depth Testing
	glDepthFunc(GL_LEQUAL);								// The Type Of Depth Testing To Do
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);	// Really Nice Perspective Calculations

	glCullFace(GL_BACK);								// Set Culling Face To Back Face
	glEnable(GL_CULL_FACE);								// Enable Culling
	glClearColor(0.1f, 1.0f, 0.5f, 1.0f);				// Set Clear Color (Greenish Color)



 	glDisable(GL_LIGHTING);
	glDepthMask(GL_FALSE);
	glDepthFunc(GL_LEQUAL);

	glEnable(GL_STENCIL_TEST);
	glColorMask(0, 0, 0, 0);
	glStencilFunc(GL_ALWAYS, 1, 0xffffffff);

	// first pass, stencil operation decreases stencil value
	glFrontFace(GL_CCW);
	glStencilOp(GL_KEEP, GL_KEEP, GL_INCR);
	for (TriangleIndex i=0; i != nTriangles; i++){
		if (visible_[i])
			for (j=0;j<3;j++)
      {
				k = va_neighbours[i].neigh[j];
				if ((!k) || (!visible_[k-1]))
        {
					// here we have an edge, we must draw a polygon
					p1 = map_ti_to_vi[i][j];
					jj = (j+1)%3;
					p2 = map_ti_to_vi[i][jj];

					//calculate the length of the vector
					v1.x = (vertices[p1].x - lp[0])*INFINITY;
					v1.y = (vertices[p1].y - lp[1])*INFINITY;
					v1.z = (vertices[p1].z - lp[2])*INFINITY;

					v2.x = (vertices[p2].x - lp[0])*INFINITY;
					v2.y = (vertices[p2].y - lp[1])*INFINITY;
					v2.z = (vertices[p2].z - lp[2])*INFINITY;
					
					//draw the polygon
					glBegin(GL_TRIANGLE_STRIP);
						glVertex3f(vertices[p1].x,
									vertices[p1].y,
									vertices[p1].z);
						glVertex3f(vertices[p1].x + v1.x,
									vertices[p1].y + v1.y,
									vertices[p1].z + v1.z);

						glVertex3f(vertices[p2].x,
									vertices[p2].y,
									vertices[p2].z);
						glVertex3f(vertices[p2].x + v2.x,
									vertices[p2].y + v2.y,
									vertices[p2].z + v2.z);
					glEnd();
				}
			}
	}

	// second pass, stencil operation increases stencil value
	glFrontFace(GL_CW);
	glStencilOp(GL_KEEP, GL_KEEP, GL_DECR);
  for (TriangleIndex i=0; i!=nTriangles; i++)
  {
		if (visible_[i])
			for (j=0; j<3; j++){
				k = va_neighbours[i].neigh[j];
				if ((!k) || (!visible_[k-1]))
        {
					// here we have an edge, we must draw a polygon
					p1 = map_ti_to_vi[i][j];
					jj = (j+1)%3;
					p2 = map_ti_to_vi[i][jj];

					//calculate the length of the vector
					v1.x = (vertices[p1].x - lp[0])*INFINITY;
					v1.y = (vertices[p1].y - lp[1])*INFINITY;
					v1.z = (vertices[p1].z - lp[2])*INFINITY;

					v2.x = (vertices[p2].x - lp[0])*INFINITY;
					v2.y = (vertices[p2].y - lp[1])*INFINITY;
					v2.z = (vertices[p2].z - lp[2])*INFINITY;
					
					//draw the polygon
					glBegin(GL_TRIANGLE_STRIP);
						glVertex3f(vertices[p1].x,
									vertices[p1].y,
									vertices[p1].z);
						glVertex3f(vertices[p1].x + v1.x,
									vertices[p1].y + v1.y,
									vertices[p1].z + v1.z);

						glVertex3f(vertices[p2].x,
									vertices[p2].y,
									vertices[p2].z);
						glVertex3f(vertices[p2].x + v2.x,
									vertices[p2].y + v2.y,
									vertices[p2].z + v2.z);
					glEnd();
				}
			}
	}

	glFrontFace(GL_CCW);
  glColorMask(1, 1, 1, 1);

	//draw a shadowing rectangle covering the entire screen
	glColor4f(0.0f, 0.0f, 0.0f, 0.5f);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glStencilFunc(GL_NOTEQUAL, 0, 0xffffffff);
	glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);


  glMatrixMode (GL_MODELVIEW);
    glPushMatrix ();
    glLoadIdentity ();
    glMatrixMode (GL_PROJECTION);
    glPushMatrix ();
    glLoadIdentity ();    
    glBegin (GL_QUADS);
      glVertex3i (-1, -1, -1);
      glVertex3i (1, -1, -1);
      glVertex3i (1, 1, -1);
      glVertex3i (-1, 1, -1);
    glEnd ();    
    glPopMatrix ();
    glMatrixMode (GL_MODELVIEW);
    glPopMatrix ();


	glDisable(GL_BLEND);

	glDepthFunc(GL_LEQUAL);
	glDepthMask(GL_TRUE);
	glEnable(GL_LIGHTING);
	glDisable(GL_STENCIL_TEST);

	glShadeModel(GL_SMOOTH);

  glClear(GL_STENCIL_BUFFER_BIT);  

}


void Mesh3d::draw(Light* l)
{
  glEnable(GL_TEXTURE_2D);

  glPushMatrix();
  glPushAttrib(GL_ALL_ATTRIB_BITS);
  draw();
  glPopAttrib();
  glPopMatrix();
  glPushMatrix();
  glPushAttrib(GL_ALL_ATTRIB_BITS);
  castShadows();
  glPopAttrib();
  glPopMatrix();

  glDisable(GL_TEXTURE_2D);
}



void Mesh3d::dump()
{
/*
 for (int i=0; i!= nTriangles; i++)
 {
  std::cout << map_ti_to_vi[i].indexOfA << map_ti_to_vi[i].indexOfB << map_ti_to_vi[i].indexOfC << std::endl;
 }
*/
/*
 for (int i=0; i!= nTriangles; i++)
 {
  std::cout << i << " " << va_triangles[i].A.x << " " << va_triangles[i].A.y << " " << " " << va_triangles[i].A.z << std::endl;

  std::cout << i << " " << va_triangles[i].B.x << " " << va_triangles[i].B.y << " "  << " " << va_triangles[i].B.z << std::endl;

  std::cout << i << " " << va_triangles[i].C.x << " " << va_triangles[i].C.y << " "  << " " << va_triangles[i].C.z << std::endl;
      
 }
*/

/*
  for (int i=0; i!= nVertices; i++)
  {
    std::cout << "vertices[" << i << "] = " << vertices[i].x << " " << vertices[i].y << " " << vertices[i].z << std::endl;
    std::cout << "normals[" << i << "] = " << normals[i].x << " " << normals[i].y << " " << normals[i].z << std::endl;
    if (nTextureStages==1)
    {
      std::cout << "texture_coords[" << i << "] = " << texture_coords[i].U << " " << texture_coords[i].V << std::endl;    
    }    
  }
*/  
}


void Mesh3d::createDL()
{
  display_list_ = glGenLists(1);
  glNewList(display_list_, GL_COMPILE);
 
  // enable pointers for vertex arrays
  glEnableClientState( GL_VERTEX_ARRAY );  
  glEnableClientState( GL_NORMAL_ARRAY );

  if (nTextureStages==1)
  {
    glBindTexture(GL_TEXTURE_2D, texture_->id);
    glEnableClientState( GL_TEXTURE_COORD_ARRAY ); 
  };

  // set pointers to vertex arrays
  glVertexPointer( 3, GL_FLOAT, 0, va_triangles ); // set the vertex pointer to our vertex data
  glNormalPointer( GL_FLOAT, 0, va_normals); // Set The Vertex Pointer To Our Vertex Data
  if (nTextureStages == 1)
  {
    glTexCoordPointer( 2, GL_FLOAT, 0, va_texture_coords ); // Set The Vertex Pointer To Our TexCoord Data
  }

  // render mesh
  glDrawArrays( GL_TRIANGLES, 0, 3*nTriangles );	// Draw All Of The Triangles At Once

  // disable pointers
  if (nTextureStages==1) 
  {
    glDisableClientState( GL_TEXTURE_COORD_ARRAY ); // Disable Texture Coord Arrays
  }
  glDisableClientState( GL_NORMAL_ARRAY ); 
  glDisableClientState( GL_VERTEX_ARRAY );  // Disable Vertex Arrays 
  glEndList();
}

void Mesh3d::draw()
{   
  glPushAttrib(GL_ALL_ATTRIB_BITS);
  glMaterialfv(GL_FRONT, GL_AMBIENT, material_.ambient);  
  glMaterialfv(GL_FRONT, GL_DIFFUSE, material_.diffuse);  
  glMaterialfv(GL_FRONT, GL_SPECULAR, material_.specular);
  glMaterialfv(GL_FRONT, GL_SHININESS, material_.shininess);
/*
  Material backFace = material_;
  backFace.ambient[4] =  0.4f;
  backFace.specular[4] = 0.4f;
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_BLEND);

  

  glMaterialfv(GL_BACK, GL_AMBIENT, backFace.ambient);  
  glMaterialfv(GL_BACK, GL_DIFFUSE, backFace.diffuse);  
  glMaterialfv(GL_BACK, GL_SPECULAR, backFace.specular);
  glMaterialfv(GL_BACK, GL_SHININESS, backFace.shininess);
*/
  glEnable(GL_CULL_FACE);
  glCallList(display_list_);
  glDisable(GL_CULL_FACE);
  glPopAttrib();
}

Mesh3d::~Mesh3d()
{
  // free memory
  if( vertices )  // Deallocate Vertex Data
    delete[] vertices;    
  vertices = NULL;

  if (normals)
    delete[] normals;
  
  if( texture_coords )  // Deallocate Texture Coord Data
    delete[] texture_coords;  
  texture_coords = NULL;
  
//  if (texture_ids)
 //   delete texture_;
  
  if (va_triangles)
    delete[] va_triangles;
    
  if (va_normals)
    delete[] va_normals;

  if (va_texture_coords)
    delete[] va_texture_coords;
    
  if (map_ti_to_vi)
    delete[] map_ti_to_vi;                
 
  if(va_neighbours)
    delete[] va_neighbours;  

  if (plane_eq_)
    delete[] plane_eq_;

  if (visible_)
    delete[] visible_;

}
