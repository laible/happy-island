#include "Intro.h"

//#define DEBUG_INTRO


Intro::Intro(): bitmap_(0,0, "Textures/Intro/TitlePage.png")
{
}

void Intro::keyPressEvent(SDL_KeyboardEvent e)
{
}

void Intro::keyReleaseEvent(SDL_KeyboardEvent e)
{
}

void Intro::mouseMotionEvent(SDL_MouseMotionEvent e)
{
}

void Intro::mousePressEvent(SDL_MouseButtonEvent e)
{
}

void Intro::mouseReleaseEvent(SDL_MouseButtonEvent e)
{
}


void Intro::draw()
{
  #ifdef DEBUG_INTRO
    printf("drawing Intro...\n");
  #endif 
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT ); 
  bitmap_.draw();
}
