#include "PlayerController.h"

#include "Player.h"
#include "InGame.h"
#include "Camera.h"
#include "Rocket.h"
#include "RocketView.h"
#include "RocketController.h"

void PlayerController::keyPressEvent(SDL_KeyboardEvent e)
{
/*	
  switch (e.keysym.sym) 
  {
    case SDLK_UP:
    case SDLK_w:        
        player_->moving_forwards_ = true;

        if (player_->isJumping())
        {
          // while jumping it's not possible to change anything, so do nothing
        } else if (player_->isForward())  // player walking forward? then accelerate
        {
          #ifdef DEBUG_PLAYERCONTROLLER
            printf("accelerating forward...\n");
          #endif
          float v = player_->getPlayerPhysics()->getAbsVelocity();
          float mv = player_->getPlayerPhysics()->getAbsMaxVelocity();
          float a = player_->getPlayerPhysics()->getAbsAcceleration();
          player_->getPlayerPhysics()->setAbsVelocity(mv - (mv-v)*a);
        } else if (player_->isBackward()) // player walking backward? then break
        {    
          player_->setForward(true);
          player_->setBackward(false);
          player_->getPlayerPhysics()->stop();     
        }
      break;
    case SDLK_DOWN:
    case SDLK_s:
        player_->moving_backwards_ = true;

        if (player_->isJumping())
        {
          // while jumping it's not possible to change anything, so do nothing
          #ifdef DEBUG_PLAYERCONTROLLER
            printf("Player::PS_JUMPING\n");
          #endif
        } else if (player_->isBackward())    // player walking backward? then accelerate     
        {
          #ifdef DEBUG_PLAYERCONTROLLER
            printf("accelerating backward...\n");
          #endif
          float v = player_->getPlayerPhysics()->getAbsVelocity();
          float mv = player_->getPlayerPhysics()->getAbsMaxVelocity();
          float a = player_->getPlayerPhysics()->getAbsAcceleration();
          player_->getPlayerPhysics()->setAbsVelocity(-mv + (mv+v)*a); 
       } else if (player_->isForward())
        {          
          #ifdef DEBUG_PLAYERCONTROLLER
            printf("breaking after moving backward... ");
          #endif   
          player_->setForward(false);
          player_->setBackward(true);
          player_->getPlayerPhysics()->stop();        
        }
      break;
    case SDLK_LEFT:
    case SDLK_a:
    case SDLK_RIGHT:
    case SDLK_d:
        if (!player_->isJumping())  // player can only turn if not jumping
          player_->setTurning(true);  
      break;
  
    case SDLK_SPACE:
    case SDLK_RCTRL:
      if (!player_->isJumping())
      {
        player_->setCenter(player_->getCenter()+Vector3d(0,0,0.1f));
        player_->setJumping(true);    
        player_->getForceReciever()->setVelocity(player_->getDirection()*50+ Vector3d(0,0, 20));
      }
    break;

    default:
      break;
  }
  */ 
}

void PlayerController::keyReleaseEvent(SDL_KeyboardEvent e)
{
	/*
  #ifdef DEBUG_PLAYERCONTROLLER
    printf("key release: ");
  #endif
  switch (e.keysym.sym) 
  {
    case SDLK_UP:
    case SDLK_w:      
        player_->moving_forwards_ = false;
        if (player_->isJumping())
        {
        } else if (player_->isForward())  
        {
          player_->getPlayerPhysics()->stop();   
        } else if (player_->isBackward()) 
        {
          player_->setForward(true);
          player_->setBackward(false);
          player_->getPlayerPhysics()->stop();   
        }
      break;
    case SDLK_DOWN:
    case SDLK_s:
        player_->moving_backwards_ = false;
        if (player_->isJumping())
        {
          // while jumping it's not possible to change anything, so do nothing
        } else if (player_->isBackward())       
        {
          // set default
          player_->setForward(true);
          player_->setBackward(false);
          player_->getPlayerPhysics()->stop();   
        } else if (player_->isForward())
        {
          // set default
          player_->setForward(true);
          player_->setBackward(false); 
          player_->getPlayerPhysics()->stop();           
        }
      break;
    case SDLK_LEFT:
    case SDLK_a:
    case SDLK_RIGHT:
    case SDLK_d:
        player_->setTurning(false); 
      break;
    default:
      break;
  } 
  */
}

void PlayerController::mouseReleaseEvent(SDL_MouseButtonEvent e)
{ 
  if (shooting_power_bar_ != 0) 
  {
    float power = shooting_power_bar_->getValue();

  //  shooting_power_bar_->setGarbage(true);
  //  shooting_power_bar_->setTerminated(true);
    shooting_power_bar_ = 0;

 //   in_game_->playLaunchSound();

    if (player_->getWeaponType() == 0)
    {
      Rocket* rocket_ = new Rocket(this->getPlayer()->getCenter());
      rocket_->setLocalAABB(AABB(Vector3d(-0.5,-0.5, -0.5), Vector3d(+0.5, +0.5, +0.5)));
    
      RocketView* rocket_view_ = new RocketView(rocket_);
      Vector3d shooting_direction; 
      shooting_direction = this->getPlayer()->getDirection()*power*200;


      rocket_->getPhysics()->setVelocity(shooting_direction);
      RocketController* rocket_controller = new RocketController(rocket_, rocket_view_,in_game_);
  //    in_game_->addView(rocket_view_);
  //    in_game_->addController(rocket_controller);     
    }
  
    if (player_->getWeaponType() == 1)
    {
      Rocket* rocket_ = new Rocket(this->getPlayer()->getCenter());
      rocket_->setDeformLandscape(true);
      rocket_->setLocalAABB(AABB(Vector3d(-0.5,-0.5, -0.5), Vector3d(+0.5, +0.5, +0.5)));
    
      RocketView* rocket_view_ = new RocketView(rocket_);
      Vector3d shooting_direction; 
      shooting_direction = this->getPlayer()->getDirection()*power*200;


      rocket_->getPhysics()->setVelocity(shooting_direction);
      RocketController* rocket_controller = new RocketController(rocket_, rocket_view_,in_game_);
   //   in_game_->addView(rocket_view_);
   //   in_game_->addController(rocket_controller);  
      player_->setWeaponType(0); 
    }
  }
}

void PlayerController::mousePressEvent(SDL_MouseButtonEvent e)
{
  if (shooting_power_bar_ == 0)
  {
    shooting_power_bar_ = new ShootingPowerBar(this->getPlayer());
 //   in_game_->addView(shooting_power_bar_);
 //   in_game_->addController(shooting_power_bar_); 
  }
}

void PlayerController::mouseMotionEvent(SDL_MouseMotionEvent e)
{
	/*
  if (!player_->isJumping())
  {
    Vector3d direction = player_->getDirection();  

    player_->addToVerticalRotationAngle((float)-e.yrel * 0.2864804);
    //printf("rot: %f\n", player_->getRotationAngle());
    direction.y = sin(player_->getVerticalRotationAngle() * 0.0174532);
    direction.z = cos(player_->getVerticalRotationAngle() * 0.0174532);
    //printf("rot: %f\n", player_->getVerticalRotationAngle());

    // set new rotation_angle_
    player_->addToHorizontalRotationAngle((float)e.xrel * 0.2864804);
    //printf("rot: %f\n", player_->getRotationAngle());

    direction.x = sin(player_->getHorizontalRotationAngle() * 0.0174532);
    direction.y = cos(player_->getHorizontalRotationAngle() * 0.0174532);
    
    player_->setDirection(direction.normalisedCopy());
  }   
  */
}

void PlayerController::updateTime(long time, long delta_t)
{
/*	
  if  (in_game_->getActivePlayer() == player_)
  {
    if (player_->getWeaponType() == 0) in_game_->getHud()->drawAtomIcon = false;
    if (player_->getWeaponType() == 1) in_game_->getHud()->drawAtomIcon = true;
    in_game_->getHud()->drawEnergy = true;
    in_game_->getHud()->setEnergyValue(player_->getEnergy());
  }
  
  float dt = float(delta_t)/1000.0f;

  if (delta_t !=0)
  {
    //printf("%f\n", player_->getPlayerPhysics()->getAbsVelocity());
    // move calculated by player movement (running forward... etc.)
    Vector3d run_move = (player_->getDirection() * player_->getPlayerPhysics()->getAbsVelocity())  * dt;  
    run_move.z = 0.0f; // no z component as we don't want to fly when looking upwards

    // move caluclated by external forces    
    player_->getForceReciever()->simulate(dt);
    Vector3d extern_move = player_->getForceReciever()->getMove();     
    
    // get collision candidates in neighbourhood of player
    std::vector<Triangle3d> collision_candidates = in_game_->getSpatialSearchStructure()->requestNearTriangles(in_game_->getLandscape()->getMesh(), player_->getCenter(), 1000.0f);
    
    // resulting move by combining  non-physical correct player-movement and move inducted by physical-correct external forces
    Vector3d move = run_move + extern_move;

    // request move from Collision Server
    in_game_->getCollisionServer()->setBoundingEllipsoid(Vector3d(2,2,4));
    Vector3d response = in_game_->getCollisionServer()->requestMove(player_->getCenter(), move, collision_candidates);

    // !!! hack {
    response.normalise();
    response = response * move.length();
    // }
    player_->move(response);  

    if (in_game_->getCollisionServer()->isCollided()) 
    {
      Vector3d v = player_->getVelocity();
      player_->setVelocity(Vector3d(0,0,0));      
      player_->setJumping(false);
    }

  }

  // check if player is active and under water surface
  if (player_ == in_game_->getActivePlayer() && player_->getCenter().z < 0)
  {
     float energy = player_->getEnergy();
     energy -= dt * 10.0f;
     if (energy <= 0.0f )
     {
       player_->setEnergy(0.0f);
       player_->setDead(true);
     } else  player_->setEnergy(energy);
  }

  if (player_->moving_forwards_ || player_->moving_backwards_) {
    if (time_since_animation_change_ == 0) {
      time_since_animation_change_ = time;
    }
    if ((time - time_since_animation_change_) > time_between_keyframes_) {
      time_since_animation_change_ = time;
      
      // keyframe change
      if (player_->moving_forwards_) {
        if (++number_of_current_keyframe_ > 15) {
          number_of_current_keyframe_ = 0;
        }
      }
      else {
        if (--number_of_current_keyframe_ < 0) {
          number_of_current_keyframe_ = 15;
        }
      }
      player_->setNumberOfCurrentMesh(sequenceOfAnimation_[number_of_current_keyframe_]);
    }
  }

*/
}

void PlayerController::onNextRound()
{
  // remove power bar if there is one
  if (shooting_power_bar_ != 0) 
  {
  //  shooting_power_bar_->setGarbage(true);
    shooting_power_bar_->setTerminated(true);
    shooting_power_bar_ = 0;
  }

  // remove energy bar
  in_game_->getHud()->drawEnergy = false;
  in_game_->getHud()->drawAtomIcon = false;

  // current player stops running
  player_->getPlayerPhysics()->stop();

  player_->moving_forwards_ = player_->moving_backwards_ = false;

}