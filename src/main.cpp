#include "SDL.h"
#include "SDL_opengl.h"

// for OpenAL
#include "al.h"
#include "alc.h"
#include "alut.h"

#include <stdio.h> 
#include <stdlib.h> 
#include "Game.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#include "main.h"


Game* game;





#include <iostream>
 using std::cout;
 using std::endl;




static void repaint()
{
  game->draw();
  SDL_GL_SwapBuffers();
}

static void setup_sdl() 
{
  int videoFlags;
    const SDL_VideoInfo* video;

    if ( SDL_Init(SDL_INIT_VIDEO) < 0 ) {
        fprintf(stderr,
                "Couldn't initialize SDL: %s\n", SDL_GetError());
        exit(1);
    }

    atexit(SDL_Quit);

    video = SDL_GetVideoInfo( );

    if( !video ) {
        fprintf(stderr,
                "Couldn't get video information: %s\n", SDL_GetError());
        exit(1);
    }

    videoFlags = SDL_OPENGL;
    videoFlags |= SDL_GL_DOUBLEBUFFER;
    videoFlags |= SDL_HWPALETTE;

    if (FULLSCREEN)
    videoFlags |= SDL_FULLSCREEN;  


	// check for hardware surfaces
	printf("Hardware surfaces supported: ");    
    if (video->hw_available)
    {
      printf("yes\n");
      videoFlags |= SDL_HWSURFACE;  
    } else
    {
      printf("no\n");    	
      videoFlags |= SDL_SWSURFACE;
    }

    // check for hardware blitting
	printf("Hardware blitting supported: ");    
    if (video->blit_hw)
    {
      printf("yes\n");    	
      videoFlags |= SDL_HWACCEL;  
    } else
    {
      printf("no\n");     
	}

    /* Set the minimum requirements for the OpenGL window */
    SDL_GL_SetAttribute( SDL_GL_RED_SIZE, 5 );
    SDL_GL_SetAttribute( SDL_GL_GREEN_SIZE, 5 );
    SDL_GL_SetAttribute( SDL_GL_BLUE_SIZE, 5 );
    SDL_GL_SetAttribute( SDL_GL_DEPTH_SIZE, 16 );
    SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );
    SDL_GL_SetAttribute( SDL_GL_STENCIL_SIZE, 8);

    /* Note the SDL_floatBUF flag is not required to enable float 
     * buffering when setting an OpenGL video mode. 
     * float buffering is enabled or disabled using the 
     * SDL_GL_floatBUFFER attribute.
     */
    if( SDL_SetVideoMode( WIDTH, HEIGHT, 0, videoFlags ) == 0 ) {
        fprintf(stderr, 
                "Couldn't set video mode: %s\n", SDL_GetError());
        exit(1);
    }

  SDL_EnableKeyRepeat(SDL_DEFAULT_REPEAT_DELAY, SDL_DEFAULT_REPEAT_INTERVAL);
//  SDL_WM_GrabInput(SDL_GRAB_ON);  
//  SDL_ShowCursor(0);  // hide cursor

}

static void setup_openal()
{	
  ALCdevice *device;
  ALCcontext *context;

  // Initialize Open AL
  device = alcOpenDevice(NULL); // open default device
  if (device != NULL) {
    context=alcCreateContext(device, NULL); // create context
    if (context != NULL) {
      alcMakeContextCurrent(context); //Set active context
    }
  }
}


static void setup_opengl()
{
    float aspect = (float)WIDTH / (float)HEIGHT;

    /* Make the viewport cover the whole window */
    glViewport(0, 0, WIDTH, HEIGHT);
  
    
    glEnable(GL_TEXTURE_2D);
    glShadeModel(GL_SMOOTH);
    //glClearDepth( 1.0f ); 
    glEnable(GL_DEPTH_TEST);   
    glDepthFunc(GL_LEQUAL);
 //   glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST );     
    glEnable(GL_NORMALIZE);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);

    glClearColor(0.0f, 0.5f, 1.0f, 0.0f);
    
	  glMatrixMode(GL_PROJECTION);
	  glLoadIdentity();
	  gluPerspective(60, aspect, 1, 500000);


	  glMatrixMode(GL_MODELVIEW);
	  glLoadIdentity();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    SDL_GL_SwapBuffers();
}

static void main_loop() 
{
  static int previous_time = 0;
  SDL_Event event;
  while(1) 
  {
      // update time
      long time = SDL_GetTicks();

      #ifdef SHOW_FPS
      if (previous_time!=0)
      {
        float fps = 1/(float(SDL_GetTicks() - previous_time)/1000);
        printf("%f\n", fps);    
      }
      #endif


      game->updateTime(time, time-previous_time);

      previous_time = time;

      // process pending events 
      while( SDL_PollEvent( &event ) ) 
      {
          switch( event.type ) 
          {
            case SDL_KEYDOWN:
              game->keyPressEvent(event.key);
            break;
            case SDL_KEYUP:
              game->keyReleaseEvent(event.key);
            break;
            case SDL_MOUSEMOTION:
              game->mouseMotionEvent(event.motion);
            break;
            case SDL_MOUSEBUTTONDOWN:
              game->mousePressEvent(event.button);
            break;
            case SDL_MOUSEBUTTONUP:
              game->mouseReleaseEvent(event.button);
            break;
            case SDL_QUIT:
              exit (0);
            break;
            default:
            break;
          }
      }
    // update the screen
    repaint();
  }
}

int main(int argc, char* argv[])
{
  FULLSCREEN = false;
  SHADOW = true;

  printf("%d\n", argc);

  if (argc == 3)
  {
    if ( std::string(argv[1]) == "NOFULLSCREEN" || std::string(argv[2]) == "NOFULLSCREEN")
      FULLSCREEN = false; 

    if ( std::string(argv[1]) == "NOSHADOW" || std::string(argv[2]) == "NOSHADOW")
      SHADOW = false;     
  }

  srand( (unsigned)time( NULL ) );
  setup_sdl(); 
  setup_opengl();
  setup_openal();

  game = new Game();
  main_loop();
  return 0;
}
