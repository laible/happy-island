#include "Shark.h"
//#include "MathUtilities.h"

#include <iostream>

void Shark::setCenter(Vector3d vec)
{
  printf("Shark::Shark\n");
  center = vec;
}

void Shark::raiseDegree(double deg)
{
  degree += deg;
  if (degree >= 360) degree -= 360;
}

void Shark::draw()
{
  glPushMatrix();
  glPushAttrib(GL_ALL_ATTRIB_BITS);
  double rad = degree*0.0174532;
  glTranslated(center.x + cos(rad) * radius * 1.25, 
               center.y + sin(rad) * radius,
               center.z + surface);
    glScaled(0.5, 0.5, 0.5);
    glRotated(degree-90, 0, 0, 1);
    mesh.draw();
  glPopAttrib();
  glPopMatrix();    
}

void Shark::update(long t, long delta_t, float z)
{
  raiseDegree(delta_t * velocity);
  surface = z;
}