#include "Box.h"
#include "InGame.h"

Box::Box(const Vector3d& c, InGame* ig): 
MovableGameObject("Box", c, AABB(Vector3d(-8,-8,-8), Vector3d(8,8,8)) ),
in_game_(ig),
texture_(0)
{        
  texture_ = new Texture("Textures/Crate.png");	
  vertices_ = new Vector3d[8];
  float size = 16;
  Vector3d C0 = Vector3d(center_.x - size/2, center_.y - size/2, center_.z - size/2); // lower-left-front-Corner
  Vector3d C1 = Vector3d(center_.x + size/2, center_.y + size/2, center_.z + size/2); // higher-right-back Corner         
  // we get all vertices of the box by counting binary 
  vertices_[0] = Vector3d(C0.x, C0.y, C0.z);
  vertices_[1] = Vector3d(C0.x, C0.y, C1.z);
  vertices_[2] = Vector3d(C0.x, C1.y, C0.z);
  vertices_[3] = Vector3d(C0.x, C1.y, C1.z);
  vertices_[4] = Vector3d(C1.x, C0.y, C0.z);
  vertices_[5] = Vector3d(C1.x, C0.y, C1.z);
  vertices_[6] = Vector3d(C1.x, C1.y, C0.z);
  vertices_[7] = Vector3d(C1.x, C1.y, C1.z);
  
  smoke_trail_ = new SmokeTrail(this, 5, 200, 500, 30);
  smoke_trail_->setVelocity(1.0);
  smoke_trail_->setColor(SmokeTrail::CL_RED);
  smoke_trail_->setTimeToLive(50000);
  smoke_trail_->setDying(true);
//  in_game_->addView(smoke_trail_);
//  in_game_->addController(smoke_trail_);  
}  
