#include "Player.h"
#include "SDL.h"
#include "SDL_opengl.h"
#include <cmath>
#include "glut.h"
#include "InGame.h"
#include "PlayerPhysics.h"

#include "main.h"

Player::Player(Vector3d center, Vector3d direction, Vector3d up, InGame* ig):
MovableGameObject("Player", center, AABB(Vector3d(-1,-1,-2),Vector3d(+1,+1,+2))), 
direction_(direction), 
in_game_(ig),
energy_(100),
up_(up), 
gravity_(Vector3d(0,0,-1)),
forward_(true),
backward_(false),
jumping_(false),
turning_(false),
max_velocity_(10),
run_acceleration_(0.5),
height_to_eye_(4),
force_reciever_(0),
horizontal_rotation_angle_(0.0),
vertical_rotation_angle_(0.0),
player_physics_(new PlayerPhysics()),
weapon_type_(0),
mesh_0_00_("ASE/beaver00.ASE"),
mesh_0_01_("ASE/beaver01.ASE"),
mesh_0_02_("ASE/beaver02.ASE"),
mesh_0_03_("ASE/beaver03.ASE"),
mesh_0_04_("ASE/beaver04.ASE"),
mesh_0_05_("ASE/beaver05.ASE"),
mesh_0_06_("ASE/beaver06.ASE"),
mesh_0_07_("ASE/beaver07.ASE"),
mesh_0_08_("ASE/beaver08.ASE"),
mesh_1_00_("ASE/rabbit00.ASE"),
mesh_1_01_("ASE/rabbit01.ASE"),
mesh_1_02_("ASE/rabbit02.ASE"),
mesh_1_03_("ASE/rabbit03.ASE"),
mesh_1_04_("ASE/rabbit04.ASE"),
mesh_1_05_("ASE/rabbit05.ASE"),
mesh_1_06_("ASE/rabbit06.ASE"),
mesh_1_07_("ASE/rabbit07.ASE"),
mesh_1_08_("ASE/rabbit08.ASE"),
number_of_current_mesh_(0),
moving_forwards_(false),
moving_backwards_(false),
appearance_("beaver"),
dead_(false),
      number_of_current_keyframe_(0),
      time_since_animation_change_(0),
      time_between_keyframes_(30)

{
	
  sequenceOfAnimation_.push_back(4); sequenceOfAnimation_.push_back(5); sequenceOfAnimation_.push_back(6); sequenceOfAnimation_.push_back(7);   
  sequenceOfAnimation_.push_back(8); sequenceOfAnimation_.push_back(7); sequenceOfAnimation_.push_back(6); sequenceOfAnimation_.push_back(5);   
  sequenceOfAnimation_.push_back(4); sequenceOfAnimation_.push_back(3); sequenceOfAnimation_.push_back(2); sequenceOfAnimation_.push_back(1);   
  sequenceOfAnimation_.push_back(0); sequenceOfAnimation_.push_back(1); sequenceOfAnimation_.push_back(2); sequenceOfAnimation_.push_back(3);   
	
	
  setLocalAABB(AABB(Vector3d(-2.0, -2.0, -2.0), Vector3d(2.0,2.0,2.0)));

  force_reciever_ = new PrimitivePhysics();
  force_reciever_->addForce(Vector3d(0, 0, force_reciever_->getMass() * (-2.0))); // low gravity
  
  d_=20;

  //initialize direction
  direction_.x = sin(horizontal_rotation_angle_ * 0.0174532);
  direction_.y = cos(horizontal_rotation_angle_ * 0.0174532);
  direction_.normalise();
}

void Player::draw()
{
  glPushMatrix();

    glTranslated(center_.x, center_.y, center_.z); 

    glTranslatef(0.0, 0.0, -1.5);
    glScaled(0.2, 0.2, 0.2);
    glRotatef(horizontal_rotation_angle_ + 180.0, 0.0, 0.0, -1.0);
  
  glPushMatrix();
  glPushAttrib(GL_ALL_ATTRIB_BITS);  
    getCurrentMesh().draw();
  glPopAttrib();
  glPopMatrix();

  glPushMatrix();
  glPushAttrib(GL_ALL_ATTRIB_BITS);  
    getCurrentMesh().castShadows();
  glPopAttrib();
  glPopMatrix();
  glPopMatrix();

  return;
}

void Player::loadSequences(const std::string& base_filename)
{
}

// collision response for moveable objects
void Player::onBBCollision(MovableGameObject* collision_partner)
{
  if (collision_partner->getType() == "Player")
  { 
    Player* other = static_cast<Player*>(collision_partner);
    Vector3d my_center = this->getCenter();
    Vector3d other_center = other->getCenter();
    Vector3d me_to_other = (other_center - my_center);

    if (this->getPlayerPhysics()->getAbsVelocity() > 0) // only handle running beaver
      ((MovableGameObject*)this)->setCenter(getCenter()-me_to_other * 0.5); 

    return;
  }

  if (collision_partner->getType() == "Box")
  {
  //    in_game_->playReloadSound();
 //   collision_partner->setGarbage(true);
    weapon_type_ = 1;   
    in_game_->getHud()->drawAtomIcon = true;
  }

}

void Player::setHorizontalRotationAngle(float angle)
{  
  if (angle < 0) {
    horizontal_rotation_angle_ = 360 - angle;
  }
  else if (angle >= 360) {
    horizontal_rotation_angle_ = angle - 360;
  }
  else {
    horizontal_rotation_angle_ = angle;
  }
}

void Player::addToHorizontalRotationAngle(float delta)
{
  setHorizontalRotationAngle(horizontal_rotation_angle_ + delta);
}

float Player::getHorizontalRotationAngle()
{
  return horizontal_rotation_angle_;
}


void Player::setVerticalRotationAngle(float angle)
{  
  if (angle < 0) {
    vertical_rotation_angle_ = 360 - angle;
  }
  else if (angle >= 360) {
    vertical_rotation_angle_ = angle - 360;
  }
  else {
    vertical_rotation_angle_ = angle;
  }
  
}

void Player::addToVerticalRotationAngle(float delta)
{
  if (delta < 0 && vertical_rotation_angle_ + delta < 180)
  {
    setVerticalRotationAngle(180);
    return;
  }

  if (delta > 0 && vertical_rotation_angle_ + delta > 359)
  {
    setVerticalRotationAngle(359);
    return;
  }


  setVerticalRotationAngle(vertical_rotation_angle_ + delta);
}

float Player::getVerticalRotationAngle()
{
  return vertical_rotation_angle_;
}

// Animation
Mesh Player::getCurrentMesh()
{
  switch (number_of_current_mesh_) {
    case 1:
      if (appearance_ == "beaver") return mesh_0_01_;
      else return mesh_1_01_;
      break;
    case 2:
      if (appearance_ == "beaver") return mesh_0_02_;
      else return mesh_1_02_;
      break;
    case 3:
      if (appearance_ == "beaver") return mesh_0_03_;
      else return mesh_1_03_;
      break;
    case 4:
      if (appearance_ == "beaver") return mesh_0_04_;
      else return mesh_1_04_;
      break;
    case 5:
      if (appearance_ == "beaver") return mesh_0_05_;
      else return mesh_1_05_;
      break;
    case 6:
      if (appearance_ == "beaver") return mesh_0_06_;
      else return mesh_1_06_;
      break;
    case 7:
      if (appearance_ == "beaver") return mesh_0_07_;
      else return mesh_1_07_;
      break;
    case 8:
      if (appearance_ == "beaver") return mesh_0_08_;
      else return mesh_1_08_;
      break;
    
    default:
      if (appearance_ == "beaver") return mesh_0_00_;
      else return mesh_1_00_;
      break;         
  }
}
void Player::setNumberOfCurrentMesh(unsigned int nr)
{
  number_of_current_mesh_ = nr;
}
unsigned int Player::getNumberOfCurrentMesh()
{
  return number_of_current_mesh_;
}

// event handling
void Player::onForwardPressed()
{
	moving_forwards_ = true;
    if (isJumping())
    {
          // while jumping it's not possible to change anything, so do nothing
    } else if (isForward())  // player walking forward? then accelerate
    {
      float v = getPlayerPhysics()->getAbsVelocity();
      float mv = getPlayerPhysics()->getAbsMaxVelocity();
      float a = getPlayerPhysics()->getAbsAcceleration();
      getPlayerPhysics()->setAbsVelocity(mv - (mv-v)*a);
    } else if (isBackward()) // player walking backward? then break
    {    
      setForward(true);
      setBackward(false);
      getPlayerPhysics()->stop();     
    }
    return;	
}

void Player::onBackwardPressed()
{
    moving_backwards_ = true;
    if (isJumping())
    {
      // while jumping it's not possible to change anything, so do nothing
    } else if (isBackward())    // player walking backward? then accelerate     
    {
      float v = getPlayerPhysics()->getAbsVelocity();
      float mv = getPlayerPhysics()->getAbsMaxVelocity();
      float a = getPlayerPhysics()->getAbsAcceleration();
      getPlayerPhysics()->setAbsVelocity(-mv + (mv+v)*a); 
    } else if (isForward())
    {          
      setForward(false);
      setBackward(true);
      getPlayerPhysics()->stop();        
    } 
}

void Player::onJumpPressed()
{
  if (!isJumping())
  {
    setCenter(getCenter()+Vector3d(0,0,0.1f));
    setJumping(true);    
    getForceReciever()->setVelocity(getDirection()*50+ Vector3d(0,0, 20));
  }	
}

void Player::onForwardReleased()
{
    moving_forwards_ = false;
    if (isJumping())
    {
    } else if (isForward())  
    {
      getPlayerPhysics()->stop();   
    } else if (isBackward()) 
    {
      setForward(true);
      setBackward(false);
      getPlayerPhysics()->stop();   
    }
}

void Player::onBackwardReleased()
{
    moving_backwards_ = false;
    if (isJumping())
    {
      // while jumping it's not possible to change anything, so do nothing
    } else if (isBackward())       
    {
      // set default
      setForward(true);
      setBackward(false);
      getPlayerPhysics()->stop();   
    } else if (isForward())
    {
      // set default
      setForward(true);
      setBackward(false); 
      getPlayerPhysics()->stop();           
    }
}

void Player::onShootPressed()
{
  is_shooting_ = true;
  notifyObservers();
}

void Player::onShootReleased()
{
  is_shooting_ = false;
  notifyObservers();	
}

void Player::onViewChanged(int dx, int dy)
{
  if (!isJumping())
  {
    Vector3d direction = getDirection();  

    addToVerticalRotationAngle((float)-dy * 0.2864804);
    direction.y = sin(getVerticalRotationAngle() * 0.0174532);
    direction.z = cos(getVerticalRotationAngle() * 0.0174532);

    // set new rotation_angle_
    addToHorizontalRotationAngle((float)dx * 0.2864804);

    direction.x = sin(getHorizontalRotationAngle() * 0.0174532);
    direction.y = cos(getHorizontalRotationAngle() * 0.0174532);
    
    setDirection(direction.normalisedCopy());
    //notifyObservers();
  }  	
}


void Player::updateTime(long int time, long int delta_t)
{
  float dt = float(delta_t)/1000.0f;

  if (delta_t !=0)
  {
    //printf("%f\n", player_->getPlayerPhysics()->getAbsVelocity());
    // move calculated by player movement (running forward... etc.)
    Vector3d run_move = (getDirection() * getPlayerPhysics()->getAbsVelocity())  * dt;  
    run_move.z = 0.0f; // no z component as we don't want to fly when looking upwards

    // move caluclated by external forces    
    getForceReciever()->simulate(dt);
    Vector3d extern_move = getForceReciever()->getMove();     
    
    // get collision candidates in neighbourhood of player
    std::vector<Triangle3d> collision_candidates = in_game_->getSpatialSearchStructure()->requestNearTriangles(in_game_->getLandscape()->getMesh(), getCenter(), 1000.0f);
    
    // resulting move by combining  non-physical correct player-movement and move inducted by physical-correct external forces
    Vector3d move = run_move + extern_move;

    // request move from Collision Server
    in_game_->getCollisionServer()->setBoundingEllipsoid(Vector3d(2,2,4));
    Vector3d response = in_game_->getCollisionServer()->requestMove(getCenter(), move, collision_candidates);

    // !!! hack {
    response.normalise();
    response = response * move.length();
    // }
    center_ = center_ + response; 
 

    if (in_game_->getCollisionServer()->isCollided()) 
    {
      Vector3d v = getVelocity();
      setVelocity(Vector3d(0,0,0));      
      setJumping(false);
    }

  }

  // check if player is active and under water surface
  if (getCenter().z < 0)
  {
     float energy = getEnergy();
     energy -= dt * 10.0f;
     if (energy <= 0.0f )
     {
       setEnergy(0.0f);
       setDead(true);
     } else setEnergy(energy);
  }


  // set animation sequence
  if (moving_forwards_ || moving_backwards_) {
    if (time_since_animation_change_ == 0) {
      time_since_animation_change_ = time;
    }
    if ((time - time_since_animation_change_) > time_between_keyframes_) {
      time_since_animation_change_ = time;
      
      // keyframe change
      if (moving_forwards_) {
        if (++number_of_current_keyframe_ > 15) {
          number_of_current_keyframe_ = 0;
        }
      }
      else {
        if (--number_of_current_keyframe_ < 0) {
          number_of_current_keyframe_ = 15;
        }
      }
      setNumberOfCurrentMesh(sequenceOfAnimation_[number_of_current_keyframe_]);
    }
  }
}


