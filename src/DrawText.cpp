
#include <stdio.h>
#include <stdarg.h>

#include "DrawText.h"
#include <assert.h>
#include "SDL_opengl.h"			
#include "SDL.h"

#include <iostream>

// Util functions.  Global as they may come in useful elsewhere.

// Switches to 2d ortho projection mode.
// Use in conjunction with Exit2d()
void Go2d()
{
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();									// Store The Projection Matrix
	glLoadIdentity();								// Reset The Projection Matrix
	glOrtho(0,1024,0,768,-1,1);						// Set Up An Ortho Screen
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();									// Store old Modelview Matrix
	glLoadIdentity();								// Reset The Modelview Matrix
}


// Switches out of 2d ortho projection mode.
// Use in conjunction with Go2d()
void Exit2d()
{
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();									// Restore old Projection Matrix
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();									// Restore old Projection Matrix
}


// The single instance of our class.
CTextDrawer* CTextDrawer::mSingleton = NULL;


// Provides access to the single global CTextDrawer object.
CTextDrawer &CTextDrawer::GetSingleton()
{
	if (!mSingleton)
	{
		mSingleton = new CTextDrawer;
	}

	return *mSingleton;
}


// Call when finished.
void CTextDrawer::Destroy()
{
	if (mSingleton)
	{
		// Delete the gl display lists for each letter.
		glDeleteLists(mSingleton->mBaseListId, NUMCHARS);			// Delete All Display Lists
	
		delete mSingleton;
		mSingleton = NULL;
	}
}


CTextDrawer::CTextDrawer()
{
	mBaseListId = 0;
	mR = mG = mB = mA = 1.0f;
}



void CTextDrawer::loadFontFromBMP(const char* filename)
{
  Initialise();

  GLuint texture_id_;
  SDL_Surface *image ;
  image = SDL_LoadBMP(filename);
  std::cout << SDL_GetError() << std::endl;
  if (!image)
  {
    printf ( "Texture load error!");
  } else 
  {            
	  //Check that the image is square (width equal to height)
	  if (image->w != image->h) 
    {
		  std::cout << "Error: width doesn't match height!" << std::endl;
	  }    

	  //Check that the image's width is valid and then check that the image's width is a power of 2
	  if (image->w < 1) 
    {
		  std::cout << "Error: width < 1!" << std::endl;
	  } else if ( !((image->w & (image->w-1))==0) ) 
    {
		  std::cout << "Error: texture data not power of 2!" << std::endl;
	  }    
          
    // generate Texture ids
    glGenTextures(1, &texture_id_);

	  //Load the texture
	  glBindTexture(GL_TEXTURE_2D, texture_id_);

	  //Generate the texture
	  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, image->w, image->h, 0, GL_BGR, GL_UNSIGNED_BYTE, image->pixels);

    // select modulate to mix texture with color for shading
    glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );

    // when texture area is small, bilinear filter the closest mipmap
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST );
    // when texture area is large, bilinear filter the original
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

    // the texture wraps over at the edges (repeat)
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
    
    // build our texture mipmaps
   // gluBuild2DMipmaps( GL_TEXTURE_2D, 3, image->w, image->h, GL_BGR, GL_UNSIGNED_BYTE, image->pixels );

  };
  if (image) { SDL_FreeSurface(image); }

	mSingleton->mFontTexId = texture_id_;
}


void CTextDrawer::Initialise()		// Build Our Font Display List
{
  printf("%d :",mSingleton->mFontTexId);
	assert(mSingleton->mFontTexId);		// No font texture id set: Use TEXTDRAWER.SetFontTextureId(aFontTextureId);

	float	cx;										// Holds Our X Character Coord
	float	cy;										// Holds Our Y Character Coord
	int		loop;

	mSingleton->mBaseListId = glGenLists(NUMCHARS);				// Creating NUMCHARS Display Lists
	glBindTexture(GL_TEXTURE_2D, mSingleton->mFontTexId);
	for (loop=0; loop<NUMCHARS; loop++)
	{
		cx=((float)(loop%16))/16.0f;				// X Position Of Current Character
		cy=((float)(loop/16))/16.0f;				// Y Position Of Current Character

		glNewList(mSingleton->mBaseListId+loop, GL_COMPILE);	// Start Building A List
			glBegin(GL_QUADS);						// Use A Quad For Each Character
				glTexCoord2f(cx,1-cy-0.0625f);		// Texture Coord (Bottom Left)
				glVertex2i(0,0);					// Vertex Coord (Bottom Left)
				glTexCoord2f(cx+0.0625f,1-cy-0.0625f);// Texture Coord (Bottom Right)
				glVertex2i(16,0);					// Vertex Coord (Bottom Right)
				glTexCoord2f(cx+0.0625f,1-cy);		// Texture Coord (Top Right)
				glVertex2i(16,16);					// Vertex Coord (Top Right)
				glTexCoord2f(cx,1-cy);				// Texture Coord (Top Left)
				glVertex2i(0,16);					// Vertex Coord (Top Left)
			glEnd();								// Done Building Our Quad (Character)
			glTranslatef(10.0f,0,0);				// Move To The Right Of The Character
		glEndList();								// Done Building The Display List
	}
}


// Sets the colour of the text.
void CTextDrawer::SetColor(float aR, float aG, float aB, float aA)
{
	mR = aR;
	mG = aG;
	mB = aB;
	mA = aA;
}


/*
INFO From siggraph:

The GL_INTENSITY texture format is a good texture format for textures containing glyphs
because the texture format is compact. The intensity texture format replicates the single
intensity component value in the red, green, blue, and alpha channels. When rendering
colored glyphs, use the GL_MODULATE texture environment and set the current color to the
desired glyph color.
*/

bool CTextDrawer::PrintText(float x, float y, const char *fmt, ...)	
{

  //printf("printing text\n");
	assert(mBaseListId != 0);

	char		text[1024];							// Holds Our String
	va_list		ap;									// Pointer To List Of Arguments

	if (fmt == NULL)
		return true;

	// Parse The String For Variables
	va_start(ap, fmt);							
	vsprintf(text, fmt, ap);
	va_end(ap);

  glPushAttrib(GL_ALL_ATTRIB_BITS);

  glShadeModel(GL_SMOOTH);							// Enable Smooth Shading
	glClearColor(0.0f, 0.0f, 0.0f, 0.5f);				// Black Background
	glClearDepth(1.0f);									// Depth Buffer Setup
	glEnable(GL_DEPTH_TEST);							// Enables Depth Testing
	glDepthFunc(GL_LEQUAL);								// The Type Of Depth Testing To Do
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);	// Really Nice Perspective Calculations
	glEnable(GL_LIGHT0);								// Enable Default Light (Quick And Dirty)
	glEnable(GL_LIGHTING);								// Enable Lighting
	glEnable(GL_COLOR_MATERIAL);



	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, mFontTexId);



	Go2d();

	glTranslated(x, y, 0);							// Position The Text (0,0 - Top Left)

	// Note: the 128 below refers to the font texture containing 2 font sets.
	// Use 0 for the first set, or 128 for the second set.
	glListBase(mBaseListId - ' ' + 128);			// Font bitmaps starts at ' ' (space/32).

	glColor4f(mR, mG, mB, mA);			// Set text colour.

	glCallLists(strlen(text), GL_BYTE, text);		// Write The Text To The Screen

	Exit2d();


  glPopAttrib();
	return true;
}

