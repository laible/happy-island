#include "Light.h"

void Light::on()
{
  glLightfv(GL_LIGHT0, GL_POSITION, position);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
}

void Light::off()
{
  glDisable(GL_LIGHTING);
  glDisable(GL_LIGHT0);
}