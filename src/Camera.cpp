#include "Camera.h"

#include <cmath>

#include "Player.h"

#include "SDL.h"
#include "SDL_opengl.h"

void Camera::activate()
{
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(eye.x, eye.y, eye.z, center.x, center.y, center.z, up.x, up.y, up.z); 
}

void Camera::attach_to_player(Player* player)
{
  current_player_ = player;
  height_offset_ = 4;
  distance_offset_ = 20;  
}
