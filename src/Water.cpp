#include "SDL_opengl.h"
#include "SDL.h"
#include "Water.h"
#include <iostream>

Water::Water(Vector3d a, Vector3d b, Vector3d c, Vector3d d, char* tile)
:A(a),B(b),C(c),D(d), 
 tile_texture_(tile)
{
}

void Water::setHeight(float h) 
{
  A.z = B.z = C.z = D.z = h;
}

void Water::addToHeight(float d)
{
  B.z = C.z = D.z = (A.z += d);
}

void Water::updateTime(long time, long delta_t)
{
  setHeight(sin(time/1000.0f)); // frequency = 1 hz
}    

void Water::draw()
{
  
  glPushAttrib(GL_ALL_ATTRIB_BITS);
    glDisable(GL_CULL_FACE); // ??? wieso funktionierts nicht mit cull_face
    GLfloat mat_color[] = {1.0f, 1.0f, 1.0f, 0.9f};
    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, mat_color);  
    GLfloat mat_spec[] = {0.0f, 1.0f, 1.0f, 1.0f};
    glMaterialfv(GL_FRONT, GL_SPECULAR, mat_spec);
    GLfloat mat_shine[] = {128.0f}; 
    glMaterialfv(GL_FRONT, GL_SHININESS, mat_shine);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);
    if (tile_texture_.id !=0)
    {
      // infinit plane
      glDisable(GL_TEXTURE_2D);
      glDisable(GL_LIGHTING);
      glColor3f(0.07f, 0.04f, 0.2f);
      glBegin(GL_QUADS);
      glNormal3d(0, 0, 1);
      glVertex3d(-1000000, -1000000, A.z-1000);
      glNormal3d(0, 0, 1);
      glVertex3d(-1000000, 1000000, A.z-1000);
      glNormal3d(0, 0, 1);
      glVertex3d(1000000, 1000000, A.z-1000);
      glNormal3d(0, 0, 1);
      glVertex3d( 1000000, -1000000, A.z-1000);
      glEnd();  

      glEnable(GL_LIGHTING);
      glEnable(GL_TEXTURE_2D);
      glBindTexture(GL_TEXTURE_2D, tile_texture_.id);
      glBegin(GL_QUADS);
      glNormal3d(0, 0, 1);
      glTexCoord2d(0 , 0);
      glVertex3d(A.x, A.y, A.z);
      glNormal3d(0, 0, 1);
      glTexCoord2d(1 , 0);
      glVertex3d(B.x, B.y, B.z);
      glNormal3d(0, 0, 1);
      glTexCoord2d(1 , 1);
      glVertex3d(C.x, C.y, C.z);
      glNormal3d(0, 0, 1);
      glTexCoord2d(0 , 1);
      glVertex3d(D.x, D.y, D.z);
      glEnd();  

    } else
    {
      glBegin(GL_QUADS);
      glNormal3d(0, 0, 1);
      glVertex3d(A.x, A.y, A.z);
      glVertex3d(B.x, B.y, B.z);
      glVertex3d(C.x, C.y, C.z);
      glVertex3d(D.x, D.y, D.z);
      glEnd();  
    }
  glDisable(GL_BLEND);
  glPopAttrib();
}


