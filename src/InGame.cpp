#include "InGame.h"
#include <iostream>

#include "AABBView.h"
#include "AABB.h"

#include "PrimitivePhysics.h"

#include "RocketView.h"
#include "RocketController.h"

#include "glut.h"

#include "ShootingPowerBar.h"

#include "Particle.h"

#include "DrawText.h"

#include "Palms.h"

#include "Shark.h"

#include "FallingObject.h"

#include "Box.h"

#include "Bitmap.h"

#include <map>

#include "main.h"


// return a random integer in the range [0, n)
// from KM p135
int nrand(int n)
{
  /*if (n <= 0 || n > RAND_MAX)
    throw domain_error("Argument to nrand is out of range");*/

  const int bucket_size = RAND_MAX / n;
  int r;

  do r = rand() / bucket_size;
  while (r >= n);

  return r;
}

InGame::InGame()
:
active_player_index_(0),
skybox_(0),
landscape_(0),
water_(0),
camera_(0),
sun_light_(0),
active_player_controller_(0),
sss_(0),
round_transition_(0),
player_won_transition_(0),
round_(0),
hud_(0),
loading_(0,0,"Textures/loading.PNG"),
data_loaded_(false),
ig_state_(PLAYING)
{
}


void InGame::loadLevelData()
{
  
  camera_ = new Camera(-9000, -16000, 2000, 0, 0, 2000, 0, 0, 1),
  sun_light_ = new Light(0.0, 0.0, 1000.0);

  hud_ = new Hud();

  collision_server_.setInGame(this);

 // CTextDrawer::GetSingleton().loadFontFromBMP("Textures/Font.bmp");

  // preloading...
  Mesh temp1("ASE/Flugzeug.ASE");
  Mesh temp2("ASE/nextplayer.ASE");
  Mesh temp3("ASE/go.ASE");


  sun_light_->on();  

  skybox_ = new SkyDome("ASE/skydome.ASE");

  landscape_ = new Landscape("ASE/insel.ASE");
  sss_ = new SpatialSearchStructure();
  sss_->build(landscape_->getMesh());

  water_ = new Water(Vector3d(-1600, -1600, 0),Vector3d(-1600,  1600, 0),Vector3d(1600, 1600, 0),Vector3d(1600, -1600, 0), "Textures/water.PNG");


  PalmCreater* palm_factory = new PalmCreater();
  if (SHADOW) palm_factory->prepareForShadows();

 // Shark* shark = new Shark(Vector3d(-550, 18, -18));

 // SharkController* shark_controller = new SharkController(shark, water_);


  palms_.push_back(palm_factory->createPalm(Vector3d(0, -866, 13.5),0));
  palms_.push_back(palm_factory->createPalm(Vector3d(-5, -861, 13.5),150));
  palms_.push_back(palm_factory->createPalm(Vector3d(35, 149, 27.5),45));
  palms_.push_back(palm_factory->createPalm(Vector3d(89, 141, 26.5),90));
  palms_.push_back(palm_factory->createPalm(Vector3d(142, 158, 27.5),180));
  palms_.push_back(palm_factory->createPalm(Vector3d(-286, 385, 26),270));
  palms_.push_back(palm_factory->createPalm(Vector3d(566, 400, 17.5),0));
  palms_.push_back(palm_factory->createPalm(Vector3d(-926, 322, 13), 0)); 
  palms_.push_back(palm_factory->createPalm(Vector3d(345, -568, 25), 0));
  palms_.push_back(palm_factory->createPalm(Vector3d(-330, 1006, 16), 270));

  printf("bis einschl. palme geladen\n");


  printf("  createPlayers(2);\n");
  createPlayers(2);

 // addView(new Bitmap(240, 700, "Textures/energy.PNG"));
 

  printf("shark und water\n");
 // addShark(shark);






 // addView(round_);
  views_.push_front(skybox_);  // must be drawn first because of disabled depth buffer

  // controller handels events

//  addController(shark_controller);

  // SET UP SOUND
  sound_manager_.loadSounds();

  #ifdef DEBUG_INGAME
    printf("done\n");
  #endif

  data_loaded_ = true;
  sound_manager_.playMusic();
  sound_manager_.playGongSound();
  round_ = new Round();
  round_->startRound(30000); 
}


void InGame::createPlayers(int nPlayers)
{
  for(int i=0; i!=nPlayers; i++)
  {
    printf("create player %d\n", i);
    Player* player;
    switch(i)
    {
      case 0:
        player = new Player(Vector3d(881, 936, 50), Vector3d(-1, 0, 0), Vector3d(0, 0, 1), this);        
      break;
      case 1:
        player = new Player(Vector3d(-931, 348, 50), Vector3d(-1, 0, 0), Vector3d(0, 0, 1), this);  
        player->appearance_ = "rabbid";
      break;
      case 2:
        player = new Player(Vector3d(776, 919, 50), Vector3d(-1, 0, 0), Vector3d(0, 0, 1), this);   
      break;
      case 3:
        player = new Player(Vector3d(937, -730, 50), Vector3d(-1, 0, 0), Vector3d(0, 0, 1), this);   
        player->appearance_ = "rabbid";
      break;

      default:
      break;
    }

    printf("shadows\n");
    collision_server_.registerMovableGameObject(player);
    if (SHADOW) player->prepareForShadows();

    printf("add\n");
    PlayerController* player_controller = new PlayerController(player, this);
    addPlayer(player, player_controller);
  }

}

void InGame::onLeaveInGame()
{
  setTerminated(true);  
  sound_manager_.stopMusic();
  active_player_index_ = 0;
  delete(skybox_);
  delete(landscape_);
  delete(water_);
  delete(camera_);
  delete(sun_light_);
  delete(active_player_controller_);
  delete(sss_);
  delete(round_transition_);
  delete(player_won_transition_);
  delete(round_);
  delete(hud_);  
  player_controllers_.clear();
  players_.clear();
  tick_recievers_.clear();
  views_.clear();
  data_loaded_ = false;
  ig_state_ = PLAYING;
}



void InGame::keyPressEvent(SDL_KeyboardEvent e) 
{
  if (!data_loaded_ || this->isTerminated()) return;

  if (e.keysym.sym == SDLK_ESCAPE) {
    onLeaveInGame();
    return;
  }

  if (!round_->isTerminated())
  {
	  switch (e.keysym.sym) 
	  {
	    case SDLK_UP:
	    	players_[0]->onForwardPressed();   
	      break;
	    case SDLK_DOWN:
	    	players_[0]->onBackwardPressed();   
	      break;
	    default:
	      break;
	  }
  }
}

void InGame::mouseMotionEvent(SDL_MouseMotionEvent e) 
{
  if (!data_loaded_ || this->isTerminated()) return;
  if (!round_->isTerminated())
  {  	
  	players_[0]->onViewChanged(e.xrel,e.yrel);
  }  
}

void InGame::keyReleaseEvent(SDL_KeyboardEvent e) 
{
  if (!data_loaded_ || this->isTerminated()) return;
  if (!round_->isTerminated())
  {
	  switch (e.keysym.sym) 
	  {
	    case SDLK_UP:
	    	players_[0]->onForwardReleased();   
	      break;
	    case SDLK_DOWN:
	    	players_[0]->onBackwardReleased();   
	      break;
	    default:
	      break;
	  }  	
  }   
}

void InGame::mouseReleaseEvent(SDL_MouseButtonEvent e) 
{
  if (!data_loaded_ || this->isTerminated()) return;
  if (!round_->isTerminated())
    active_player_controller_->mouseReleaseEvent(e);
}

void InGame::mousePressEvent(SDL_MouseButtonEvent e) 
{
  if (!data_loaded_ || this->isTerminated()) return;
  if (!round_->isTerminated())
    active_player_controller_->mousePressEvent(e);
}

void InGame::updateTime(long time, long delta_t)
{   
  if (data_loaded_)
  {    
 //   printf("time: %d\n", round_->timeLeftMS);
 //   printf("delta_t %d\n", delta_t);


  // hack: box_of_goody_ should be 0 but doesn't become zero
  /*    if ( !box_of_goody_.isVisible())
      {
        plantBox();
      }
*/
    // check if one of the players are dead
    for (std::vector<Player*>::iterator it = players_.begin(); it!= players_.end(); it++)
    {
      if ( (*it)->isDead() )  
      {
        onPlayerDied( (*it) );
        break;
      }    
    }

    // check if round transition has ended this timer event
    if (ig_state_ == ROUNDTRANSITION)
    {
      if ( round_transition_->isTerminated())
      {
        printf("round transition ended\n");

        delete(round_transition_);
        round_transition_ = 0;
        ig_state_ = PLAYING;
        this->nextPlayer();
        sound_manager_.playGongSound();
        // turn off sounds from previous player
		sound_manager_.stopBubbleSound();
		sound_manager_.stopOceanSound();
		sound_manager_.stopSeagullSound();
        round_->startRound(30000);
      }
    }


    // check if round has terminated
    if (round_->isTerminated())  {
      onRoundTerminated();
    }



    for (std::list<ITimedObject*>::iterator it = tick_recievers_.begin(); it!=tick_recievers_.end(); it++)
    {

      if (*it!=NULL)
      {
        (*it)->updateTime(time, delta_t);
      } else
      {
      //  printf("Error: *it == NULL ");  TODO: BUG!!!!
      }
    }  
    
    
	// update player entities	
    for (std::vector<Player*>::iterator it = players_.begin(); it!=players_.end(); it++) {
      (*it)->updateTime(time, delta_t);
    }    
    

	water_->updateTime(time, delta_t);

    if (ig_state_ == ROUNDTRANSITION)  // eventuell andere cameraf�hrung in cutscenen
    {
     
    } else  // normales spiel
    {
      
      Vector3d eye_of_player = active_player_controller_->getPlayer()->getCenter() + active_player_controller_->getPlayer()->getUp() * active_player_controller_->getPlayer()->getHeightToEye();    
      Vector3d eye_of_camera = eye_of_player - active_player_controller_->getPlayer()->getDirection() * 20;  // camera base point
      Vector3d center_of_camera = eye_of_camera + active_player_controller_->getPlayer()->getDirection();
      Vector3d up_of_camera = active_player_controller_->getPlayer()->getUp();
      camera_->set_center(center_of_camera);
      camera_->set_eye(eye_of_camera);
      camera_->set_up(up_of_camera);    
    }

    collision_server_.checkForAABBCollision();

    skybox_->center = active_player_controller_->getPlayer()->getCenter();  

    round_->updateTime(time, delta_t);
    updateOpenAL();
  }
}

void InGame::updateOpenAL()
{
  // update OpenAL
  // place listener at camera
  //  Vector3d eye_of_player = player_controller_->getPlayer()->getCenter() + player_controller_->getPlayer()->getUp() * player_controller_->getPlayer()->getHeightToEye();
  sound_manager_.setListenerPosition(camera_->get_center().x, camera_->get_center().y, camera_->get_center().z);
      
  float directionvect[6];
  directionvect[0] = active_player_controller_->getPlayer()->getDirection().x*0.02;  
  directionvect[0] = active_player_controller_->getPlayer()->getDirection().y*0.02;  
  directionvect[0] = active_player_controller_->getPlayer()->getDirection().z*0.02;  
  directionvect[0] = active_player_controller_->getPlayer()->getUp().x*0.02;  
  directionvect[0] = active_player_controller_->getPlayer()->getUp().y*0.02;  
  directionvect[0] = active_player_controller_->getPlayer()->getUp().z*0.02;  
  alListenerfv(AL_ORIENTATION, directionvect);

  // you hear bubbles under the ocean
  if (active_player_controller_->getPlayer()->getCenter().z < 0) 
  {
      sound_manager_.playBubbleSound();
  }

  // you hear the waves near the ocean
  else if (active_player_controller_->getPlayer()->getCenter().z < 10) 
  {
      sound_manager_.playOceanSound();     
  }  

  // remember player 5sec before new round
  // doesn't work, maybe the buffer is full already!!!
  if (round_->clock_) 
  {
      sound_manager_.playClockSound();  
  }

  // near shark the jaws theme is played
  float player_to_shark = (Vector3d(-550, 18, -18) - active_player_controller_->getPlayer()->getCenter()).length();
  if (player_to_shark < 300.0f) 
  {
      sound_manager_.playJawsSound();    
  }

  // sometimes you hear a seagull crying
  int n = nrand(1000);
  if (n == 500) 
  {
      sound_manager_.playSeagullSound();
  }

}



void InGame::onExplosion(Vector3d center, Explosion_Type et)
{
  sound_manager_.playBombSound(center.x, center.y, center.z);

  for (std::vector<Player*>::iterator it = players_.begin(); it != players_.end(); it++)
  {
    Vector3d explosion_to_player = (*it)->getCenter() - center;
    float distance = explosion_to_player.length();

    if ( distance <= 50.0f)
    {      
      ((MovableGameObject*)(*it))->setCenter((*it)->getCenter()+Vector3d(0,0,1.0));
      explosion_to_player.normalise();
      explosion_to_player.z += 2;
      float power = 1000.0f / distance;
      if (power > 60) power = 60;
      (*it)->getForceReciever()->setVelocity(explosion_to_player.normalisedCopy() * power);

    switch(et)
    {
      case ATOM_BOMB:
        (*it)->setEnergy((*it)->getEnergy()-10);
      break;
      case AIRPLANE:
        (*it)->setEnergy((*it)->getEnergy()-10);
      break;
      default:
      break;
    };


    }
  }

  // remove palm if hit by explosion
  for (std::vector<Palm*>::iterator it = palms_.begin(); it != palms_.end(); it++)
  {
    Vector3d explosion_to_palm = (*it)->getCenter() - center;
    float distance = explosion_to_palm.length();
   
   float explosion_radius;
    
    switch(et)
    {
      case ATOM_BOMB:
        explosion_radius = 200;
      break;
      case AIRPLANE:
        explosion_radius = 200;
      break;
      default:
        explosion_radius = 50;
      break;
    };


    if ( distance <= explosion_radius)
    {
      (*it)->setVisible(false);   
      ParticleSystem* ps = new ParticleSystem((*it)->getCenter(), 1.0f); 
	  addParticleSystem(ps);
    }    
  }

}

void InGame::onRoundTerminated()
{   
  active_player_controller_->onNextRound();
  if (round_transition_ == 0)  // currently not in transition?
  {

    round_->setTerminated(false);
    round_transition_ = new RoundTransition(this);
    ig_state_ = ROUNDTRANSITION;

//    addView(round_transition_);
//    addController(round_transition_);
  }
}

void InGame::plantBox()
{
  std::vector<Vector3d> v;
  v.push_back(Vector3d( -865, 281, 80 ) );
  v.push_back(Vector3d( -787, 571, 80 ) );
  v.push_back(Vector3d( -486, 504, 80 ) );
  v.push_back(Vector3d( -453, 738, 100 ) );
  v.push_back(Vector3d( -286, 515, 80 ) );
  v.push_back(Vector3d(   47, 437, 80 ) );
  v.push_back(Vector3d( -186, 248, 80 ) );
  v.push_back(Vector3d(   47, 215, 80 ) );
  v.push_back(Vector3d(  225, 126, 80 ) );
  v.push_back(Vector3d(  503, 192, 80 ) );
  v.push_back(Vector3d(  637, 504, 80 ) );
  v.push_back(Vector3d(  548, 749, 80 ) );
  v.push_back(Vector3d(  581, -74, 80 ) );
  v.push_back(Vector3d(  269,-118, 80 ) );
  v.push_back(Vector3d(  537,-341, 80 ) );
  v.push_back(Vector3d(  347,-478, 80 ) );
  v.push_back(Vector3d(  637,-530, 80 ) );
  v.push_back(Vector3d(  581,-786, 80 ) );
//  v.push_back(Vector3d(   14,-864, 80 ) );  too hard to get
  v.push_back(Vector3d( -219,-252, 100 ) );
  v.push_back(Vector3d( -442,-430, 80 ) );
  v.push_back(Vector3d( -709,-608, 80 ) );
  v.push_back(Vector3d( -631,-809, 80 ) );
  

  
  //double r = (   (double)rand() / ((double)(RAND_MAX)+(double)(1)) );      
  double r = rand();

 // printf("%f\n", (int)r % v.size());
  Vector3d pos = v[(int)r % v.size()];    


  printf("creating new box at position " );
  Vector3d::DUMP(pos);
  
 // box_of_goody_.setVisible(true);
//  box_of_goody_.setCenter(pos);

//  collision_server_.registerMovableGameObject(&box_of_goody_);
 // addView(&box_of_goody_);

}

void InGame::onPlayerDied(Player* victim)
{

}


void InGame::onPlayerWon(Player* winner)
{
  ig_state_ = PLAYERWONTRANSITION;
  if (player_won_transition_ == 0)
  {
    player_won_transition_ = new PlayerWonTransition(this);
//    addView(player_won_transition_);
//    addController(player_won_transition_);
  }
  round_->startRound(888888);

}


void InGame::draw()
{
  if (data_loaded_)
  {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT); 

    // draw scene
    glPushAttrib(GL_ALL_ATTRIB_BITS);

    sun_light_->on();

    // set up view matrix
    camera_->activate();

	landscape_->draw();
	
	// draw player models	
    for (std::vector<Player*>::iterator it = players_.begin(); it!=players_.end(); it++) {
      (*it)->draw();
    }
    	
    // draw palm	
    for (std::vector<Palm*>::iterator it = palms_.begin(); it!=palms_.end(); it++) {
      (*it)->draw();
    }

	
    for (std::vector<ParticleSystem*>::iterator it = particle_systems_.begin(); it!=particle_systems_.end(); it++)
    {
    	(*it)->draw();
    }
	
    // draw water last in scene
	water_->draw();
	
    // after scene has been draw, draw Head Up Display
    hud_->draw();

    glPopAttrib();


  } else // !data_loaded_
  {
    //loading_.draw();    
    //SDL_GL_SwapBuffers();
    loadLevelData();
    //this->nextPlayer();
    updateTime(0,0);  // hack as we have a big delta_t after the long loading time
  }
  
}

void InGame::addParticleSystem(ParticleSystem* ps)
{
	particle_systems_.push_back(ps);
}

