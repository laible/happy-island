#include "SDL_opengl.h"			
#include "SDL.h"
#include "Particle.h"

#include <iostream>

#include <cmath>
#include <stdlib.h>

#ifndef M_PI
#define M_PI 3.14159f
#endif


// NOTE! particle systems handle termination automaticly


SimpleParticleSystem::SimpleParticleSystem( Vector3d center)
:center_(center)
{
	float	phi;
	float	theta;

	timeToLive = 1000;
	age = 0;

	numParticles = 1000;
	particles = new Particle [ numParticles ];

	for( int i = 0; i < numParticles; i++)
	{
		particles[i].position.x = center_.x;
		particles[i].position.y = center_.y;
		particles[i].position.z = center_.z;

		phi = 2*M_PI*((float)(500 - rand() % 1000))/ 500.0f;
		theta = 2*M_PI*((float)(500 - rand() % 1000))/ 500.0f;
		particles[i].velocity.x = (float) ( sin(phi)*cos(theta) ) * 60.0f;
		particles[i].velocity.y = (float) sin(theta) * 60.0f;
		particles[i].velocity.z = (float) ( cos(phi)*cos(theta) ) * 100.0f;

		age = 0;
	}
}


void SimpleParticleSystem::draw( void )
{
  glPushAttrib(GL_ALL_ATTRIB_BITS);
	if( particles == NULL )
		return;

	glDisable(GL_TEXTURE_2D);
	glDisable(GL_LIGHTING);
	glColor3f(1, .5, 0);
	glPointSize(3.0);
	glBegin(GL_POINTS);
	for( int i = 0; i < numParticles; i++)
	{
		glVertex3f(particles[i].position.x, particles[i].position.y, particles[i].position.z);
	}
	glEnd();
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_LIGHTING);
  glPopAttrib();
}

void SimpleParticleSystem::updateTime( long time, long delta_t )
{
  age += delta_t;
	if(age > timeToLive)
	{
    	this->setTerminated(true);
    	this->setVisible(false);
		delete [] particles;
		particles = NULL;
		return;
	}

	for( int i = 0; i < numParticles; i++)
	{
		particles[i].position.x += particles[i].velocity.x * delta_t/1000.0f;
		particles[i].position.y += particles[i].velocity.y * delta_t/1000.0f;
		particles[i].position.z += particles[i].velocity.z * delta_t/1000.0f + (0.5f*(-5.0f)*age*age/1000/1000);
	}
}

ParticleSystem::ParticleSystem( Vector3d center, float size) : 
SimpleParticleSystem(center),
texture_("Textures/explosion.PNG", NO_MIPMAPS)
{
	size_ = size;
}


void ParticleSystem::draw()
{
	if(particles == NULL )
		return;
  glPushAttrib(GL_ALL_ATTRIB_BITS);

  glDisable(GL_CULL_FACE); // ??? wieso funktionierts nicht mit cull_face	
	glColor4f( 1.0f, 0.4f, 0.1f, 0.7f );
  GLfloat mat_color[] = { 1.0f, 0.4f, 0.1f, 0.7f };
  glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, mat_color);  
  GLfloat mat_spec[] = { 1.0f, 0.4f, 0.1f, 0.7f };
  glMaterialfv(GL_FRONT, GL_SPECULAR, mat_spec);
  GLfloat mat_shine[] = {128.0f}; 
  glMaterialfv(GL_FRONT, GL_SHININESS, mat_shine);



  // use alpha channel to mask out the unwanted bits
  glEnable(GL_BLEND);
  //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE);

  // make sure the depth buffer is only updated for the unmasked pixels 
  glEnable(GL_ALPHA_TEST);
  glAlphaFunc(GL_GREATER, 0);

  // get the modelview matrix
  float mat[16];
  glGetFloatv(GL_MODELVIEW_MATRIX, mat);

  // get the right and up vectors
  Vector3d right, up;

  right.x = mat[0] * size_;
  right.y = mat[4] * size_;
  right.z = mat[8] * size_;

  up.x = mat[1] * size_;
  up.y = mat[5] * size_;
  up.z = mat[9] * size_;

  // select the texture
  glBindTexture(GL_TEXTURE_2D, texture_.id);
  glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

  Vector3d pos;

  // draw particles as single quads
  glBegin(GL_QUADS);
  for( int i = 0; i < numParticles; i++)
  {
		memcpy(&pos, &particles[i].position, sizeof(pos));
		//glVertex3f(particles[i].position.x, particles[i].position.y, particles[i].position.z);

		// bottom left corner
		glTexCoord2f(0.0, 0.0); 
		glVertex3f(pos.x-(right.x+up.x),pos.y-(right.y+up.y),pos.z-(right.z+up.z) );
   
		// bottom right corner
		glTexCoord2f(1.0, 0.0);
		glVertex3f(pos.x+(right.x-up.x),pos.y+(right.y-up.y),pos.z+(right.z-up.z) );

		// top right corner
		glTexCoord2f(1.0, 1.0); 
		glVertex3f(pos.x+(right.x+up.x),pos.y+(right.y+up.y),pos.z+(right.z+up.z) );

		// top left corner
		glTexCoord2f(0.0, 1.0); 
		glVertex3f(pos.x-(right.x-up.x),pos.y-(right.y-up.y),pos.z-(right.z-up.z) );
	}

  glEnd();

  glDisable(GL_ALPHA_TEST);
  glDisable(GL_BLEND);
  glPopAttrib();
}

void ParticleSystem::updateTime( long time, long delta_t )
{
  size_ *= 1.01f;
	SimpleParticleSystem::updateTime( time, delta_t );
}


SmokeTrail::SmokeTrail( MovableGameObject *obj, float si, unsigned int eR, unsigned int mP, float radius ):
ParticleSystem(obj->getCenter(), si), 
subject(obj), 
maxParticles(mP),
emissionRate(eR),
texture_("Textures/puff.png"),
radius_(radius),
dying_(false)
{
  //printf("SmokeTrail::SmokeTrail\n");

 
	subject = obj;

	timeToLive = 2000;  // zeit in der immer noch particel ausgestrahlt werden auch nachdem das subject tot ist
	age = 0;

	particles = new Particle [ maxParticles ];
	current = 0;
	numParticles = 0;
	timeAccumulator = 0.0f;
}


void SmokeTrail::draw( void )
{
	if( isTerminated() || particles == NULL )
		return;
	
 glColor4f( 0.6f, 0.6f, 0.6f, 0.5f );

  switch(color_)
  {
    case CL_BLUE:
      glColorMask(false, false, true, true);      
    break;
    case CL_RED:
      glColorMask(true, false, false, true);      
      glColor4f( 0.6f, 0.6f, 0.6f, 0.7f );
    break;
    case CL_WHITE:
    default:
      glColorMask(true, true, true, true);     
    break;
  }

 

  // use alpha channel to mask out the unwanted bits
  glEnable(GL_BLEND);
  //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE);
  glDepthMask(false);
  // make sure the depth buffer is only updated for the unmasked pixels 
  glEnable(GL_ALPHA_TEST);
  glAlphaFunc(GL_GREATER, 0);


  // get the modelview matrix
  float mat[16];
  glGetFloatv(GL_MODELVIEW_MATRIX, mat);

  // get the right and up vectors
  Vector3d right, up;

  right.x = mat[0] * size_;
  right.y = mat[4] * size_;
  right.z = mat[8] * size_;

  up.x = mat[1] * size_;
  up.y = mat[5] * size_;
  up.z = mat[9] * size_;

  // select the texture
  glBindTexture(GL_TEXTURE_2D, texture_.id);
  glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);



  // draw particles as single quads
  glBegin(GL_QUADS);
  for( int i = 0; i < numParticles; i++)
  {
		//memcpy(&pos, &particles[i].position, sizeof(pos));
      Vector3d pos = particles[i].position;

		// bottom left corner
		glTexCoord2f(0.0, 0.0); 
		glVertex3f(pos.x-(right.x+up.x),pos.y-(right.y+up.y),pos.z-(right.z+up.z) );
   
		// bottom right corner
		glTexCoord2f(1.0, 0.0);
		glVertex3f(pos.x+(right.x-up.x),pos.y+(right.y-up.y),pos.z+(right.z-up.z) );

		// top right corner
		glTexCoord2f(1.0, 1.0); 
		glVertex3f(pos.x+(right.x+up.x),pos.y+(right.y+up.y),pos.z+(right.z+up.z) );

		// top left corner
		glTexCoord2f(0.0, 1.0); 
		glVertex3f(pos.x-(right.x-up.x),pos.y-(right.y-up.y),pos.z-(right.z-up.z) );
	}

  glEnd();
  glColorMask(true, true, true, true);
  glDepthMask(true);
  glDisable(GL_ALPHA_TEST);
  glDisable(GL_BLEND);
}

void SmokeTrail::updateTime( long time, long delta_t )
{

  if (age > timeToLive)
  {
    	setVisible(false);
    	setTerminated(true);
	  	delete [] particles;
	  	particles = NULL;
	  	return;
  }

  //printf("updating Smoke\n");
  float	phi;
  float	theta;
  int		i;
  int		newParticles;

  float dt = (float)(delta_t) / 1000.0f;
  
  center_ = subject->getCenter();

  if (dying_)
  {
    age += delta_t;     
  //  printf("dying: %d %d", age, timeToLive);
  }

  timeAccumulator += dt;
  newParticles = (int)( timeAccumulator * emissionRate );
  timeAccumulator -= newParticles/emissionRate;

  for(  i = 0; i < newParticles; i++)
  {
	  // Note: current is a cyclic pointer into the particle array
	  // when it reaches the end, it starts again at 0, overwriting
	  // the oldest particles.
	  // maxParticles is the size of the array.
	  // numParticles is how much of the array is filled, will be equal
	  // to maxParticles except when just starting up.
	  //
	  current++;
	  if(current >= maxParticles)
		  current = 0;
	  else
	  {
		  if(current > numParticles)
			  numParticles++;
	  }

	  particles[current].position = center_;


	  phi = 2*M_PI*((float)(500 - rand() % 1000))/ 500.0f;
	  theta = 2*M_PI*((float)(500 - rand() % 1000))/ 500.0f;
	  particles[current].velocity.x = velocity_ *  radius_ * ( (float)sin(phi)*(float)cos(theta) );
	  particles[current].velocity.y = velocity_ * radius_ * (float)sin(theta);
	  particles[current].velocity.z = velocity_ *  radius_ * ( (float)cos(phi)*(float)cos(theta) ) ;
  }

  for( i = 0; i < numParticles; i++)
  {
	  particles[i].position.x += particles[i].velocity.x * dt;
	  particles[i].position.y += particles[i].velocity.y * dt;
	  particles[i].position.z += particles[i].velocity.z * dt;
  }
}




