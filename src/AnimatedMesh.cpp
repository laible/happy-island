#include "AnimatedMesh.h"

#include <fstream>

#include <iostream>
#include <cstdlib>

void strreverse(char* begin, char* end) {
	
	char aux;
	
	while(end>begin)
	
		aux=*end, *end--=*begin, *begin++=aux;
	
}
	
void itoa(int value, char* str, int base) {
	
	static char num[] = "0123456789abcdefghijklmnopqrstuvwxyz";
	
	char* wstr=str;
	
	int sign;
	

	
	// Validate base
	
	if (base<2 || base>35){ *wstr='\0'; return; }
	

	
	// Take care of sign
	
	if ((sign=value) < 0) value = -value;
	

	
	// Conversion. Number is reversed.
	
	do *wstr++ = num[value%base]; while(value/=base);
	
	if(sign<0) *wstr++='-';
	
	*wstr='\0';
	

	
	// Reverse string
	
	strreverse(str,wstr-1);
	
}

AnimatedMesh::AnimatedMesh(const std::string& base_filename)
{
  printf("AnimatedMesh::AnimatedMesh\n");
  FrameIndex fi = 0;
  SequenceIndex si = 0;
  char buffer1[20];
  itoa(si, buffer1, 10);

  char buffer2[20];
  itoa(fi, buffer2, 10);

  std::string currentFileName = base_filename + "_" + buffer1 + "_" + buffer2 + ".ASE";
  std::cout << currentFileName << std::endl;
//  std::ifstream file(currentFileName.c_str());

  do  // read frames of current sequence
  { 
    

  char buffer1[20];
  itoa(si, buffer1, 10);

  char buffer2[20];
  itoa(fi, buffer2, 10);
    std::string currentFileName = base_filename + "_" + buffer1 + "_" + buffer2 + ".ASE";
    std::ifstream file(currentFileName.c_str());
    if (file.good())
    {
      //frame laden
      sequences_.resize(si+1);  
      sequences_[si].push_back( Mesh3d(currentFileName.c_str()));
      fi++;
    } else 
    {
      if (fi == 0) break;
      printf("found sequence with %d frames", fi);
      fi = 0;
      si++;      
    }
   } while (true);
   printf("found %d sequence", si);
}

void AnimatedMesh::draw()
{
  if (currentSequence_ <= sequences_.size() && sequences_[currentSequence_].size() <= currentFrame_)
  {
    sequences_[currentSequence_][currentFrame_].draw();
  } else
  {
    printf("Error: out of bounds! in %s %d", __FILE__, __LINE__);
  }
}

void AnimatedMesh::updateTime(long time)
{
  // check if animation is running
  if (running_)
  {
    long delta_t = sequence_start_time_ - time; // time since last update
    // calculate new frame index
    float frame_difference = 1000*(1/fps_);  // time in ms we spend for one step
    FrameIndex delta_frames = (FrameIndex)(delta_t / frame_difference);  // number of frames that already passed since last update
    currentFrame_ = (currentFrame_ + delta_frames) % sequences_[currentSequence_].size();  // set new index modulo number of frames in sequence
  }
}
