#include "Outro.h"

//#define DEBUG_OUTRO


Outro::Outro(): image_(0,0,"Textures/Outro/Outro.png")
{
}

void Outro::keyPressEvent(SDL_KeyboardEvent e)
{
}

void Outro::keyReleaseEvent(SDL_KeyboardEvent e)
{
}

void Outro::mouseMotionEvent(SDL_MouseMotionEvent e)
{
}

void Outro::mousePressEvent(SDL_MouseButtonEvent e)
{
}

void Outro::mouseReleaseEvent(SDL_MouseButtonEvent e)
{
}


void Outro::draw()
{
  #ifdef DEBUG_OUTRO
    printf("drawing Outro...\n");
  #endif 
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT ); 
  image_.draw();
}
