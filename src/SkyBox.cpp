#include "SkyBox.h"

SkyDome::SkyDome(std::string filename):
skydome_(filename.c_str())
{  
}

void SkyDome::draw()
{
  glPushAttrib(GL_ALL_ATTRIB_BITS);
  glPushMatrix();
    glDepthMask(false);
    glTranslated(center.x, center.y, center.z); 
    
    GLfloat mat_color[] = {1.0f, 1.0f, 1.0f, 1.0f};
    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, mat_color);  
    GLfloat mat_spec[] = {0.0f, 0.0f, 0.0f, 0.0f};
    glMaterialfv(GL_FRONT, GL_SPECULAR, mat_spec);
    GLfloat mat_shine[] = {0.0f}; 
    glMaterialfv(GL_FRONT, GL_SHININESS, mat_shine);
  //  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_DEPTH_TEST);
    glDisable(GL_LIGHTING);
    glColor3f(1.0, 1.0, 1.0);
    skydome_.draw();  
    glDepthMask(true);
  glPopMatrix();
  glPopAttrib();
}
