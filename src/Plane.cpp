#include "Plane.h"

// construct from origin and normal vector
Plane::Plane(const Vector3d& origin, const Vector3d& normal)
{
  // copy normal and origin vector
  this->normal = normal;
  this->origin = origin;
  equation[0] = normal.x;
  equation[1] = normal.y;
  equation[2] = normal.z;
  equation[3] = -(normal.x*origin.x+normal.y*origin.y+normal.z*origin.z);    
}

// construct from triangle
Plane::Plane(const Vector3d& p1, const Vector3d& p2, const Vector3d& p3)
{
  normal = (p2-p1).crossProduct(p3-p1);
  normal.normalise();
  origin = p1;  
  equation[0] = normal.x;
  equation[1] = normal.y;
  equation[2] = normal.z;
  equation[3] = -(normal.x*origin.x+normal.y*origin.y+normal.z*origin.z);    
}

// check if Plane is facing into same direction as v
bool Plane::isFrontFacingTo(const Vector3d& v) const
{
  double dot = normal.dotProduct(v);
  return (dot<=0);
}

// returns the signed distance to p
float Plane::signedDistanceTo(const Vector3d& p) const
{
  return (p.dotProduct(normal)) + equation[3];
}
