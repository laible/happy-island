#include "ShootingPowerBar.h"

#include "SDL_opengl.h"

void ShootingPowerBar::draw()
{
  glPushMatrix();
  glPushAttrib(GL_ALL_ATTRIB_BITS);
   
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);
    glColor3f(1.0, 1.0, 1.0);
    glDisable(GL_LIGHTING);


    glTranslatef(player_->getCenter().x, player_->getCenter().y, player_->getCenter().z);  
    glRotatef(player_->getHorizontalRotationAngle() - 90.0, 0.0, 0.0, -1.0);
    glRotatef(player_->getVerticalRotationAngle() - 270 , 0.0, -1.0,  0.0);
    
    //glTranslatef(0.0, 0.0, 0.0);
    glScaled(value_, value_, value_);



    //glRotatef(-30.0f, 0.0, 1.0, 0.0);
    glScaled(1.0f, 1.0f, 0.2f);
    
  /*	glPointSize(5.0);
    glBegin(GL_LINES);
      glVertex3f(0,0,0);
      glVertex3f(d.x, d.y, d.z);
    glEnd();*/
    mesh_.draw();
    glDisable(GL_BLEND);
  glPopMatrix();  
  glPopAttrib();
}

void ShootingPowerBar::updateTime(long time, long delta_t)
{
  value_ += 0.0005f * delta_t;  // it takes 2 seconds to reach maximum (1.0f)
  if (value_ > 1.0f) value_ = 1.0f;
}