#include "CollisionServer.h"
  
#include "Plane.h"  
#include "MathUtilities.h"
#include "InGame.h"

Vector3d CollisionServer::requestMove(const Vector3d& position, const Vector3d& move, const std::vector<Triangle3d>& tri_list)
{
  nearest_distance_ = 999999.99999f; // sentinel
  collision_found_ = false;
  position_ = position;
  move_ = move;
  
  findNearestCollidingTriangle(tri_list);

  if (collision_found_) 
  {

    Vector3d ePos = realToEllipse_(position);
    Vector3d eIS = realToEllipse_(intersection_point_);
    Vector3d eMove = realToEllipse_(move);
    Vector3d ePlaneNormal = (eIS-ePos).normalisedCopy();
    // project velocity vector on plane (through origin)
    float proj_length = ePlaneNormal.dotProduct(eMove.normalisedCopy());    
    eMove = (eMove.normalisedCopy() - ePlaneNormal * proj_length) /*+ Vector3d(0,0,0.01)*/;
    if (eMove.length()<=0.5) return Vector3d(0,0,0); else
      return ellipseToReal_(eMove);

  //  return Vector3d(0,0,0);
  }
  else return move;

  return Vector3d(move.x, move.y, 0);
}

void CollisionServer::checkForAABBCollision()
{
  for (std::list<MovableGameObject*>::iterator it1 = mgo_list_.begin(); it1!=mgo_list_.end(); it1++)
  {      
    for (std::list<MovableGameObject*>::iterator it2 = mgo_list_.begin(); it2!=mgo_list_.end(); it2++)
    {
      if (it1 == it2) continue; // don't test bb on self-collision
      
      // check if global bb overlap
      if ((*it1)->getGlobalAABB().intersectBoundingBox((*it2)->getGlobalAABB()))
      {
        // fire event  (note: problem if event handling wants to remove himself/or other from mgo_list_)
        (*it1)->onBBCollision(*it2);
        (*it2)->onBBCollision(*it1);
      }  
    }
  }

  // remove garbage from list
  for (std::list<MovableGameObject*>::iterator it = mgo_list_.begin(); it!=mgo_list_.end(); it++)
  {

  }   

}

/*
Vector3d CollisionServer::collideWithWorld(const Vector3d& pos, const Vector3d& move)
{
  float unitScale = 10.0;
  float veryCloseDistance = 0.005 * unitScale;
  if (collisionRecursionDepth>5) return pos;
  move_ = move;
      Vector3d destinationPoint = position + move;
    Vector3d newBasePoint = position;
    if (nearest_distance_ >= 0.1)
    {
      Vector3d m = move;
      m = m.normalisedCopy()*(nearest_distance_-0.1);
      newBasePoint = position + m;
      m.normalise();
      intersection_point_ -= m*0.1;
      Plane slidingPlane(intersection_point_, (newBasePoint-intersection_point_).normalisedCopy() );
      Vector3d newDestinationPoint = destinationPoint - slidingPlane.signedDistanceTo(destinationPoint)*(newBasePoint-intersection_point_).normalisedCopy();
      Vector3d newMoveVector = newDestinationPoint - intersection_point_;
      if (newMoveVector.length() < 0.1) return newBasePoint;
      collisionRecursionDepth++;
      return 
    }
}
*/

void CollisionServer::findNearestCollidingTriangle(const std::vector<Triangle3d>& tri_list)
{
//   printf("tri_list: %d\n",tri_list.size());
   for (int i=0; i!=tri_list.size(); i++)
   {        
     checkTriangle(tri_list[i].A, tri_list[i].B, tri_list[i].C);
   }
} 

bool CollisionServer::checkTriangle(const Vector3d& P1, const Vector3d& P2, const Vector3d& P3)
{


  float t=0; // time
  bool foundCollision=false; // indicates if there was a collision with this triangle
  // convert vectors to ellipsoid space
  Vector3d eP1 = realToEllipse_(P1);
  Vector3d eP2 = realToEllipse_(P2); 
  Vector3d eP3 = realToEllipse_(P3);
  
  Vector3d eMove = realToEllipse_(move_);  
  Vector3d ePosition = realToEllipse_(position_);
  Vector3d eCollisionPosition;
    
  // create plane in ellipsoid space 
  Plane eTrianglePlane(eP1, eP2, eP3);
 
//  if (eTrianglePlane.isFrontFacingTo(eVelocity.normalisedCopy()))
//  {
    
    float eSignedDistance = eTrianglePlane.signedDistanceTo(ePosition);
//    printf("signed distance:%f!\n", eSignedDistance);
    
    float eNormalDotVelocity = eTrianglePlane.normal.dotProduct(eMove);
    
    float t0, t1;
    bool isEmbeddedInPlane = false;

    if (eNormalDotVelocity == 0.0)      // Sphere is travelling parallel to plane. 
    {
      // 2 Cases:
      // Sphere is not embedded in plane (distance >= 1.0) -> there is no collision possible
      // Sphere is embedded in plane -> intersects with whole intervall      
      if (fabs(eSignedDistance) >= 1.0) 
      {
        return false;
      } else
      {        
        isEmbeddedInPlane = true;
        t0 = 0.0;
        t1 = 1.0;
      }      
    }  else  // Sphere is not parallel -> there exists an intersection with plane
    {
       t0 = (-1.0f - eSignedDistance)/eNormalDotVelocity;
       t1 = (1.0f - eSignedDistance)/eNormalDotVelocity;
       if (t0>t1) std::swap<float>(t0,t1);
       
       if (t0 > 1.0f || t1 < 0.0f)  // check if both t are outside of [0..1]  
       {
        // no collision possible
        return false;
       }
       
       if (t0 < 0.0) t0 = 0.0;
       if (t1 < 0.0) t1 = 0.0;       
       if (t0 > 1.0) t0 = 1.0;
       if (t1 > 1.0) t1 = 1.0;                     
    }
    // Here we have a collision of the sweep sphere and the plane.  
    
    // Check for intersection in triangle:
    if (!isEmbeddedInPlane)
    {
      // first calculate intersection point with plane
      Vector3d eIntersectionPoint = (ePosition-eTrianglePlane.normal) + eMove*t0;  // projection on plane
      if (MathUtilities::isVector3dInsideTriangle3d(eIntersectionPoint, eP1, eP2, eP3))
      {
        foundCollision = true;
        t = t0;
        eCollisionPosition = eIntersectionPoint;
      }      
    }
    
    // check against edges and vertices
    if (!foundCollision)
    {
      float eVelocitySquaredLength = eMove.squaredLength();
      
      
      // check against vertices
      float eA, eB, eC;
      eA = eVelocitySquaredLength;
      eB = 2.0f * eMove.dotProduct(ePosition-eP1);      
      eC = (eP1-ePosition).squaredLength() - 1.0f;
      double newT;      
      if (MathUtilities::getLowestRoot(eA, eB, eC, t, &newT))
      {
        t = (float)newT;
        foundCollision = true;
        eCollisionPosition = eP2;
      } 
      
      eB = 2.0f * eMove.dotProduct(ePosition-eP2);      
      eC = (eP2-ePosition).squaredLength() - 1.0f;
      if (MathUtilities::getLowestRoot(eA, eB, eC, t, &newT))
      {
        t = (float)newT;
        foundCollision = true;
        eCollisionPosition = eP2;
      } 

      eB = 2.0f * eMove.dotProduct(ePosition-eP3);      
      eC = (eP3-ePosition).squaredLength() - 1.0f;
      if (MathUtilities::getLowestRoot(eA, eB, eC, t, &newT))
      {
        t = (float)newT;
        foundCollision = true;
        eCollisionPosition = eP3;
      } 
      
      // check against edges
      Vector3d eEdge1To2 = eP2 - eP1;
      Vector3d eEdgeVToP = eP1 - ePosition;
      float eEdgeSquaredLength = eEdge1To2.squaredLength();
      float eEdgeDotEMove = eEdge1To2.dotProduct(eMove);
      float eEdgeDotVToP = eEdge1To2.dotProduct(eEdgeVToP);
      
      eA = eEdgeSquaredLength * -eVelocitySquaredLength + eEdgeDotEMove*eEdgeDotEMove;
      eB = eEdgeSquaredLength * (2*eMove.dotProduct(eEdgeVToP)) - 2*eEdgeDotEMove*eEdgeDotVToP;
      eC = eEdgeSquaredLength * (1-eEdgeVToP.squaredLength()) + eEdgeDotVToP * eEdgeDotVToP;
      
      if (MathUtilities::getLowestRoot(eA, eB, eC, t, &newT))
      {
        // check if intersection is within line segment
        float f = (eEdgeDotEMove*(float)newT-eEdgeDotVToP)/eEdgeSquaredLength;
        if (f >= 0.0f && f<= 1.0f)
        {
          t = (float)newT;
          foundCollision = true;
          eCollisionPosition = eP1 + eEdge1To2*f;
        }
      }
      
      Vector3d eEdge2To3 = eP3 - eP2;
      eEdgeVToP = eP2 - ePosition;
      eEdgeSquaredLength = eEdge2To3.squaredLength();
      eEdgeDotEMove = eEdge2To3.dotProduct(eMove);
      eEdgeDotVToP = eEdge2To3.dotProduct(eEdgeVToP);
      
      eA = eEdgeSquaredLength * -eVelocitySquaredLength + eEdgeDotEMove*eEdgeDotEMove;
      eB = eEdgeSquaredLength * (2*eMove.dotProduct(eEdgeVToP)) - 2*eEdgeDotEMove*eEdgeDotVToP;
      eC = eEdgeSquaredLength * (1-eEdgeVToP.squaredLength()) + eEdgeDotVToP * eEdgeDotVToP;
      
      if (MathUtilities::getLowestRoot(eA, eB, eC, t, &newT))
      {
        // check if intersection is within line segment
        float f = (eEdgeDotEMove*(float)newT-eEdgeDotVToP)/eEdgeSquaredLength;
        if (f >= 0.0f && f<= 1.0f)
        {
          t = (float)newT;
          foundCollision = true;
          eCollisionPosition = eP1 + eEdge2To3*f;
        }
      }      

      
      Vector3d eEdge3To1 = eP1 - eP3;
      eEdgeVToP = eP2 - ePosition;
      eEdgeSquaredLength = eEdge3To1.squaredLength();
      eEdgeDotEMove = eEdge3To1.dotProduct(eMove);
      eEdgeDotVToP = eEdge3To1.dotProduct(eEdgeVToP);
      
      eA = eEdgeSquaredLength * -eVelocitySquaredLength + eEdgeDotEMove*eEdgeDotEMove;
      eB = eEdgeSquaredLength * (2*eMove.dotProduct(eEdgeVToP)) - 2*eEdgeDotEMove*eEdgeDotVToP;
      eC = eEdgeSquaredLength * (1-eEdgeVToP.squaredLength()) + eEdgeDotVToP * eEdgeDotVToP;
      
     if (MathUtilities::getLowestRoot(eA, eB, eC, t, &newT))
      {
        // check if intersection is within line segment
        float f = (eEdgeDotEMove*(float)newT-eEdgeDotVToP)/eEdgeSquaredLength;
        if (f >= 0.0f && f<= 1.0f)
        {
          t = (float)newT;
          foundCollision = true;
          eCollisionPosition = eP1 + eEdge3To1*f;
        }
      }                      
    }
  
    if (foundCollision == true)
    {
      float distToCollision = t * eMove.length();
      // if we haven't found a collision yet or we found a collision nearer to previously found then update CollisionInfo
      if (collision_found_ == false || nearest_distance_ > distToCollision)
      {
        nearest_distance_ = distToCollision;
        intersection_point_ = ellipseToReal_(eCollisionPosition);
        collision_found_ = true;
        return true;
      }
    }    
  //}*/
  return false;
} 

Vector3d CollisionServer::realToEllipse_(const Vector3d& v)
{
  Vector3d ret;
  ret.x = v.x/radius_.x;
  ret.y = v.y/radius_.y;
  ret.z = v.z/radius_.z;
  return ret;
}

Vector3d CollisionServer::ellipseToReal_(const Vector3d& v)
{
  Vector3d ret;
  ret.x = v.x*radius_.x;
  ret.y = v.y*radius_.y;
  ret.z = v.z*radius_.z;
  return ret;
}
