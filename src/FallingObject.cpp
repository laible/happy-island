
#include "FallingObject.h"
#include "PrimitivePhysics.h"
#include "InGame.h"

FallingObject::FallingObject(Vector3d start, char* asefile):
      MovableGameObject("FallingObject", start),
      mesh_(asefile),
      start_position_(start)
{      
  force_reciever_ = new PrimitivePhysics();  
  force_reciever_->addForce(Vector3d(0, 0, force_reciever_->getMass() * (-2.0f)));  
  force_reciever_->setVelocity(Vector3d(0, 0, -100));       
}




// View
FallingObjectView::FallingObjectView(FallingObject* fo):
      CutSceneView(fo),
      fo_(fo) 
{
}  

void FallingObjectView::draw()
{
  glPushMatrix();
  glPushAttrib(GL_ALL_ATTRIB_BITS);    
  glTranslatef(fo_->getCenter().x , fo_->getCenter().y , fo_->getCenter().z );
  glScalef(10.0, 10.0, 10.0);
  if (fo_ != 0)
    fo_->getMesh()->draw();
  glPopAttrib();
  glPopMatrix();     
}


// Controller
FallingObjectController::FallingObjectController(FallingObject* model, FallingObjectView* view, InGame* ig): 
    CutSceneController(model, view, in_game_),
    fov_(view),
    fo_(model), 
    in_game_(ig),
    smoke_trail_(0),
    age(0),
    timeToLive(5000),
    dying_(false) 
{
  smoke_trail_ = new SmokeTrail(fo_, 10, 200, 1000, 120);
  smoke_trail_->setVelocity(0.2f);
  smoke_trail_->setTimeToLive(20000);
 // in_game_->addView(smoke_trail_);
 // in_game_->addController(smoke_trail_);  
}

void FallingObjectController::updateTime(long time, long delta_t)
{
  if (isTerminated()) return;
  

	age += delta_t;
	if(age > timeToLive)
	{
    terminated_ = true;
		return;
	}


  float dt = float(delta_t)/1000.0f;
  fo_->getPhysics()->simulate(dt);       

  // get collision candidates in neighbourhood
  std::vector<Triangle3d> collision_candidates = in_game_->getSpatialSearchStructure()->requestNearTriangles(in_game_->getLandscape()->getMesh(), fo_->getCenter(), 1000.0f);

  // request move from Collision Server
  in_game_->getCollisionServer()->setBoundingEllipsoid(Vector3d(1,1,1));
  Vector3d response = in_game_->getCollisionServer()->requestMove(fo_->getCenter(), fo_->getPhysics()->getMove(), collision_candidates);
  ((MovableGameObject*)fo_)->setCenter(((MovableGameObject*)fo_)->getCenter() + response);

  // if we collided set gravity velocity component to zero and create explosion
  if (in_game_->getCollisionServer()->isCollided() || fo_->getCenter().z < 0.1f) {
    // controller and view should be destroyed by garbage collector so mark them as terminated
    //terminated_ = true;
    //fov_->setGarbage(true);
    setDying(true);          // prepare for controller remove 
 //   fov_->setGarbage(true);  // remove view

    fo_->getPhysics()->setVelocity(Vector3d(0,0,0)); 
    // deform landscape
    in_game_->getLandscape()->calcSmoothDeformation(fo_->getCenter(), 100);
    // update spartial search structure to reflect modified landscape
    in_game_->getSpatialSearchStructure()->build(in_game_->getLandscape()->getMesh());
    smoke_trail_->setDying(true);
    in_game_->onExplosion(fo_->getCenter(), AIRPLANE);
    

    ParticleSystem* ps = new ParticleSystem(fo_->getCenter(), 1.0f); 
 //   in_game_->addView(ps);
 //   in_game_->addController(ps);        
  }



  // let camera look on falling object
  Camera* camera_ = in_game_->getCamera();
  camera_->set_center(fo_->getCenter());  // lookat
  camera_->set_eye(fo_->getStartPosition() * 0.5 );
  camera_->set_up(Vector3d(0,0,1));  
}

