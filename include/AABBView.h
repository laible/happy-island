#ifndef _aabbview_h_
#define _aabbview_h_

#include "AABB.h"
#include "IDrawable.h"

class AABBView : public IDrawable
{
  public:
    AABBView(AABB* model):aabb_(model) {}
    void draw() 
    {
      glPushMatrix();
      glPushAttrib(GL_ALL_ATTRIB_BITS);
    	glDisable(GL_LIGHTING);
    	glDisable( GL_TEXTURE_2D );

	    glBegin(GL_LINE_LOOP);
	    glVertex3f( aabb_->lowerCorner.x, aabb_->lowerCorner.y, aabb_->lowerCorner.z );
	    glVertex3f( aabb_->upperCorner.x, aabb_->lowerCorner.y, aabb_->lowerCorner.z );
	    glVertex3f( aabb_->upperCorner.x, aabb_->lowerCorner.y, aabb_->upperCorner.z );
	    glVertex3f( aabb_->lowerCorner.x, aabb_->lowerCorner.y, aabb_->upperCorner.z );
	    glEnd();

	    glBegin(GL_LINE_LOOP);
	    glVertex3f( aabb_->lowerCorner.x, aabb_->upperCorner.y, aabb_->lowerCorner.z );
	    glVertex3f( aabb_->upperCorner.x, aabb_->upperCorner.y, aabb_->lowerCorner.z );
	    glVertex3f( aabb_->upperCorner.x, aabb_->upperCorner.y, aabb_->upperCorner.z );
	    glVertex3f( aabb_->lowerCorner.x, aabb_->upperCorner.y, aabb_->upperCorner.z );
	    glEnd();

	    glBegin(GL_LINES);
	    glVertex3f( aabb_->lowerCorner.x, aabb_->lowerCorner.y, aabb_->lowerCorner.z );
	    glVertex3f( aabb_->lowerCorner.x, aabb_->upperCorner.y, aabb_->lowerCorner.z );

	    glVertex3f( aabb_->upperCorner.x, aabb_->lowerCorner.y, aabb_->lowerCorner.z );
	    glVertex3f( aabb_->upperCorner.x, aabb_->upperCorner.y, aabb_->lowerCorner.z );

	    glVertex3f( aabb_->upperCorner.x, aabb_->lowerCorner.y, aabb_->upperCorner.z );
	    glVertex3f( aabb_->upperCorner.x, aabb_->upperCorner.y, aabb_->upperCorner.z );

	    glVertex3f( aabb_->lowerCorner.x, aabb_->lowerCorner.y, aabb_->upperCorner.z );
	    glVertex3f( aabb_->lowerCorner.x, aabb_->upperCorner.y, aabb_->upperCorner.z );
	    glEnd();      
      glPopAttrib();
      glPopMatrix();
    }
  protected:
    AABB* aabb_;
};

#endif
