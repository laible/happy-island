#ifndef _game_h_
#define _game_h_

#include "IDrawable.h"
#include "ITimedObject.h"
#include "IInputListener.h"
#include "Intro.h"
#include "Outro.h"
#include "Menu.h"
#include "InGame.h"

class Game : public IDrawable, public ITimedObject, public IInputListener
{
  public:
    /// game state identifiers
    enum GameStateIdentifier {
      GS_INTRO,
      GS_MENU,
      GS_INGAME,
      GS_OUTRO,
      GS_LEAVE

    };    
    /// constructor
    Game();  
    void draw();   
    // timer event
    void updateTime(long time, long delta_t);

    // input events
    void keyPressEvent(SDL_KeyboardEvent e);
    void keyReleaseEvent(SDL_KeyboardEvent e);
    void mouseMotionEvent(SDL_MouseMotionEvent e);
    void mousePressEvent(SDL_MouseButtonEvent e);
    void mouseReleaseEvent(SDL_MouseButtonEvent e);

  protected:
    Intro intro_;
    Menu menu_;
    InGame* inGame_; 
    Outro outro_;
    GameStateIdentifier state_;

};

#endif
