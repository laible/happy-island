#ifndef TRIANGLE3D
#define TRIANGLE3D

#include "Vector3d.h"


/** Triangle3d
  **/

// Note: Implements Fly-Weight Pattern
class Triangle3d
{
  public:    
    /// the Triangle data
    Vector3d A;
    Vector3d B;
    Vector3d C;
        
    //////////////////////////////////////////////////////////////////////////////////////////////
    /// Constructors        
    /// ! no default initialisation to (0,0,0) for speed reasons
    inline Triangle3d() 
    {
    }; 
               
    /// create triangle from vector
    inline Triangle3d(Vector3d va, Vector3d vb, Vector3d vc)
    :A(va), B(vb), C(vc)
    {
    };


    inline Triangle3d(const Triangle3d& t)
      :A(t.A), B(t.B), C(t.C) 
    {
    };
    
    //////////////////////////////////////////////////////////////////////////////////////////////
    
    inline Vector3d getTriangleNormal() const
    {
      return (A-B).crossProduct(C-A);
    }       
            
    inline Triangle3d& operator=(const Triangle3d& t)
    {
      this->A = t.A;
      this->B = t.B;
      this->C = t.C;
      return *this;
    }

    inline bool operator==(const Triangle3d &t) const
    {
      return ( A == t.A && B == t.B && C == t.C );
    }
             
    inline bool operator!= (const Triangle3d &t) const
    {
      return ( A != t.A || B != t.B || C != t.C );
    }
        
};

#endif
