#ifndef _ingame_h_
#define _ingame_h_

#include "GameState.h"
#include "SkyBox.h"
#include "Palms.h"
#include "Landscape.h"
#include "Water.h"
#include "Camera.h"
#include "Light.h"
#include "Player.h"
#include "CollisionServer.h"
#include "IInputListener.h"
#include "PlayerController.h"
#include "SpatialSearchStructure.h"
#include "IPhysics.h"
#include "Round.h"
#include "Box.h"

#include "Hud.h"

#include "SoundManager.h"

class RoundTransition;
class PlayerWonTransition;




#include <list>

#include <assert.h>

enum Explosion_Type
{
  DEFAULT_BOMB =0,
  ATOM_BOMB = 1,
  AIRPLANE = 2
};

enum InGameState
{
  PLAYING = 0,
  ROUNDTRANSITION = 1,
  PLAYERWONTRANSITION = 2
};


//#define DEBUG_INGAME

class InGame:public GameState
{
  // initialisation
  public:
    InGame();  
    void loadLevelData();
  private:
    bool data_loaded_;  // flag that indicates if level data has been loaded
	std::vector<ParticleSystem*> particle_systems_;
  public:

    // game phase
    bool isDataLoaded() { return data_loaded_; }



    void draw();

	void addParticleSystem(ParticleSystem* ps);
	//void addSmokeTrail(SmokeTrail* st);
	
	

    void addPlayer(Player* p, PlayerController* pc) 
    { 
      if (pc==0) printf("Warning: adding 0 player controller!!!\n");
      if (p==0) printf("Warning: adding 0 player model!!!\n");
      players_.push_back(p) ; 
      player_controllers_.push_back(pc);
      active_player_controller_ = player_controllers_[0];
    }

    void nextPlayer() 
    {       
      if (players_.size() > 1 )
      {
        printf("next Player\n");
        //printf("old player %d\n", index);
        active_player_index_++;  // determine next player index
        active_player_index_ %= players_.size();
        //printf("switching to player %d\n", index);
        active_player_controller_ = player_controllers_[active_player_index_];
      } else
      {
        active_player_controller_ = player_controllers_[0];
        onPlayerWon(players_[0]);
      }
    }

    Player* getActivePlayer() { return players_[active_player_index_]; }

    void plantBox();

    void updateTime(long time, long delta_t);
    Landscape* getLandscape() {return landscape_;}
    CollisionServer* getCollisionServer() { return &collision_server_;}
    SpatialSearchStructure* getSpatialSearchStructure() { return sss_ ; }
    Light* getLight() { return sun_light_; }
    Camera* getCamera() { return camera_; }

   // Landscape* getLandscape() { return landscape_; }

    // input events
    void keyPressEvent(SDL_KeyboardEvent e);
    void keyReleaseEvent(SDL_KeyboardEvent e);
    void mouseMotionEvent(SDL_MouseMotionEvent e);
    void mousePressEvent(SDL_MouseButtonEvent e);
    void mouseReleaseEvent(SDL_MouseButtonEvent e);
 
    void onRoundTerminated();    
    void onExplosion(Vector3d center, Explosion_Type et);
    void onPlayerDied(Player* victim); 
    void onPlayerWon(Player* winner);

    void updateOpenAL();

    void onLeaveInGame();

    void createPlayers(int nPlayers); 

    Hud* getHud() { return hud_;}
    
  protected:
    int active_player_index_;
    Box box_of_goody_;
    Round* round_;
    RoundTransition* round_transition_ ;
    PlayerWonTransition* player_won_transition_;
    SkyDome* skybox_;
    Landscape* landscape_;
    Water* water_;
    Camera* camera_;
    Light* sun_light_;
    std::vector<Palm* > palms_;
    
    PlayerController* active_player_controller_;
    


    std::list<IDrawable*> views_;
    std::list<ITimedObject*> tick_recievers_;

    // player model/view & controller
    std::vector<Player*> players_;
    std::vector<PlayerController*> player_controllers_;

    CollisionServer collision_server_;

    SpatialSearchStructure* sss_;
  
    Hud* hud_;

    SoundManager sound_manager_;


  private:   
    Bitmap loading_;

    InGameState ig_state_;

    float silence;

};

#endif
