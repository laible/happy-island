#ifndef _hud_h
#define _hud_h

#include "IDrawable.h"
#include <string>
#include "Bitmap.h"

#include "main.h"

// head up display
class Hud : public IDrawable  
{
  public:
    Hud():
    drawAtomIcon(false),
    drawEnergy(false),
    atom_icon_(892, 636, "Textures/atomicon.PNG"),
    energy_bar_(200, 736, "Textures/energy.PNG")
    {      

    }
    void draw()
    { 
      glColor4f(1.0f, 1.0f, 1.0f, 0.7f); 
      if (drawAtomIcon) atom_icon_.draw();    
      if (drawEnergy)
      {
        // energy
        energy_bar_.draw();
        glDisable(GL_DEPTH_TEST);
        glDisable(GL_LIGHTING);  
        glColor4f(0.2f, 2.0f, 0.5f, 0.7f);                
      	
        glDisable(GL_TEXTURE_2D);      
        glMatrixMode (GL_MODELVIEW);
        glPushMatrix ();
          glLoadIdentity ();
          glMatrixMode (GL_PROJECTION);

          glPushMatrix ();
            glLoadIdentity ();   
            gluOrtho2D (0, WIDTH, 0, HEIGHT);

            glBegin(GL_QUADS);						
	            glTexCoord2f(0,0);		
	            glVertex2i(206, 740);					
	            glTexCoord2f(1,0);
	            glVertex2i(206 + (int)energy_*5.0f , 740);					
	            glTexCoord2f(1,1);		
	            glVertex2i(206+ (int)energy_ *5.0f, 764);					
	            glTexCoord2f(0,1);				
	            glVertex2i(206,764);					
            glEnd();								

          glPopMatrix();
          glMatrixMode(GL_MODELVIEW);

        glPopMatrix();
        glDisable(GL_BLEND);
        glEnable(GL_TEXTURE_2D);
        glEnable(GL_LIGHTING);
        glEnable(GL_DEPTH_TEST) ; 
      }
    }
    bool drawAtomIcon;
    bool drawEnergy;
    void setEnergyValue(float e) { energy_ = e; }
  protected:    
    Bitmap atom_icon_;
    Bitmap energy_bar_;
    float energy_;
    
    
};

#endif
