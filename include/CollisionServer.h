#ifndef _Collision_Server_h
#define _Collision_Server_h

#include "Vector3d.h"
#include "Landscape.h"
#include "Player.h"
#include "MovableGameObject.h"
#include "AABB.h"

class InGame;

class CollisionServer
{
  public:
    void setInGame(InGame* ig) { in_game_ = ig; }
    void setPosition(const Vector3d& pos) { position_ = pos;}
    void setMove(const Vector3d& move) { move_ = move;}
    void setBoundingEllipsoid(const Vector3d& el) { radius_ = el;}  //< vector entries are interpreted as width, depth, height of ellipsoid
    void setBoundingEllipsoid(const AABB& aabb) { radius_ = aabb.upperCorner - aabb.lowerCorner;} 

    Vector3d requestMove(const Vector3d& position, const Vector3d& move, const std::vector<Triangle3d>& tri_list);
    bool isCollided() { return collision_found_ ; }

    void registerMovableGameObject(MovableGameObject* mgo) { mgo_list_.push_back(mgo); }
  /*  void unregisterMovableGameObject(MovableGameObject* mgo) 
    { 
      for (std::list<MovableGameObject*>::iterator it = mgo_list_.begin(); it!=mgo_list_.end(); it++)
      {
        if (*it == 0 || mgo == 0) continue;
        if (*it == mgo)
        {
          mgo_list_.erase(it);
          return;
        }  
      }
    }
*/

    void checkForAABBCollision();
  private:
    InGame* in_game_;
    std::list<MovableGameObject*> mgo_list_; 


    Vector3d realToEllipse_(const Vector3d& v);
    Vector3d ellipseToReal_(const Vector3d& v);
    void findNearestCollidingTriangle(const std::vector<Triangle3d>& tri_list);
    bool checkTriangle(const Vector3d& P1, const Vector3d& P2, const Vector3d& P3);
    Landscape* landscape_;


    Vector3d radius_;
    Vector3d position_;
    Vector3d move_;

    // requested move expressed in transformed space
    Vector3d e3velocity_;
    Vector3d e3basePoint_;
    Vector3d e3normalizedVelocity_; 

    // hit information
    bool collision_found_;
    float nearest_distance_;
    Vector3d intersection_point_; 
};

#endif
