#ifndef _rocketcontroller_h_
#define _rocketcontroller_h_

#include "ITimedObject.h"
#include "PrimitivePhysics.h"
#include "Rocket.h"
#include "InGame.h"
#include "Particle.h"


class RocketController : public ITimedObject
{
  public:
    RocketController(Rocket* model, RocketView* view, InGame* ig):
      rocket_(model), 
      view_(view),
      in_game_(ig),
      smoke_trail_(0) 
      {
        smoke_trail_ = new SmokeTrail(rocket_, 1.0, 200, 400, 1);
        smoke_trail_->setVelocity(1.0);
//        in_game_->addView(smoke_trail_);
//        in_game_->addController(smoke_trail_); 
      }

    // timer event
    void updateTime(long time, long delta_t)
    {
      if (isTerminated()) return;
      

      float dt = float(delta_t)/1000;
      rocket_->getPhysics()->simulate(dt);  
    

      // get collision candidates in neighbourhood
      std::vector<Triangle3d> collision_candidates = in_game_->getSpatialSearchStructure()->requestNearTriangles(in_game_->getLandscape()->getMesh(), rocket_->getCenter(), 1000.0f);

      // request move from Collision Server
      in_game_->getCollisionServer()->setBoundingEllipsoid(Vector3d(1,1,1));
      Vector3d response = in_game_->getCollisionServer()->requestMove(rocket_->getCenter(), rocket_->getPhysics()->getMove(), collision_candidates);
      ((MovableGameObject*)rocket_)->setCenter(((MovableGameObject*)rocket_)->getCenter() + response);

      // if we collided set gravity velocity component to zero and create explosion
      if (in_game_->getCollisionServer()->isCollided()) {
        // controller and view should be destroyed by garbage collector so mark them as terminated
        terminated_ = true;
        view_->setVisible(false);

        rocket_->getPhysics()->setVelocity(Vector3d(0,0,0)); 

        smoke_trail_->setDying(true);
        ParticleSystem* ps = new ParticleSystem(rocket_->getCenter(), 1.0f); 
        if (rocket_->getDeformLandscape() == true)
        {
          // deform landscape
          in_game_->getLandscape()->calcSmoothDeformation(rocket_->getCenter(), 100);
          // update spartial search structure to reflect modified landscape
          in_game_->getSpatialSearchStructure()->build(in_game_->getLandscape()->getMesh());
          in_game_->onExplosion(rocket_->getCenter(), ATOM_BOMB);
        } else
        {
          in_game_->onExplosion(rocket_->getCenter(), DEFAULT_BOMB);
        }
        //delete(rocket_);
//        in_game_->addView(ps);
//        in_game_->addController(ps);        
      }
    }
  private:
    Rocket* rocket_;
    RocketView* view_;
    SmokeTrail* smoke_trail_;
    InGame* in_game_;
};

#endif
