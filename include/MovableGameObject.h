#ifndef _movablegameobject_h_
#define _movablegameobject_h_

#include "IDrawable.h"
#include "AABB.h"
#include "Vector3d.h"
#include "ObserverPattern.h"


#include <string>

class MovableGameObject : public IDrawable, public Observable
{
  public:
    MovableGameObject(const std::string& type):typeid_(type), center_(Vector3d(0,0,0)), aabb_(AABB(Vector3d(-2,-2,-2),Vector3d(+2,+2,+2))) {}    
    MovableGameObject(const std::string& type, const Vector3d& c, const AABB& bb=AABB()):typeid_(type), center_(c), aabb_(bb) {}    
    virtual ~MovableGameObject() {}

    Vector3d getCenter() const { return center_ ; }
    virtual void setCenter(Vector3d c) { center_ = c; }

    std::string getType() { return typeid_; }

    AABB getGlobalAABB() const
    {
      return AABB(aabb_.lowerCorner + center_, aabb_.upperCorner + center_); 
    } 

    void setGlobalAABB(const AABB& bb)
    {
      aabb_ = AABB(aabb_.lowerCorner - center_, aabb_.upperCorner - center_); 
    }

    void setLocalAABB(const AABB& bb) { aabb_ = bb; }
    AABB getLocalAABB() const { return aabb_; }
    virtual void onBBCollision(MovableGameObject* collision_partner) // bounding box collision event
    {
      printf("collision of %s with %s\n", this->getType().c_str(), collision_partner->getType().c_str());
    } 
  protected:
    Vector3d center_;
    AABB aabb_;
    std::string typeid_;
};

#endif
