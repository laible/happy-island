#ifndef _animated_mesh_h_
#define _animated_mesh_h_

#include "IDrawable.h"
#include "ITimedObject.h"
#include "Mesh3d.h"
#include <vector>



class AnimatedMesh: public IDrawable, public ITimedObject
{
  typedef std::vector<Mesh3d> Sequence;
  typedef unsigned int SequenceIndex;
  typedef unsigned int FrameIndex;
  typedef std::vector<Sequence> SequenceList;

  /*enum PlaybackPolicy {
    PB_STARTATBEGINNING,
    PB_UPANDDOWN,
    
  };
*/

  public:
    AnimatedMesh(const std::string& base_filename);
    AnimatedMesh() { printf("Achduscheisse\n");}

    void setSequenceIndex(SequenceIndex si) {currentSequence_ = si;};

    SequenceIndex getSequenceIndex() {return currentSequence_;};

    void setFrameIndex(FrameIndex fi) {currentFrame_ = fi;};

    FrameIndex getFrameIndex() { return currentFrame_; };

    void startSequence(unsigned long sst) 
    {
      sequence_start_time_ = sst;
      running_ = true;
    };

    void stopSequence() { running_ = false;};
    
    bool isRunning() { return running_; };

    void setFramesPerSecond(float fps) { fps_ = fps;};
    
    void loadFromFiles(const std::string& base_filename);

    void updateTime(long time);
    void draw();
  protected:
    SequenceList sequences_;
    SequenceIndex currentSequence_;
    FrameIndex currentFrame_;
    unsigned long sequence_start_time_;
    bool running_;
    float fps_;
};

#endif
