#ifndef _IPhysics_h_
#define _IPhysics_h_

#include "Vector3d.h"

class IPhysics
{
  public:
    virtual void simulate(const float delta_t) = 0;
};

#endif
