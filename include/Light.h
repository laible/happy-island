#ifndef GUARD_Light_h
#define GUARD_Light_h

#include "SDL_opengl.h"

#include "Vector3d.h"

/// A simple light source.

class Light {
public:
  Light() 
  {    
    position[0] = position[1] = position[2] = position[3] = 0;
  }
  
  Light(GLfloat* pos)
  {
    position[0] = pos[0];
    position[1] = pos[1];
    position[2] = pos[2];
    position[3] = 0;
  }

  Light(float x, float y, float z, float w=0) 
  {
    position[0] = x;
    position[1] = y;
    position[2] = z;
    position[3] = w;    
  }

  void on();
  void off();

  Vector3d getCenter() { return Vector3d(position[0], position[1], position[2]); }

private:
  GLfloat position[4];
};

#endif
