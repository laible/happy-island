#ifndef _intro_h_
#define _intro_h_

#include "GameState.h"
#include "Camera.h"
#include "Bitmap.h"

class Intro : public GameState
{
public:
  Intro();

  // input events
  void keyPressEvent(SDL_KeyboardEvent e);
  void keyReleaseEvent(SDL_KeyboardEvent e);
  void mouseMotionEvent(SDL_MouseMotionEvent e);
  void mousePressEvent(SDL_MouseButtonEvent e);
  void mouseReleaseEvent(SDL_MouseButtonEvent e);

  void draw();

private:
  Bitmap bitmap_;
};

#endif
