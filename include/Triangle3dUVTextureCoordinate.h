#ifndef TRIANGLE3DUVTEXTURECOORDS
#define TRIANGLE3DUVTEXTURECOORDS

#include "UVTextureCoordinate.h"

// Note: Implements Fly-Weight Pattern
class Triangle3dUVTextureCoords
{
  public:
    Triangle3dUVTextureCoords() {};
    Triangle3dUVTextureCoords(UVTextureCoordinate a, UVTextureCoordinate b, UVTextureCoordinate c):A(a),B(b),C(c) {};
    UVTextureCoordinate A;
    UVTextureCoordinate B;
    UVTextureCoordinate C;
};

#endif
