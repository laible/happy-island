#ifndef _round_h_
#define _round_h_

#include "ITimedObject.h"
#include "IDrawable.h"

#include "DrawText.h"

//#define DEBUG_ROUND

class Round : public ITimedObject, public IDrawable
{  
  public:
    Round() : 
      clock_(false), 
      timeLeftMS(30000)
    {
      printf("Round::Round\n");
    }

    void startRound(long roundTimeMS) 
    { 
      clock_ = false; 
      terminated_ = false; 
      timeLeftMS = roundTimeMS; 
    }
    void updateTime(long time, long delta_t)
    {
      #ifdef DEBUG_ROUND
        printf("%s: updating Time\n", __FILE__);
      #endif

      //if (isTerminated() && timeLeftMS==0) return;

      timeLeftMS -= delta_t;
      if (timeLeftMS <= 0)
      {
        timeLeftMS = 0;
        setTerminated(true);
        clock_ = false;
      }
      else if (timeLeftMS < 5000) {// remember player 5sec before new round
        clock_ = true;
      }


    }  
    void draw()
    {
      #ifdef DEBUG_ROUND
        printf("%s: time left in round: %d , isTerminated()==%d\n", __FILE__, timeLeftMS, isTerminated());
      #endif

     // CTextDrawer::GetSingleton().PrintText(200,200, "test");
    }
    long timeLeftMS; //
    bool clock_;
};

#endif
