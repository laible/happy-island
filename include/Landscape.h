#ifndef _landscape_h_
#define _landscape_h_

#include "Mesh3d.h"
#include "SpatialSearchStructure.h"


#include <list>

class Landscape : public IDrawable
{
  public:
    Landscape(char* ase_filename);

     /** Triangle selectors
      *  Uses lazy creation -> Additional search structures are build on first use.
      **/              
    std::vector<std::list<Mesh3d::TriangleIndex> > map_vi_to_ti; ///< map from vertex index to triangle index list
        
    /// returns a list of indizes of the Triangles that contain the given Vertex (specified by vertex index);  
    std::list<Mesh3d::TriangleIndex> getAdjacentTriangles(const Mesh3d::VertexIndex vi);    
            
    /// returns a list of vertices with dist(v-Vertice) <= max_distance 
    std::vector<Mesh3d::VertexIndex> getNearVertices(const Vector3d& v, const float max_distance) ;   
    
    /// returns a list of face indizes for which at least one vertice has dist(v-Vertice) <= max_distance
    std::list<Mesh3d::TriangleIndex> getNearTriangles(const Vector3d& v, const float max_distance) ;     
    
    Mesh getMesh() { return mesh_; }

    void draw() { mesh_.draw();}
    

    Triangle3d* getTriangles() {return mesh_.getTriangles();}
    
    /** Triangle Modifiers
      *
      **/ 
    /// perform sperical derformation on mesh
    void calcSphericalDeformation(const Vector3d& v, const float radius);
    void calcSmoothDeformation(const Vector3d& v, const float radius);
    void buildSSS();
  private:
    Mesh mesh_;
    std::vector<std::vector< std::list<int> > > structure_;
    Vector3d lowerLeftCorner;
    Vector3d upperRightCorner;
};

#endif

