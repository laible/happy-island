#ifndef _IInputListener_h_
#define _IInputListener_h_

#include "SDL.h"

/** Interface for every Object that recieves Input Events  
  */

class IInputListener
{
  public:
    virtual void keyPressEvent(SDL_KeyboardEvent e) = 0 ;
    virtual void keyReleaseEvent(SDL_KeyboardEvent e)  = 0;
    virtual void mouseMotionEvent(SDL_MouseMotionEvent e) = 0 ;
    virtual void mousePressEvent(SDL_MouseButtonEvent e) = 0;
    virtual void mouseReleaseEvent(SDL_MouseButtonEvent e) = 0;
};

#endif
