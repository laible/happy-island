#ifndef _shootingpowerbar_h_
#define _shootingpowerbar_h_

#include "IDrawable.h"
#include "ITimedObject.h"
#include "Vector3d.h"
#include "Mesh3d.h"
#include "Player.h"

/** draws shooting-bar at position "center" in direction "direction".  
  **/


const Material POWERBAR_MATERIAL = {{1.0f, 0.9f, 1.0f, 0.8f},
                              {1.0f, 0.9f, 1.0f, 0.8f},
                              {0.05f, 0.05f, 0.05f, 0.8f},
                              {1.0}}; 




class ShootingPowerBar: public IDrawable, public ITimedObject
{
  public:
    ShootingPowerBar(Player* p): 
      player_(p),
      value_(0.1f),
      mesh_("ASE/power.ASE") 
    {
      mesh_.setMaterial(POWERBAR_MATERIAL);
    }
    float getValue() const { return value_; }    
    void draw();
    void updateTime(long time, long delta_t);
  protected:
    Player* player_;
    Mesh mesh_;
    float value_;
};

#endif
