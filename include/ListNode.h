#ifndef _ListNode_h_
#define _ListNode_h_

#include "Node.h"
#include "Vector3d.h"

#include <list>

namespace gris 
{
  class ListNode: public Node 
  {
    public:
      /// stl adaptors
      void push_back(Node* n) { list_.push_back(n); }
      Node* pop_back() const { return list_.pop_back(); } 
      void push_front(Node* n) { list_.push_front(n); }
      Node* pop_front() const { return list_.pop_front(); } 
      void draw() 
      {
        for (std::list<Node*>::iterator it = list_.begin(); it != list_.end(); it++)
        {
          it->draw();
        }
      }
    protected:
      std::list<Node*> list_;
  };
}

#endif