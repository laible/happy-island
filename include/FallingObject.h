#ifndef _fallingobject_h_
#define _fallingobject_h_

#include "MovableGameObject.h"
#include "ITimedObject.h"
#include "Mesh3d.h"
#include "Particle.h"

class InGame;
class PrimitivePhysics;

#include "CutScene.h"

class FallingObject: public MovableGameObject, public CutScene
{
  public:
    FallingObject(Vector3d start, char* asefile);
    Vector3d getStartPosition() { return start_position_; }
    Mesh* getMesh() { return &mesh_; }
    PrimitivePhysics* getPhysics() { return force_reciever_; }
    void onBBCollision(MovableGameObject* collision_partner) {}
  private:
    PrimitivePhysics* force_reciever_;
    Vector3d start_position_;
    Mesh mesh_;
};

class FallingObjectView : public CutSceneView
{
  public:
    FallingObjectView(FallingObject* fo);
    void draw();
  private:
    FallingObject* fo_;
};

class FallingObjectController: public CutSceneController
{
  public:
    FallingObjectController(FallingObject* model, FallingObjectView* view, InGame* ig);
    void updateTime(long time, long delta_t);
    void setDying(bool b) { dying_ = b;}
  private:
    FallingObject* fo_;
    FallingObjectView* fov_;
    InGame* in_game_;
    SmokeTrail* smoke_trail_;
	  long		age;
	  long		timeToLive;
    bool dying_;
};


#endif
