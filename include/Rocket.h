#ifndef _rocket_h_
#define _rocket_h_

#include "Vector3d.h"
#include "AABB.h"
#include "PrimitivePhysics.h"
#include "IPhysics.h"
#include "MovableGameObject.h"


class Rocket : public MovableGameObject
{
  public:
    Rocket(const Vector3d& c):
      MovableGameObject("Rocket", c, AABB(Vector3d(-4,-4,-4), Vector3d(4,4,4)))    
    {
      deform_landscape_ = false;
      physics_ = new PrimitivePhysics();

      physics_->addForce(Vector3d(0,0, (-2.0f) * physics_->getMass()));  // add gravity
    }

    void shoot(Vector3d velocity) { physics_->setVelocity(velocity); }
    PrimitivePhysics* getPhysics() { return physics_; }

    virtual void onBBCollision(MovableGameObject* collision_partner) // bounding box collision event
    {
      printf("collision of %s with %s\n", this->getType().c_str(), collision_partner->getType().c_str());
    } 

    void draw() {}

    void setDeformLandscape(bool b) { deform_landscape_ = b; }
    bool getDeformLandscape() { return deform_landscape_;}

  private:    
    PrimitivePhysics* physics_;
    bool deform_landscape_;
};

#endif
