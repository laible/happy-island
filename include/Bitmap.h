#ifndef _bitmap_h
#define _bitmap_h

#include "IDrawable.h"
#include <string>
#include "Texture.h"

#include "main.h"

class Bitmap : public IDrawable
{
  public:
    Bitmap(int x, int y, std::string filename):x_(x), y_(y), texture_(filename)
    {
    }
    void draw()
    { 
      glDisable(GL_DEPTH_TEST);
      glDisable(GL_ALPHA_TEST);
      //glAlphaFunc(GL_GREATER, GL_ZERO);
      glDisable(GL_LIGHTING);      
      glDisable(GL_CULL_FACE);
      glEnable(GL_BLEND);
      //glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
      //glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
      //glColor3f(1.0,1.0,1.0);
      
      glEnable(GL_TEXTURE_2D);      
      glMatrixMode (GL_MODELVIEW);
      glPushMatrix ();
      glLoadIdentity ();
      glMatrixMode (GL_PROJECTION);
      glPushMatrix ();
      glLoadIdentity ();   
      gluOrtho2D (0, WIDTH, 0, HEIGHT);

      glBindTexture(GL_TEXTURE_2D, texture_.id);
  		glBegin(GL_QUADS);						
				glTexCoord2f(0,0);		
				glVertex2i(x_ , y_ + texture_.getHeight());					
				glTexCoord2f(1,0);
				glVertex2i(x_+ texture_.getWidth() , y_ + texture_.getHeight());					
				glTexCoord2f(1,1);		
				glVertex2i(x_ + texture_.getWidth() , y_);					
				glTexCoord2f(0,1);				
				glVertex2i(x_,y_);					
			glEnd();								
 
      glPopMatrix();
      glMatrixMode(GL_MODELVIEW);
      glPopMatrix();
      glDisable(GL_BLEND);
      glEnable(GL_DEPTH_TEST) ;    
      glEnable(GL_LIGHTING);
    }
  protected:
    int x_, y_;
    Texture texture_;
};

#endif