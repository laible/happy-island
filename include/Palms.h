#ifndef GUARD_Palms_h
#define GUARD_Palms_h

#include "IDrawable.h"
#include "Vector3d.h"
#include "Mesh3d.h"
#include <vector>

#include "ITimedObject.h"

class Palm : public IDrawable, public ITimedObject 
{
  public:
    Palm(const Vector3d& c, const float hor_rot, Mesh trunk, Mesh leafs): 
      center_(c), 
      horizontal_rotation_(hor_rot),
      trunk_(trunk),
      leafs_(leafs)
      {
        printf("Palm::Palm \n");
      }  
    Vector3d getCenter() { return center_; }

    void draw()
    {
      glPushMatrix();
        glTranslatef(center_.x, center_.y, center_.z);
        glRotatef(horizontal_rotation_, 0, 0, 1);
        trunk_.draw();
        leafs_.draw();
      glPopMatrix();
      glPushMatrix();
        glTranslatef(center_.x, center_.y, center_.z);
        glRotatef(horizontal_rotation_, 0, 0, 1);
        trunk_.castShadows();
        leafs_.castShadows();
      glPopMatrix(); 

    }
    void updateTime(long time, long delta_t) {};
  private:
    float horizontal_rotation_;
    Vector3d center_;
    Mesh trunk_;
    Mesh leafs_;
};

class PalmCreater
{
  public:
    PalmCreater():  mesh_trunk_("ASE/palm_trunk.ASE"), mesh_leafs_("ASE/palm_leafs.ASE") 
    {
      printf("PalmCreater::PalmCreater\n");
    }
    void prepareForShadows()
    {
      mesh_trunk_.prepareForShadows();
      mesh_leafs_.prepareForShadows();
    }
  
    Palm* createPalm(const Vector3d& v, const float hor_rot)
    {
      printf("creating Palm at ");
      Vector3d::DUMP(v);
      return (new Palm(v, hor_rot, mesh_trunk_, mesh_leafs_));
    }

  private:
    Mesh mesh_trunk_;
    Mesh mesh_leafs_;
};



#endif
