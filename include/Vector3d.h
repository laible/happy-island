#ifndef VECTOR3D
#define VECTOR3D

#include "assert.h"
#include <cmath>

#include "stdio.h"

class Vector3d
{
  public:    
    /// the vector data
    union {
      struct {
        float x,y,z;
      };
      float val[3];
    };
    
    //////////////////////////////////////////////////////////////////////////////////////////////
    // Constructors
        
    /// ! no default initialisation to (0,0,0) for speed reasons
    inline Vector3d() 
    {
    }; 
    
    inline Vector3d(float xd, float yd, float zd)
      :x(xd), y(yd), z(zd) 
    {
    };
    
    inline Vector3d(float coords[3])
      :x(coords[0]), y(coords[1]), z(coords[2]) 
    {
    };
    
    inline Vector3d(const float* const coords_p)
      :x(coords_p[0]), y(coords_p[1]), z(coords_p[2]) 
    {
    };
    
    inline Vector3d(const Vector3d& v)
      :x(v.x), y(v.y), z(v.z) 
    {
    };

    //////////////////////////////////////////////////////////////////////////////////////////////
       
    inline float& operator[](const unsigned int& i)    
    {
      assert(i<3);
      return *(&x+i);
    };

    inline const float& operator[](const unsigned int& i) const  
    {
      assert(i<3);
      return *(&x+i);
    };

           
    inline Vector3d& operator=(const Vector3d& v)
    {
      x = v.x;
      y = v.y;
      z = v.z;
      return *this;
    }
  
    inline bool operator==(const Vector3d &v)
    {
      return ( x == v.x && y == v.y && z == v.z );
    }
    
      
    inline bool operator== (const Vector3d &v) const
    {
      return ( x == v.x && y == v.y && z == v.z );
    }
    
    inline bool  operator!= (const Vector3d &v) const
    {
      return ( x != v.x || y != v.y || z == v.z );
    }
    
    inline Vector3d operator+(const Vector3d &v) const
    {
      Vector3d sum;
      sum.x = x + v.x;
      sum.y = y + v.y;
      sum.z = z + v.z;
      return sum;
    }
    
    inline Vector3d operator-(const Vector3d &v) const
    {
      Vector3d diff;
      diff.x = x - v.x;
      diff.y = y - v.y;
      diff.z = z - v.z;
      return diff;
    }
    
    inline Vector3d& operator/= (float scalar)
    {
      assert(scalar!=0.0f);
      float inv = 1.0f / scalar;
      x *= inv;
      y *= inv;
      z *= inv;
      return *this;  
    }
    
    inline Vector3d& operator*= (float scalar)
    {
      x *= scalar;
      y *= scalar;
      z *= scalar;
      return *this;  
    }

    inline Vector3d operator * (float scalar) const
    {
      Vector3d mult;
      mult.x = x * scalar;
      mult.y = y * scalar;
      mult.z = z * scalar;
      return mult;  
    }
    
        
    inline float length () const
    {
      return (float)sqrt( x * x + y * y + z * z );
    }
    
    inline float squaredLength () const
    {
      return x * x + y * y + z * z ;
    }
    
    inline float dotProduct(const Vector3d& v) const
    {
      return x * v.x + y * v.y + z * v.z;
    }
  
    /// normalise vector if not zero, don't do anything if zero
    inline float normalise() 
    {
      float length = (float)sqrt(x*x + y*y + z*z);
      
      if (length > 1e-08)
      {
        float inv = 1.0f / length;
        x*=inv;
        y*=inv;
        z*=inv;
      }
      return length;
    }
      
    inline Vector3d crossProduct(const Vector3d &v) const
    {
      Vector3d cross;
      cross.x = y * v.z - z * v.y;
      cross.y = z * v.x - x * v.z;
      cross.z = x * v.y - y * v.x;
      return cross;
    }
    
    inline bool isZeroLength () const
    {
      float sqlen = (x * x) + (y * y) + (z * z);
      return (sqlen < (1e-06 * 1e-06));
    }
    
    inline Vector3d normalisedCopy () const
    {
      Vector3d ret = *this;
      ret.normalise();
      return ret;
    }       

    static void DUMP(const Vector3d& v)
    {
      printf(":%f %f %f\n", v.x, v.y, v.z);
    }


};

#endif
