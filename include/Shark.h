#ifndef GUARD_Shark_h
#define GUARD_Shark_h

#include "IDrawable.h"
#include "ITimedObject.h"
#include "IInputListener.h"
#include "Vector3d.h"
#include "Mesh3d.h"

#include "Water.h"

class Shark : public IDrawable 
{
public:
  Shark(): 
    center(Vector3d(0, 0, 0)), 
    mesh("ASE/Shark.ASE"), 
    radius(100), 
    degree(0), 
    velocity(0.03), 
    surface(0) {}
  Shark(Vector3d vec): 
    center(vec), 
    mesh("ASE/Shark.ASE"), 
    radius(100), 
    degree(0), 
    velocity(0.03),   
    surface(0) {}

  void setCenter(Vector3d);
  void raiseDegree(double);
  void draw();
  void update(long t, long delta_t, float z);

private:
  Vector3d center;
  Mesh mesh;

  double radius;
  double degree;
  double velocity;
  float surface;
};

class SharkController : public ITimedObject
{
  public:
    SharkController(Shark* s, Water* w):shark_(s), water_(w) {};
    void updateTime(long time, long delta_t)
    {
      shark_->update(time, delta_t, water_->getCenter().z);
    }
  private:
    Shark* shark_;
    Water* water_;
};

#endif
