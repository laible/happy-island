#ifndef _cutscene_h_
#define _cutscene_h_

#include "IDrawable.h"
#include "ITimedObject.h"

#include "InGame.h"

class FallingObject;

class CutScene 
{        
};

class CutSceneView : public IDrawable
{
  public:
    CutSceneView(CutScene* cs): cs_(cs) {}
    virtual void draw() = 0;
  private:
    CutScene* cs_;
};

class CutSceneController : public ITimedObject
{
  public:
    CutSceneController(CutScene* cs, CutSceneView* csv, InGame* ig): 
      cs_(cs), 
      csv_(csv),
      age_(0), 
      in_game_(ig) 
      {
      }
  private:
    InGame* in_game_;
    long age_;
    CutScene* cs_;
    CutSceneView* csv_;
};



class RoundTransition : public IDrawable, public ITimedObject
{
  public:
    RoundTransition(InGame* ig);
    ~RoundTransition()
    {
/* falsch???
      for (std::vector<CutSceneController*>::iterator it = controllers_.begin(); it!=controllers_.end(); it++)
      {
        delete(*it);
      }

      for (std::vector<CutSceneView*>::iterator it = views_.begin(); it!=views_.end(); it++)
      {
        delete(*it);
      }

      for (std::vector<CutScene*>::iterator it = models_.begin(); it!=models_.end(); it++)
      {
        delete(*it);
      }
*/
    }

    void draw()
    {
      if (current_view_ != 0)
        current_view_->draw();
    }

    void updateTime(long time, long delta_t)
    {
      if (current_controller_->isTerminated())
      {
        state_++; 
        if (state_>=controllers_.size())  // if we reached the end of the round transition (played all cutscenes) set to terminated
          terminated_ = true;

        // set current views/controller 
        if (!(this->isTerminated()))
        {
          current_view_ = views_[state_];
          current_controller_ = controllers_[state_];
        }
      } else  current_controller_->updateTime(time, delta_t);
    }

  protected:    
    InGame* in_game_;
    unsigned int state_;
    std::vector<CutScene*> models_;
    std::vector<CutSceneView*> views_;
    std::vector<CutSceneController*> controllers_;

    IDrawable* current_view_;
    ITimedObject* current_controller_;
};


class Text3D: public MovableGameObject, public CutScene
{
  public:
    Text3D(Vector3d start, char* asefile):
      MovableGameObject("Text3D", start), 
      mesh_(asefile),
      scale_(0.003f)
    {    
    }
    float getHorizontalRotationAngle() { return hori_angle_; }
    void setHorizontalRotationAngle(float a) { hori_angle_ = a; }
    float getVerticalRotationAngle() { return verti_angle_; }
    void setVerticalRotationAngle(float a) { verti_angle_ = a; }
    void setScale(float f) { scale_ = f; }
    float getScale() { return scale_;}
    Mesh* getMesh() { return &mesh_; }
    void onBBCollision(MovableGameObject* collision_partner) {}
  private:
    float hori_angle_;
    float verti_angle_;
    float scale_;
    Mesh mesh_;
};

class Text3DView : public CutSceneView
{
  public:
    Text3DView(Text3D* model):
      CutSceneView(model),      
      model_(model) {}
  void draw();
  private:
    Text3D* model_;
};

class Text3DController: public CutSceneController
{
  public:
    Text3DController(Text3D* model, Text3DView* view, InGame* ig): 
      CutSceneController(model, view, in_game_),
      view_(view),
      model_(model), 
      in_game_(ig),
      time_to_live_(5000),
      attached_to_player_(false),
      attached_to_fo_(false),
      age_(0) {}

      void attachToPlayer(Player* p )
      {        
        attached_to_player_ = true;
        attached_to_fo_ = false;
        player_ = p;
      }

      void attachToFallingObject(FallingObject* fo)
      {       
        attached_to_player_ = false;
        attached_to_fo_ = true;
        fo_ = fo;
      }

    void updateTime(long time, long delta_t);

  private:
    Text3D* model_;
    Text3DView* view_;
    InGame* in_game_;
  
    Player* player_;
    FallingObject* fo_;
    bool attached_to_player_;
    bool attached_to_fo_;
    long age_;
    long time_to_live_;
};

class PlayerWonTransition : public IDrawable, public ITimedObject
{
  public:
    PlayerWonTransition(InGame* ig);
    void draw();
    void updateTime(long time, long delta_t);
  protected:
    InGame* in_game_;
    Text3D* model_;
    Text3DView* view_;
    Text3DController* controller_;
};


#endif
