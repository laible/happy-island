#ifndef _itimedobject_h_
#define _itimedobject_h_

class ITimedObject
{  
  public:
    ITimedObject():terminated_(false) {};
    virtual ~ITimedObject() {};
    virtual void updateTime(long time, long delta_t)=0;
    void setTerminated(bool b) {terminated_ = b;};
    bool isTerminated() {return terminated_;};
  protected:
    bool terminated_;
};

#endif
