#ifndef GUARD_GraphicMenu_h
#define GUARD_GraphicMenu_h

#include "IDrawable.h"
#include "Bitmap.h"
#include "SDL.h"
#include <vector>

class GraphicMenu : public IDrawable {
public:
  GraphicMenu(): 
    backGround_(0,0, "Textures/Menu/menu_background.png"),
    itemIntro_(700, 600, "Textures/Menu/Item_Intro.png"), 
    itemPlay_(700, 450, "Textures/Menu/Item_Play.png"),
    itemOutro_(700, 300, "Textures/Menu/Item_Outro.png"), 
    itemLeave_(700, 150, "Textures/Menu/Item_Leave.png"),
    state_(0) 
    {}

  void draw();

  int state_;
  void keyPressEvent(SDL_KeyboardEvent e);

private:
  Bitmap backGround_;
  Bitmap itemIntro_;
  Bitmap itemPlay_;
  Bitmap itemOutro_;
  Bitmap itemLeave_;
};

#endif
