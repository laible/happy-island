#ifndef _PlayerController_h_
#define _PlayerController_h_

#include "IInputListener.h"
#include "ITimedObject.h"
#include "ShootingPowerBar.h"
#include <vector>


class InGame;
class Player;

class PlayerController:public IInputListener, public ITimedObject
{
  public:
    PlayerController(Player* p, InGame* ig)
      :player_(p), 
      in_game_(ig),
      shooting_power_bar_(0),   
      number_of_current_keyframe_(0),
      time_since_animation_change_(0),
      time_between_keyframes_(30)
    {
      
      sequenceOfAnimation_.push_back(4); sequenceOfAnimation_.push_back(5); sequenceOfAnimation_.push_back(6); sequenceOfAnimation_.push_back(7);   
      sequenceOfAnimation_.push_back(8); sequenceOfAnimation_.push_back(7); sequenceOfAnimation_.push_back(6); sequenceOfAnimation_.push_back(5);   
      sequenceOfAnimation_.push_back(4); sequenceOfAnimation_.push_back(3); sequenceOfAnimation_.push_back(2); sequenceOfAnimation_.push_back(1);   
      sequenceOfAnimation_.push_back(0); sequenceOfAnimation_.push_back(1); sequenceOfAnimation_.push_back(2); sequenceOfAnimation_.push_back(3);   

    }
    Player* getPlayer() const {return player_;}
    // input events
    void keyPressEvent(SDL_KeyboardEvent e);
    void keyReleaseEvent(SDL_KeyboardEvent e);
    void mouseMotionEvent(SDL_MouseMotionEvent e);
    void mousePressEvent(SDL_MouseButtonEvent e);
    void mouseReleaseEvent(SDL_MouseButtonEvent e);
    // timer event
    void updateTime(long time, long delta_t);

    // handels next Round event
    void onNextRound();
  private:
    InGame* in_game_;
    Player* player_; 
    ShootingPowerBar* shooting_power_bar_;

    // Animation
    std::vector<unsigned> sequenceOfAnimation_;
    unsigned number_of_current_keyframe_;
    long time_since_animation_change_;
    long time_between_keyframes_;

};

#endif
