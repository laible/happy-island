#ifndef _objectnode_h_
#define _objectnode_h_

#include "Node.h"
#include "Vector3d.h"

namespace gris 
{
  class ObjectNode: public Node 
  {
    public:
      void setCenter(const Vector3d& c) { center_ = c; }
      Vector3d getCenter() const { return center_; }       
    protected:
      Vector3d center_;
  };
}

#endif