#ifndef GUARD_Player_h
#define GUARD_Player_h

#include "SDL.h"

#include <vector>
#include <iostream>


#include "Mesh3d.h"
#include "Vector3d.h"
#include "AnimatedMesh.h"
#include "IInputListener.h"
#include "PrimitivePhysics.h"
#include "MovableGameObject.h"
#include "PlayerPhysics.h"

#include "Bitmap.h"

#include <string>

class InGame;

class Player : public MovableGameObject, public ITimedObject
{
  public:
      
    Player(Vector3d center, Vector3d direction, Vector3d up, InGame* ig);

    void turn_right(float);    
    void turn_left(float);
    void loadSequences(const std::string& base_filename);
    
    float getEnergy() { return energy_; }
    void setEnergy(float e) { energy_ = e; }


    void draw();

    Vector3d getUp() { return up_.normalisedCopy();}
    Vector3d getDirection() {return direction_.normalisedCopy();};
    void setDirection(Vector3d newDirection) { direction_ = newDirection; }
    void setGravityComponent(Vector3d g) { gravity_ = g; }
    Vector3d getGravityComponent() { return gravity_; }

    int getWeaponType() { return weapon_type_; }
    void setWeaponType(int t) { weapon_type_ = t; }

    float getHeightToEye() { return height_to_eye_; };  

    Vector3d getVelocity() { return force_reciever_->getVelocity(); }
    void setVelocity(const Vector3d& newVel) { force_reciever_->setVelocity(newVel) ;}

    float getMaxVelocity() { return max_velocity_; }
    void setMaxVelocity(float newVel) { max_velocity_ = newVel; }
    float getRunAcceleration() { return run_acceleration_; }
    void setRunAcceleration(float a) {run_acceleration_ = a;}

    Mesh getMesh() { return getCurrentMesh(); }
	public:
		void onForwardPressed();
		void onForwardReleased();
		void onBackwardPressed();
		void onBackwardReleased();
		void onViewChanged(int dx, int dy); // mouse moved
		void onJumpPressed();
		void onShootReleased();
		void onShootPressed();
		bool isJumping() { return jumping_;}
		void setJumping(bool b) { jumping_ = b;}
	protected:
	bool is_shooting_;

    bool isForward() { return forward_;}
    bool isBackward() { return backward_;}

    bool isTurning() { return turning_;}

    void setForward(bool b) { forward_ = b;}
    void setBackward(bool b) { backward_ = b;}

    void setTurning(bool b) { turning_ = b;}

	public:
	    // Animation
	    std::vector<unsigned> sequenceOfAnimation_;
	    unsigned number_of_current_keyframe_;
	    long time_since_animation_change_;
	    long time_between_keyframes_;

    PrimitivePhysics* getForceReciever() const { return force_reciever_; }
    PlayerPhysics* getPlayerPhysics() const { return player_physics_; }
  
    void onBBCollision(MovableGameObject* collision_partner);

    void setHorizontalRotationAngle(float);
    void addToHorizontalRotationAngle(float);
    float getHorizontalRotationAngle();

    void setVerticalRotationAngle(float);
    void addToVerticalRotationAngle(float);
    float getVerticalRotationAngle();

    Mesh getCurrentMesh();
    void setNumberOfCurrentMesh(unsigned);
    unsigned getNumberOfCurrentMesh();

    void updateOpenAL();
    void updateTime(long int time, long d_time);

    void castShadows();

    bool isDead() const { return dead_ ; }
    void setDead(bool b) { dead_ = b ; }

    void prepareForShadows()
    {
      mesh_0_00_.prepareForShadows();
      mesh_0_01_.prepareForShadows();
      mesh_0_02_.prepareForShadows();
      mesh_0_03_.prepareForShadows();
      mesh_0_04_.prepareForShadows();
      mesh_0_05_.prepareForShadows();
      mesh_0_06_.prepareForShadows();
      mesh_0_07_.prepareForShadows();
      mesh_0_08_.prepareForShadows();

      mesh_1_00_.prepareForShadows();
      mesh_1_01_.prepareForShadows();
      mesh_1_02_.prepareForShadows();
      mesh_1_03_.prepareForShadows();
      mesh_1_04_.prepareForShadows();
      mesh_1_05_.prepareForShadows();
      mesh_1_06_.prepareForShadows();
      mesh_1_07_.prepareForShadows();
      mesh_1_08_.prepareForShadows(); 
    }

  public:    
    bool moving_forwards_;
    bool moving_backwards_;
    char* appearance_;


  private:
    InGame* in_game_;
    int weapon_type_;
    PrimitivePhysics* force_reciever_;
    PlayerPhysics* player_physics_; // non-realistic player-physics
    float horizontal_rotation_angle_;
    float vertical_rotation_angle_;
      
    //meshes for beaver
    Mesh mesh_0_00_;
    Mesh mesh_0_01_;
    Mesh mesh_0_02_;
    Mesh mesh_0_03_;
    Mesh mesh_0_04_;
    Mesh mesh_0_05_;
    Mesh mesh_0_06_;
    Mesh mesh_0_07_;
    Mesh mesh_0_08_;

    //meshes for rabbit
    Mesh mesh_1_00_;
    Mesh mesh_1_01_;
    Mesh mesh_1_02_;
    Mesh mesh_1_03_;
    Mesh mesh_1_04_;
    Mesh mesh_1_05_;
    Mesh mesh_1_06_;
    Mesh mesh_1_07_;
    Mesh mesh_1_08_;

    unsigned number_of_current_mesh_;



    Vector3d direction_;
    Vector3d up_;
    Vector3d eye_;
    Vector3d gravity_;
    float height_to_eye_;
    float d_;

    bool forward_;
    bool backward_;
    bool turning_;
    bool jumping_;

    float energy_;  // in %

    float max_velocity_;  // maximum velocity that can be reached by running
    float run_acceleration_; // acceleration when running forward

    bool dead_;
};

#endif
