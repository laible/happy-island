//
// This CTextDrawer singleton class written by Martin G Bell, 2003.
//
// * Based on NeHe lesson 17 with adaptations by Marco Monster.
// * Original NeHe comments:
// *		This Code Was Created By Jeff Molofee 2000
// *		And Modified By Giuseppe D'Agata (waveform@tiscalinet.it)
// *		If You've Found This Code Useful, Please Let Me Know.
// *		Visit My Site At nehe.gamedev.net
//
// Description:
//
//   Text drawing class that uses the singleton design pattern.
//
// Usage:
//
//   init: TEXTDRAWER.SetFontTextureId(aFontTextureId);	// See note below.
//   then: TEXTDRAWER.SetColor(R, G, B, A);				// Defaults to opaque white.
//       TEXTDRAWER.PrintText(x, y, aString, ...);
//
//   Note: Requires a 256x256 texture containing 16x16 font glyphs.
//

#ifndef _DRAWTEXT_H
#define _DRAWTEXT_H

#include <windows.h>
#include <gl.h>


// The number of chars in our font texture.
// We are assuming two sets of 128 chars (e.g. as created by Bitmap Font Creator).
#define NUMCHARS    256


#define TEXTDRAWER CTextDrawer::GetSingleton()
#define DESTROY_TEXTDRAWER CTextDrawer::Destroy()


// Util function prototypes.
void Go2d();
void Exit2d();


//
// Singleton class to draw text as texture-mapped polygons.
//
class CTextDrawer
{

public:		// Interface

	static CTextDrawer& GetSingleton();
	static void Destroy();

	bool PrintText(float aX, float aY, const char *aFmt, ...);
	void loadFontFromBMP(const char* filename);
	void SetColor(float aR, float aG, float aB, float aA);

private:	// Implementation

	CTextDrawer();
	static void Initialise();							

private:	// Data

	static CTextDrawer* mSingleton;

	GLuint mBaseListId;		// Start list id
	GLuint mFontTexId;		// GL font texture id.
	float mR, mG, mB, mA;	// Colour.
};


#endif
