#ifndef _PyramidNode_h_
#define _PyramidNode_h_

#include "ObjectNode.h"

namespace gris 
{
  class PyramidNode: public ObjectNode 
  {
    enum Alignment {
      BASE_CENTER,
      MASS_CENTER
    }
    public:
      /// rectangular face (w,h) and height d
      PyramidNode():w_(1.0), h_(1.0), d_(1.0), alignment_(BASE_CENTER) { updateVertices_(); } 
      void setGeometry(float w, float h, float d) { w_ = w; h_ = h; d_ = d; updateVertices_();}
      Vector3d getGeometry() const { return Vector3d(w_, h_, d_); }
      void alignToMassCenter() { alignment_ = MASS_CENTER; updateVertices_(); }
      void alignToBaseCenter() { alignment_ = BASE_CENTER; updateVertices_(); }
      void draw()
      {
        glPushAttrib(GL_ALL_ATTRIB_BITS);
        glPushMatrix();
        glBegin(GL_QUADS);
        glNormal3f(0, 0, -1);
        glVertex3fv(rectangle_[0]);  
        glVertex3fv(rectangle_[1]);  
        glVertex3fv(rectangle_[3]);  
        glVertex3fv(rectangle_[2]);  
        glEnd();
        
        float alpha = tan(d_ / (w_/2) );
        float beta = tan(d_ / (h_/2) );
        glBegin(GL_TRIANGLE_FAN);
        glNormal3f(0, 0, -1);
        glVertex3fv(apex_);  
        glVertex3fv(rectangle_[0]);  
        glVertex3fv(rectangle_[1]);  
        glVertex3fv(rectangle_[2]);  
        glVertex3fv(rectangle_[3]);  
        glVertex3fv(rectangle_[0]);          
        glEnd();
        glPopAttrib();
        glPopMatrix();
      }
    protected:
      float w_, h_, d_;  // width, height, depth
      Alignment alignment_;
      Vector3d[4] rectangle_;
      Vector3d apex_;
      void updateVertices_()
      {
        Vector3d base_center;
        switch (alignment_)
        {          
          case MASS_CENTER:
            base_center = center_ - d_/4;
          break;
          default:
          case BASE_CENTER:
            base_center = center_;
          break;          
        }
        apex_ = base_center + Vector3d(0, 0, d_);
        rectangle_[0] = base_center - Vector3d(-w_/2, -h_/2, 0);
        rectangle_[1] = base_center - Vector3d(-w_/2,  h_/2, 0);
        rectangle_[2] = base_center - Vector3d( w_/2,  h_/2, 0);
        rectangle_[3] = base_center - Vector3d( w_/2,  -h_/2, 0);
      }      
  };
}

#endif
