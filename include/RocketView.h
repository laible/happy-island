#ifndef _rocketview_h_
#define _rocketview_h_

#include "IDrawable.h"
#include "Rocket.h"

#include "SDL_opengl.h"
#include "glut.h"

class RocketView : public IDrawable
{
  public:
    RocketView(Rocket* model):rocket_(model) {}

    void draw()
    {
      glPushAttrib(GL_ALL_ATTRIB_BITS);
      glPushMatrix();
      glTranslated(rocket_->getCenter().x, rocket_->getCenter().y, rocket_->getCenter().z); 
//      glutSolidSphere(1.0, 6, 6);
      glPopMatrix();
      glPopAttrib();
    }
  private:
    Rocket* rocket_;
};

#endif