#ifndef _water_h_
#define _water_h_

#include "Vector3d.h"
#include "IDrawable.h"
#include "ITimedObject.h"
#include "Texture.h"


class Water : public IDrawable, public ITimedObject
{
  public:
    Water(Vector3d a, Vector3d b, Vector3d c, Vector3d d, char* tile);
    Vector3d getCenter() { return (A+B+C+D)*0.25; }
    float getHeight() {return A.z;}
    void setHeight(float h);
    void addToHeight(float d);
    void draw();
    void updateTime(long time, long delta_t);
  private:
    Vector3d A,B,C,D; // vertices of the water surface
    Texture tile_texture_; 
};

#endif