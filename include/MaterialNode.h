#ifndef _MaterialNode_h_
#define _MaterialNode_h_

#include "Node.h"

struct GLMaterial 
{
  float ambient[4];
  float diffuse[4];
  float specular[4];
  float shininess[1];
};

GLMaterial DEFAULT_MATERIAL = {{0.9f, 0.9f, 1.0f, 1.0f},
                               {0.9f, 0.9f, 1.0f, 1.0f},
                               {0.05f, 0.05f, 0.05f, 1.0f},
                               {1.0}}; 

namespace gris 
{
  class MaterialNode: public Node 
  {
    public:
      MaterialNode(Node* c):child_(c), material_(DEFAULT_MATERIAL) {}
      void setMaterial(const GLMaterial& m) { material_ = m; }
      GLMaterial getMaterial() const { return material_ ;}
      void draw() 
      {
        /**
          glPushAttrib(GL_LIGHTING_BIT); saves following values
            GL_COLOR_MATERIAL enable bit 
            GL_COLOR_MATERIAL_FACE value 
            Color material parameters that are tracking the current color 
            Ambient scene color 
            GL_LIGHT_MODEL_LOCAL_VIEWER value 
            GL_LIGHT_MODEL_TWO_SIDE setting 
            GL_LIGHTING enable bit 
            Enable bit for each light 
            Ambient, diffuse, and specular intensity for each light 
            Direction, position, exponent, and cutoff angle for each light 
            Constant, linear, and quadratic attenuation factors for each light 
            Ambient, diffuse, specular, and emissive color for each material 
            Ambient, diffuse, and specular color indices for each material 
            Specular exponent for each material 
            GL_SHADE_MODEL setting 
        **/
        glPushAttrib(GL_LIGHTING_BIT);        
        glMaterialfv(GL_FRONT, GL_AMBIENT, material_.ambient);  
        glMaterialfv(GL_FRONT, GL_DIFFUSE, material_.diffuse);  
        glMaterialfv(GL_FRONT, GL_SPECULAR, material_.specular);
        glMaterialfv(GL_FRONT, GL_SHININESS, material_.shininess);
        child_->draw();
        glPopAttrib();
      }
    protected:
      Node* child_;
      GLMaterial material_;
  };
}

#endif