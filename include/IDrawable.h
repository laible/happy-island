#ifndef _idrawable_h_
#define _idrawable_h_

class IDrawable
{
  public:
    IDrawable():visible_(true) {};
    virtual ~IDrawable() {};
    virtual void draw() {};
    void setVisible(bool b) { visible_ = b; }
    bool isVisible() { return visible_; }   
  protected:
    bool visible_; 
};

#endif
