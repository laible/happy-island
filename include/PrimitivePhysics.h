#ifndef _primitivephysics_h_
#define _primitivephysics_h_

#include "Vector3d.h"
#include "IPhysics.h"

/**  usage:
  * (1. add force if new force is applied to object)
  *  2. simulate
  *  3. get move vector relative to origin
  */

//#define DEBUG_PRIMITIVEPHYSICS

class PrimitivePhysics : public IPhysics
{
  public:    
    PrimitivePhysics():delta_s_(Vector3d(0,0,0)), mass_(20.0), velocity_(Vector3d(0,0,0)), force_(Vector3d(0,0,0))  {}
    void setMass(const float m) { mass_ = m; }
    float getMass() const { return mass_; }
    void setVelocity(Vector3d v) { velocity_ = v; }
    Vector3d getVelocity() { return velocity_ ; }
    void addForce(Vector3d f) { force_ = force_ + f; }
    void setForce(Vector3d f) { force_ = f; }
    Vector3d getForce() { return force_; }
    Vector3d getMove() { return delta_s_; }
    void simulate(const float delta_t)
    {
      Vector3d acceleration = force_ ;      
      velocity_ = velocity_ + acceleration * delta_t;
      //velocity_ = 0.99 * velocity_ *delta_t;
      delta_s_ = velocity_ * delta_t + acceleration * 0.5f * delta_t * delta_t;  // calculate new position
      #ifdef DEBUG_PRIMITIVEPHYSICS
        printf("PrimitivePhysics::veloctiy = %f %f %f\n", velocity_.x,velocity_.y,velocity_.z);
        printf("PrimitivePhysics::delta_s_ = %f %f %f\n", delta_s_.x,delta_s_.y,delta_s_.z);
      #endif 
    }

  protected:
    Vector3d delta_s_;
    float mass_;
    Vector3d force_;
    Vector3d velocity_;
};

#endif
