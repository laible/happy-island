
class MathUtilities
{
  public:

  // returns the lowest solution for the quadratic equation ax*x+b*x+c in &root iff a positive solution < maxR exists
  static bool getLowestRoot(double a, double b, double c, double maxR, double* root)
  {
    // check term under square
    double determinant = b*b - 4.0*a*c;
    if (determinant < 0.0) return false; // no solution
    double sqrtD = sqrt(determinant);
    double r1 = (-b - sqrtD) / (2*a);
    double r2 = (-b + sqrtD) / (2*a);
    if (r1 > r2) std::swap<double>(r1,r2);
    
    if (r1>0 && r1 < maxR)
    {
      *root = r1;
      return true;
    }

    if (r2>0 && r2 < maxR)
    {
      *root = r2;
      return true;
    }
            
    return false;  // no solution found 
  }

/*
  // check if ray intersects plane
  inline static bool rayIntersectsPlane(const Ray& ray, const Plane& plane)
  { 
             
  }
  
  // check if ray intersects triangle
  inline static bool rayIntersectsTriangle(const Ray& ray, const Triangle& tri)
  {    
    // check first rayIntersectsPlane(ray, Plane(tri)), then check barycentric coordinates  
  }
  */
  inline static bool isVector3dInsideTriangle3d(const Vector3d& p, const Vector3d& t1, const Vector3d& t2, const Vector3d& t3)
  {        
    Vector3d e21 = t2-t1;             
    Vector3d e31 = t3-t1;
    double a = e21.dotProduct(e21);
    double b = e21.dotProduct(e31);
    double c = e31.dotProduct(e31);
    double ac_bb = a*c-b*b;
    Vector3d pt1 = p-t1;
    double d = pt1.dotProduct(e21);
    double e = pt1.dotProduct(e31);
    double x = d*c - e*b;
    double y = e*a - d*b;
    double z = x+y-ac_bb;
    return (( (unsigned int)(z)& ~((unsigned int)(x)|(unsigned int)(y))) & 0x80000000);
  }
  
};
