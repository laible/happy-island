#ifndef _particle_h_
#define _particle_h_

#include "IDrawable.h"
#include "Vector3d.h"
#include "ITimedObject.h"
#include "Texture.h"
#include "MovableGameObject.h"

class Particle
{
  public:
  	Vector3d	position;
  	Vector3d	velocity;
};

class SimpleParticleSystem : public IDrawable, public ITimedObject
{
  public:
    enum PARTICLECOLOR
    {
      CL_WHITE = 0,
      CL_BLUE = 1,
      CL_RED = 2,
      CL_RAINBOW = 3
    };

	  SimpleParticleSystem(Vector3d center);
    ~SimpleParticleSystem()
    {
      if (particles!=0)
        delete[] particles;
    }
	  void  draw( void );
	  void	updateTime( long time, long delta_t);
    void setTimeToLive(long timeInMS) { timeToLive = timeInMS; }
    void setColor(PARTICLECOLOR color) { color_ = color; }
  protected:
    Vector3d center_;
	  int			numParticles;
	  Particle	*particles;
	  long		age;
	  long		timeToLive;
    PARTICLECOLOR color_;
};


class ParticleSystem : public SimpleParticleSystem
{
  public:
	  ParticleSystem(Vector3d center, float size);
	  void  draw( void );
	  void	updateTime( long time, long delta_t );
  protected:
	  Texture texture_;
	  float		size_;
};

class SmokeTrail : public ParticleSystem
{
  public:
	  SmokeTrail( MovableGameObject *obj, float si, unsigned int eR, unsigned int mP, float radius );
	  void  draw( void );
	  void	updateTime( long time, long delta_t );
    virtual void onBBCollision(MovableGameObject* collision_partner) {} 
    void setVelocity(float v) { velocity_ = v; }
    void setDying(bool b) { dying_ = b;}
  private:
    bool dying_;
    float radius_;
    float velocity_;
    Texture texture_;
	  int	maxParticles;	// max # of particles
	  int			current;		// cyclic pointer into particle array 
	  MovableGameObject	*subject;		// game object that generates smoke trail 
	  float		emissionRate;	// particles per second
	  float		timeAccumulator;
};



#endif

