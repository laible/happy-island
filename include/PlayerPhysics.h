#ifndef _playerphysics_h_
#define _playerphysics_h_

class PlayerPhysics
{
  public:
    PlayerPhysics() : max_velocity_(50.0f), velocity_(0.0f), acceleration_(0.1f) {}
    float getAbsVelocity() { return velocity_; }
    void setAbsVelocity(float newVel) { velocity_ = newVel; }   
    float getAbsMaxVelocity() { return max_velocity_; }
    void setAbsMaxVelocity(float newVel) { max_velocity_ = newVel; }   
    float getAbsAcceleration() { return acceleration_; }
    void setAbsAcceleration(float a) { acceleration_ = a; }  
    void stop() { setAbsVelocity(0.0f) ; }  

  private:
    // properties when player is running
    float velocity_;
    float max_velocity_;
    float acceleration_;
};

#endif
