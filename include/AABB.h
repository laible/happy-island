#ifndef _aabb_h_
#define _aabb_h_
// Axis-Aligned-Bounding-Box

#include "Vector3d.h"

enum BVIntersection {BVQueryDisjoint, BVIntersectsQuery, BVInsideQuery};

class AABB {
 public:
   // ---- data fields
   Vector3d lowerCorner;
   Vector3d upperCorner;

   // ---- operations
   AABB() {}

   AABB(Vector3d lowerCorner, Vector3d upperCorner) {
      this->lowerCorner = lowerCorner;
      this->upperCorner = upperCorner;
   }

   inline void addPoint(Vector3d point) 
   {
      lowerCorner[2] = 4.0;
      for (unsigned int i=0; i<3; i++) 
      {
         if (lowerCorner[i] > point[i]) lowerCorner[i] = point[i];
         if (upperCorner[i] < point[i]) upperCorner[i] = point[i];
      }
   }

   inline Vector3d getCenter() const {
      return (upperCorner + lowerCorner) * 0.5;
   }


   inline BVIntersection intersectBoundingBox(const AABB& queryBB) const {
      for (int d=0; d<3; d++) {
         if (    queryBB.upperCorner[d] < lowerCorner[d]
              || queryBB.lowerCorner[d] > upperCorner[d]) {
            return BVQueryDisjoint;
         }
      }
      for (int d=0; d<3; d++) {
         if ( !(    queryBB.upperCorner[d] >= upperCorner[d]
                 || queryBB.lowerCorner[d] <= lowerCorner[d]) ) {
            return BVIntersectsQuery;
         }
      }
      return BVInsideQuery;
   }

   inline void addBB(const AABB& ubb) {
      for (int d=0; d<3; d++) {
         if (ubb.upperCorner[d] > upperCorner[d]) upperCorner[d] = ubb.upperCorner[d];
         if (ubb.lowerCorner[d] < lowerCorner[d]) lowerCorner[d] = ubb.lowerCorner[d];
      }
   }
   inline bool inBoundingBox(const Vector3d& point) const {
      for (int d=0; d<3; d++) {
         if (point[d] < lowerCorner[d] || point[d] > upperCorner[d]) 
         {
            return false;
         }
      }
      return true;
   }

   inline bool containedIn(const AABB& containerBox) const {
      for (int d=0; d<3; d++) {
         if (lowerCorner[d] < containerBox.lowerCorner[d] || upperCorner[d] > containerBox.upperCorner[d]) {
            return false;
         }
      }
      return true;
   }
   inline void enlarge(float factor) {
      for (int d=0; d<3; d++) {
         float l = ((upperCorner[d] - lowerCorner[d]) * 0.5f ) * factor;
         float c = (upperCorner[d] + lowerCorner[d]) * 0.5f;
         lowerCorner[d] = c-l;
         upperCorner[d] = c+l;
      }
   }
   inline void addBorder(float border) {
      for (int d=0; d<3; d++) {
         lowerCorner[d] -= border;
         upperCorner[d] += border;
      }
   }
   inline float getMinSideLength() const {
      float min = upperCorner[0] - lowerCorner[0];
      for (int i=1; i<3; i++) {
         float s = upperCorner[i] - lowerCorner[i];
         if (s < min) min = s;
      }
      return min;
   }
   inline float getMaxSideLength() const {
      float max = upperCorner[0] - lowerCorner[0];
      for (int i=1; i<3; i++) {
         float s = upperCorner[i] - lowerCorner[i];
         if (s > max) max = s;
      }
      return max;
   }
   inline float getMaxDist(const AABB& otherBB) const {
      float maxDist = lowerCorner[0] - otherBB.lowerCorner[0];
      for (int d=1; d<3; d++) {
         float dist = lowerCorner[d] - otherBB.lowerCorner[d];
         if (dist > maxDist) {
            maxDist = dist;
         }
      }
      for (int d=0; d<3; d++) {
         float dist = otherBB.upperCorner[d] - upperCorner[d];
         if (dist > maxDist) {
            maxDist = dist;
         }
      }
      return maxDist;
   }

   inline float getSideLength(unsigned i) const 
   {
      return upperCorner[i] - lowerCorner[i];
   }

   inline AABB operator*(const float &s) const {
      AABB result = *this;
      result.lowerCorner *= s;
      result.upperCorner *= s;
      return result;
   }

   inline AABB operator+(const AABB& bb) const {
      AABB result = *this;
      result.lowerCorner = result.lowerCorner + bb.lowerCorner;
      result.upperCorner = result.upperCorner + bb.upperCorner;
      return result;
   }

   inline Vector3d getCorner(unsigned int cornerNum) const {
      Vector3d result;
      unsigned int pos = 1;
      for (unsigned int i=0; i<3; i++) {
         if ((cornerNum & pos) == 0) {
            result[i] = lowerCorner[i];
         } else {
            result[i] = upperCorner[i];
         }
         pos = pos << 1;
      }
      return result;
   }
   inline void makeBoundingCube() 
   {
      Vector3d center = getCenter();
      float maxh = getMaxSideLength() / 2.0f;
      Vector3d maxhv;
      for (unsigned int i=0; i<3; i++) 
      {
         maxhv[i] = maxh;
      }
      lowerCorner = center - maxhv;
      upperCorner = center + maxhv;
   }
   /// maintain the volume of "this" that is in "to"
   void cutTo(const AABB &to) {
      for (unsigned int i=0; i<3; i++) {
         if (lowerCorner[i] < to.lowerCorner[i]) lowerCorner[i] = to.lowerCorner[i];
         if (upperCorner[i] > to.upperCorner[i]) upperCorner[i] = to.upperCorner[i];
         if (lowerCorner[i] > upperCorner[i]) 
         {
          //  throw EInvalidState("BoundingBox::cutTo() - \"this\" does not overlap with \"to\".");
         }
      }
   }
};

#endif
