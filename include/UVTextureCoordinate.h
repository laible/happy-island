#ifndef UVTEXTURECOORDINATE
#define UVTEXTURECOORDINATE

class UVTextureCoordinate
{
  public:
    inline UVTextureCoordinate(){};
    inline UVTextureCoordinate(float u, float v):U(u), V(v) {};
    float U;
    float V;
};

#endif
