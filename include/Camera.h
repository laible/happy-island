#ifndef GUARD_Camera_h
#define GUARD_Camera_h

#include "Vector3d.h"
#include "Player.h"
#include "ITimedObject.h"

class Camera
{
public:
  Camera(): current_player_(0), eye(Vector3d(0,0,0)), center(Vector3d(0,0,-1)), up(Vector3d(0,1,0)) {activate();}
  
  
  Camera(Vector3d eye_vec, Vector3d center_vec, Vector3d up_vec):current_player_(0)
  {
    set(eye_vec, center_vec, up_vec);
    activate();
  }    
  
  Camera(float eye_x, float eye_y, float eye_z, 
          float center_x, float center_y, float center_z, 
          float up_x, float up_y, float up_z)
  :current_player_(0)
  {
    set(eye_x, eye_y, eye_z,  center_x, center_y, center_z,  up_x, up_y, up_z);
    activate();
  }

  
  Camera& set(Vector3d eye_vec, Vector3d center_vec, Vector3d up_vec)  
  {
    eye = eye_vec;
    center = center_vec;
    up = up_vec;

    return *this;
  }

  Camera& set(float eye_x, float eye_y, float eye_z, 
            float center_x, float center_y, float center_z, 
            float up_x, float up_y, float up_z)
  {
    return set(Vector3d(eye_x, eye_y, eye_z), Vector3d(center_x, center_y, center_z), Vector3d(up_x, up_y, up_z));
  }

  
  Camera& set_eye(Vector3d eye_vec)
  {
    eye = eye_vec;
    return *this;
  }
  
  Camera& set_eye(float eye_x, float eye_y, float eye_z)
  {
    return set_eye(Vector3d(eye_x, eye_y, eye_z));
  }


  Camera& set_center(Vector3d center_vec)
  {
    center = center_vec;
    return *this;
  }

  Camera& set_center(float center_x, float center_y, float center_z)
  {
    return set_center(Vector3d(center_x, center_y, center_z));
  }


  Camera& set_up(Vector3d up_vec)
  {
    up = up_vec;
    return *this;
  }

  Camera& set_up(float up_x, float up_y, float up_z)
  {
    return set_up(Vector3d(up_x, up_y, up_z));
  }


  Vector3d get_eye()
  {
    return eye;
  }

  Vector3d get_center()
  {
    return center;
  }

  Vector3d get_up()
  {
    return up;
  }

  void activate();

  void attach_to_player(Player*);

protected:
  Vector3d eye, center, up;
  Player* current_player_;
  float height_offset_;
  float distance_offset_;  
};

#endif
