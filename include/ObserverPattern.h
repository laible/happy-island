#ifndef OBSERPATTERN_H_
#define OBSERPATTERN_H_

#include <deque>
#include <string>
#include <ostream>

class Observable;

struct Observer
{
  virtual void update( Observable* ) = 0;
};

class Observable
{
public:
  void attach( Observer* );
  void detach( Observer* );
  void notifyObservers();
private:
  std::deque<Observer*> observers_;
};

#endif /*OBSERPATTERN_H_*/
