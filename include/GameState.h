#ifndef _game_state_h_
#define _game_state_h_

#include "IDrawable.h"
#include "ITimedObject.h"
#include "IInputListener.h"

class GameState:public IDrawable,public  IInputListener,public  ITimedObject
{
  public:
    void draw() {}

    // input events
    void keyPressEvent(SDL_KeyboardEvent e) {};
    void keyReleaseEvent(SDL_KeyboardEvent e) {};
    void mouseMotionEvent(SDL_MouseMotionEvent e) {};
    void mousePressEvent(SDL_MouseButtonEvent e) {};
    void mouseReleaseEvent(SDL_MouseButtonEvent e) {};
    // timer events
    void updateTime(long time, long delta_t) {}
};

#endif
