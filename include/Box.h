#ifndef _box_h
#define _box_h

#include "MovableGameObject.h"
#include "Vector3d.h"
#include "IDrawable.h"
#include "Texture.h"
#include "Particle.h"

class InGame;

class Box : public MovableGameObject
{
  public:
  	Box():MovableGameObject("Box of Goddies") {};
    Box(const Vector3d& c, InGame* ig);

    virtual ~Box() { delete[] vertices_;}

    void draw()
    {
      
      glPushMatrix();
      glDisable(GL_LIGHTING);
      glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
      glEnable(GL_TEXTURE_2D);
      glBindTexture(GL_TEXTURE_2D, texture_->id);
      glDisable(GL_CULL_FACE);
      glBegin(GL_QUADS);
        // top
        glNormal3d(0, 0, 1);
        glTexCoord2d(0 , 0);
        glVertex3f(vertices_[1].x, vertices_[1].y, vertices_[1].z);
        glTexCoord2d(0 , 1);
        glVertex3f(vertices_[3].x, vertices_[3].y, vertices_[3].z);
        glTexCoord2d(1 , 1);
        glVertex3f(vertices_[7].x, vertices_[7].y, vertices_[7].z );
        glTexCoord2d(1 , 0);
        glVertex3f(vertices_[5].x, vertices_[5].y, vertices_[5].z);

        // bottom
        glNormal3d(0, 0, -1);
        glTexCoord2d(0 , 0);
        glVertex3f(vertices_[0].x, vertices_[0].y, vertices_[0].z);
        glTexCoord2d(0 , 1);
        glVertex3f(vertices_[2].x, vertices_[2].y, vertices_[2].z);
        glTexCoord2d(1 , 1);
        glVertex3f(vertices_[6].x, vertices_[6].y, vertices_[6].z);
        glTexCoord2d(1 , 0);
        glVertex3f(vertices_[4].x, vertices_[4].y, vertices_[4].z);

        // left 
        glNormal3d(-1, 0, 0);
        glTexCoord2d(0 , 0);
        glVertex3f(vertices_[0].x, vertices_[0].y, vertices_[0].z);
        glTexCoord2d(0 , 1);
        glVertex3f(vertices_[1].x, vertices_[1].y, vertices_[1].z);
        glTexCoord2d(1 , 1);
        glVertex3f(vertices_[3].x, vertices_[3].y, vertices_[3].z);
        glTexCoord2d(1 , 0);
        glVertex3f(vertices_[2].x, vertices_[2].y, vertices_[2].z);

        // right
        glNormal3d(1, 0, 0);
        glTexCoord2d(0 , 0);
        glVertex3f(vertices_[4].x, vertices_[4].y, vertices_[4].z);
        glTexCoord2d(0 , 1);
        glVertex3f(vertices_[5].x, vertices_[5].y, vertices_[5].z);
        glTexCoord2d(1 , 1);
        glVertex3f(vertices_[7].x, vertices_[7].y, vertices_[7].z );
        glTexCoord2d(1 , 0);
        glVertex3f(vertices_[6].x, vertices_[6].y, vertices_[6].z);

        // front 
        glNormal3d(0, -1, 0);
        glTexCoord2d(0 , 0);
        glVertex3f(vertices_[0].x, vertices_[0].y, vertices_[0].z);
        glTexCoord2d(0 , 1);
        glVertex3f(vertices_[1].x, vertices_[1].y, vertices_[1].z);
        glTexCoord2d(1 , 1);
        glVertex3f(vertices_[5].x, vertices_[5].y, vertices_[5].z);
        glTexCoord2d(1 , 0);
        glVertex3f(vertices_[4].x, vertices_[4].y, vertices_[4].z);

        // back
        glNormal3d(0, 1, 0);
        glTexCoord2d(0 , 0);
        glVertex3f(vertices_[3].x, vertices_[3].y, vertices_[3].z);
        glTexCoord2d(0 , 1);
        glVertex3f(vertices_[2].x, vertices_[2].y, vertices_[2].z);
        glTexCoord2d(1 , 1);
        glVertex3f(vertices_[6].x, vertices_[6].y, vertices_[6].z );
        glTexCoord2d(1 , 0);
        glVertex3f(vertices_[7].x, vertices_[7].y, vertices_[7].z);

      glEnd();  
      glPopMatrix();
      glEnable(GL_LIGHTING);
    }

  private:     
    InGame* in_game_;
    Texture* texture_;
    Vector3d* vertices_;
    SmokeTrail* smoke_trail_;
};


#endif
