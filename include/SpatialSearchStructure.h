#ifndef _Spatial_Search_Structure_h
#define _Spatial_Search_Structure_h

#include <list>
#include <vector>
#include "Mesh3d.h"

class SpatialSearchStructure
{
  public:
    SpatialSearchStructure():va_triangles(0) {};
    ~SpatialSearchStructure() { delete va_triangles; }
    void build(const Mesh m);
    std::vector<Mesh3d::VertexIndex> requestNearVertices(const Vector3d& v, const float max_distance) const; 
 
    std::vector<Triangle3d> requestNearTriangles( Mesh m, const Vector3d& v, const float max_distance);                            

    static void drawDebugTriangles(const std::vector<Triangle3d>& tri);
  private:
    std::vector<std::vector<std::vector<Triangle3d> > > structure_;
    Triangle3d* va_triangles;
    Vector3d lowerLeftCorner;
    Vector3d upperRightCorner;
      
};

#endif
