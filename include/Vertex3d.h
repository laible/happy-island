#ifndef VERTEX3D
#define VERTEX3D

#include "Vector3d.h"


class Vertex3d
{
  public:    
    /// the vertex data
    Vector3d vector;
    Vector3d normal;
   
    //////////////////////////////////////////////////////////////////////////////////////////////
    // Constructors
        
    /// ! no default initialisation (speed)
    inline Vertex3d() 
    {
    }; 
    
    inline Vertex3d(float vx, float vy, float vz, float nx, float ny, float nz) /// create Vertex with vector coords and normal coords
      :vector(vx,vy,vz), normal(nx, ny, nz) 
    {
    };
    
    inline Vertex3d(float vcoords[3], float ncoords[3])
      :vector(vcoords), normal(ncoords) 
    {
    };
    
    inline Vertex3d(const float* const coords_p)
      :vector(coords_p), normal(coords_p+3) 
    {
    };
    
    inline Vertex3d(const Vertex3d& v)
      :vector(v.vector), normal(v.normal) 
    {
    };

    //////////////////////////////////////////////////////////////////////////////////////////////
                  
    inline Vertex3d& operator=(const Vertex3d& v)
    {
      vector = v.vector;
      normal = v.normal;
      return *this;
    }

    // Comparision
    // wo vertices are equal iff the position vector and the normal vector are equal      
    inline bool operator==(const Vertex3d &v) const
    {
      return ( vector == v.vector && normal == v.normal );
    }
          
    inline bool operator== (const Vertex3d &v)
    {
      return ( vector == v.vector && normal == v.normal );
    }
       
    inline bool  operator!= (const Vertex3d &v) const
    {
      return ( vector != v.vector || normal != v.normal );
    }
          
};
#endif
