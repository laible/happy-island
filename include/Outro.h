#ifndef _outro_h_
#define _outro_h_

#include "GameState.h"
#include "Bitmap.h"

class Outro : public GameState
{
public:
  Outro();
    
  // input events
  void keyPressEvent(SDL_KeyboardEvent e);
  void keyReleaseEvent(SDL_KeyboardEvent e);
  void mouseMotionEvent(SDL_MouseMotionEvent e);
  void mousePressEvent(SDL_MouseButtonEvent e);
  void mouseReleaseEvent(SDL_MouseButtonEvent e);

  void draw();

private:
  Bitmap image_;
};

#endif
