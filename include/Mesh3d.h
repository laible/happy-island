#ifndef MESH3D
#define MESH3D

#include <vector>
#include <list>
#include <string>
#include "Vector3d.h"
#include "Vertex3d.h"
#include "UVTextureCoordinate.h"
#include "Triangle3d.h"
#include "Triangle3dUVTextureCoordinate.h"
#include "IDrawable.h"
#include "Light.h"
#include "Texture.h"

#include "SDL_opengl.h"

#include <map>
#include "boost/shared_ptr.hpp"


struct Material 
{

float ambient[4];
float diffuse[4];
float specular[4];
float shininess[1];

};

enum TextureMappingMode
{
  TMM_PLANAR,
  TMM_SPHERICAL
};

const Material DEFAULT_MATERIAL = {{0.9f, 0.9f, 1.0f, 1.0f},
                 {0.9f, 0.9f, 1.0f, 1.0f},
                 {0.05f, 0.05f, 0.05f, 1.0f},
                 {1.0}}; 


class Mesh3d:public IDrawable
{
  public:   
    Mesh3d();
    Mesh3d(const char* ASEFilename, TextureMappingMode tm = TMM_PLANAR): 
      nVertices(0),
      vertices(0),
      normals(0),
      texture_coords(0),
      nTriangles(0),
      va_triangles(0),
      va_normals(0),
      va_texture_coords(0),
      nTextureStages(0),
  //    texture_ids(0),
      map_ti_to_vi(0),
      display_list_(0),
      material_(DEFAULT_MATERIAL)
    {
      loadFromASEFile(ASEFilename, tm); 
    };
    Mesh3d(const char* ASEFilename, GLint* external_texture);
    ~Mesh3d();
    
    typedef unsigned int TriangleIndex;
    typedef unsigned int VertexIndex;
     
    struct VertexIndexTriple 
    {
      union {
        struct {
          VertexIndex indexOfA,indexOfB,indexOfC;
        };
        VertexIndex val[3];
      };

      inline VertexIndex& operator[](const unsigned int& i)    
      {
        assert(i<3);
        return *(&indexOfA+i);
      };

      inline const VertexIndex& operator[](const unsigned int& i) const  
      {
        assert(i<3);
        return *(&indexOfA+i);
      };

    };

    struct Neighbours
    {
      unsigned int neigh[3];
    };

    struct PlaneEquation
    {
      float a;
      float b;
      float c;
      float d;
    };

    /** Explanation of the following structure:
      * For maximum speed, the data must be organised in a vertex-array of triangles (that stays completly in graphic memory).
      * With this structure we can only get information from triangle to child vertices.
      * The other direction (vertex to triangle) is not possible without searching the whole structure.
      * e.g. changing one vertex would actually change all triangles that share this vertex.
      * So additional mapping/store structures are added to make this possible:
      * The mappings are:
      * TriangleIndex->VertexIndexTriple
      * VertexIndex->TriangleIndexList
      * The store structures are:
      * vertices, normals
      * Example (triangle exact collision detection):
      * given a point p and the index (in "vertices") of the vertex next to p we can use the mapping VertexIndex->TriangleIndexList
      * to find all triangles next to p.
      * Example (modification)
      * given a triangle we use TriangleIndex->VertexIndexTriple to modify all vertices in a triangle on the helper structure vertices
      * and recreate the vbo to update the data in graphic memory
      **/
            
    /** Vertex data (helper structure... NOT used for drawing but to build vertex arrays):
      */           
    unsigned int nVertices;   ///< number of vertices    
    Vector3d* vertices;       ///< vertex-array (size = n*3*float)

    /** Normal data (helper structure... NOT used for drawing but to build vertex arrays):
      */               
    Vector3d* normals;    ///< vertex-array (size = n*3*float)          

    /** Texture coordinates data (helper structure... NOT used for drawing but to build vertex arrays):
      */      
    UVTextureCoordinate* texture_coords;
                
    /** Triangle data (used for drawing totally stays in graphic card):           
      *     
      * examples: 
      * va_triangles[5] is the 6th triangle
      * va_triangles[5].B is the second vertex of the 6th triangle
      * va_triangles[5].B.x is the x-coordinate of this vertex
      **/
    unsigned int nTriangles;  ///< number of triangles
    Triangle3d* va_triangles;  ///< vertex-array of triangles (size = n*3*3*sizeof(float))    
    

    /** Normal data (used for drawing):
      * va_normals is essentially the same as va_triangles
      * va_normals[5].B.x yields the x component of the normal of the second vertex of the 6th triangle
      **/      
    Triangle3d* va_normals;   
            
    /** Texture data (used for drawing):      
      * va_texture_coords[5].B.U yields the U component of the texture coordinate of the second vertex of the 6th triangle
      **/   
    Triangle3dUVTextureCoords* va_texture_coords; ///< UV texture coordinates array

    unsigned int nTextureVertices;

    std::vector<VertexIndexTriple> map_tti_to_vi;

    unsigned int nTextureStages;  ///< number of textures (0:no texture, 1:single texture, >1:multi texture)
//    GLuint* texture_ids;
    std::string texture_filename;                            
  
    VertexIndexTriple* map_ti_to_vi;  ///< map from triangle index to vertex triple
    Neighbours* va_neighbours;  
    PlaneEquation* plane_eq_;
    bool* visible_;
    GLuint display_list_;

    // drawing       
    void draw();
    void draw(Light* l);  // draw with shadows 
    void castShadows(); // gets called by draw(Light* l)
    // loading      
    bool loadFromASEFile(const char* filename, TextureMappingMode tm = TMM_PLANAR);
    void prepareForShadows(); // create additional structures and enable shadow casting
	  void createDL();	// create Display-List
    void dump();
    void setMaterial(Material m) 
    { 
      material_ = m;
    }
    protected:
      Material material_;
      Texture* texture_;
};

class Mesh : public IDrawable
{
  public:
    //typedef std::map<std::string, boost::shared_ptr<Mesh3d> > MeshMap;

    Mesh(const char* ASEFilename, TextureMappingMode tm = TMM_PLANAR):
      mesh3d_(new Mesh3d())
    {      
      std::map<std::string, boost::shared_ptr<Mesh3d> >::iterator it = resource_manager_.find(std::string(ASEFilename));

      if (it!=resource_manager_.end())
      {
        printf("found mesh in cache... skipping loading\n");
        mesh3d_ = it->second;
      } else
      {         
        printf("mesh not found mesh in cache... loading from harddisk\n");
        mesh3d_->loadFromASEFile(ASEFilename, tm);
        resource_manager_[std::string(ASEFilename)] = mesh3d_;
      }
    }

    Triangle3d* getTriangles() { return mesh3d_->va_triangles; }

    void draw()
    {
      mesh3d_->draw();
    }

    void castShadows()
    {
      mesh3d_->castShadows();
    }

    bool loadFromASEFile(const char* filename, TextureMappingMode tm = TMM_PLANAR)
    {
      mesh3d_->loadFromASEFile(filename, tm);
      return true;
    }

    void prepareForShadows()
    {
      mesh3d_->prepareForShadows();
    } 

	  void createDL()
    {
      mesh3d_->createDL();
    };

    void dump()
    {
      mesh3d_->dump();
    }

    int getNumberOfVertices() { return mesh3d_->nVertices; }

    Vector3d* getVertices() { return mesh3d_->vertices; }

    int getNumberOfTriangles() { return mesh3d_->nTriangles; }
    
    Mesh3d::VertexIndexTriple* getFaceToVertexMap() { return mesh3d_->map_ti_to_vi; }
  
    Vector3d* getNormals() { return mesh3d_->normals; };
    Triangle3d* getNormalsVArray() { return mesh3d_->va_normals; }

    unsigned int getNumberOfTextureStages() { return mesh3d_->nTextureStages; }

    Triangle3dUVTextureCoords* getTextureCoordsVArray() { return mesh3d_->va_texture_coords; }
    UVTextureCoordinate* getTextureCoords() { return mesh3d_->texture_coords; }

    void setMaterial(Material m) 
    {
      mesh3d_->setMaterial(m);
    }    

    int getDL() { return mesh3d_->display_list_; }


    static std::map<std::string, boost::shared_ptr<Mesh3d> > resource_manager_;  

  private:    

    boost::shared_ptr<Mesh3d> mesh3d_;
};


#endif
