#ifndef SOUNDMANAGER_H_
#define SOUNDMANAGER_H_

#include "al.h"
#include "alc.h"
#include "alut.h"
#include <cstdlib>
#include <cstdio>

/**
 * Manages sound and music loading/playback
 * background music is placed in source[0], others are sound effects
 * There are 2 types of sound effects static sound effects, that are placed at the current players world position
 * and dynamic sound effect which are placed at a specific position in the world
 **/

class SoundManager
{
	public:
		SoundManager();
		void setListenerPosition(ALfloat x, ALfloat y, ALfloat z);
		void loadSounds();
		void stopMusic();		
		void playMusic();		
		void pauseMusic();						
	    void playBombSound(ALfloat x, ALfloat y, ALfloat z);  
		void playReloadSound(); 
	    void playBubbleSound(); 
	    void stopBubbleSound();
	    void playGongSound();   
	    void playLaunchSound(); 
	    void playOceanSound();  
	    void stopOceanSound();  
	    void playClockSound();  
	    void playJawsSound();	    
	    void playSeagullSound();	
		void stopSeagullSound();
	private:
		ALboolean loadWave_(char *szWaveFile, ALuint BufferID);
		bool soundStopped_(ALuint n);
		bool sound_loaded_;
    	ALuint source[10]; // Array of Source IDs
    	ALuint alBuffers[10];  // Array of Buffer IDs	
};

#endif /*SOUNDMANAGER_H_*/
