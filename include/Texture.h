#ifndef _texture_h_
#define _texture_h_

#include "SDL_opengl.h"
#include <string>

#include <map>

enum TextureFlags
{
  NO_MIPMAPS = 0,
  MIPMAPS = 1
};

class Texture
{  
  public:      
    Texture(const std::string& filename, TextureFlags flags = MIPMAPS);
    GLuint id;
    int getWidth() { return width_;}
    int getHeight() { return height_;}

    static std::map<std::string, GLuint > resource_manager_ ; 
  protected:
    int width_, height_;
};

#endif
