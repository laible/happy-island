#include "Vector3d.h"

class Plane
{
  public:
    Vector3d origin, normal;    
    
    inline Plane() /// ! no default initialisation
    {
    };
    
    Plane(const Vector3d& origin, const Vector3d& normal);  /// construct from origin and normal vector
    Plane(const Vector3d& p1, const Vector3d& p2, const Vector3d& p3); /// construct from origin and normal vector
    bool isFrontFacingTo(const Vector3d& v) const; /// check if Plane is facing into same direction as v
    float signedDistanceTo(const Vector3d& p) const; /// returns the signed distance to p
    
  private:
    float equation[4];
};
