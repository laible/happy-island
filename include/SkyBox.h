#ifndef _skybox_h_
#define _skybox_h_

#include "IDrawable.h"
#include "Texture.h"
#include <string>
#include "Mesh3d.h"

class SkyDome:public IDrawable
{
  public:
    SkyDome(std::string filename);
    Vector3d center;
    void draw();
  protected:
    float size_;
    Mesh skydome_;
};

#endif
