#ifndef _menu_h_
#define _menu_h_

#include "GameState.h"
#include "GraphicMenu.h"

class Menu : public GameState
{
public:
  Menu();
  // input events
  void keyPressEvent(SDL_KeyboardEvent e);
  void keyReleaseEvent(SDL_KeyboardEvent e) {};
  void mouseMotionEvent(SDL_MouseMotionEvent e) {};
  void mousePressEvent(SDL_MouseButtonEvent e) {};
  void mouseReleaseEvent(SDL_MouseButtonEvent e) {};

  void draw();
  void update();

  GraphicMenu mainMenu_;

private:
};

#endif
